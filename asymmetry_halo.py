#!/usr/bin/python3

"""
 ==================
 = Halo Asymmetry =
 ==================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2021

 GOAL: Compute the asymmetry ratio for spherical shells of width 5 kpc
       Exclude the disk!
       - [Fe/H] < -2.5
       - |Z| > 3

       NOTE:
       Also ran another version where I selected stars WITHIN the disk, i.e.,
       |Z| < 3.
            Saved the data as *halo_asym_disk_check.hdf5

       UPDATE:
        - Running again, but including the disk region.
            Saved the data as *halo_asym_full.hdf5
"""

## Import all of the tools for analysis
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    ### Read in the z = 0 data
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #
    # Read in the old data to use the rotational velocity
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    #
    # Select metal-poor stars
    inds_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
    #inds_1 = ut.array.get_indices(np.abs(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2]), [3, np.inf], inds_1)
    #
    # Loop over radial bins
    distances_1 = np.array([0., 5., 10., 15., 20., 25., 30., 40., 50., 60., 70.])
    asymmetry_1 = np.zeros(len(distances_1)-1)
    prograde_1 = np.zeros(len(distances_1)-1)
    retrograde_1 = np.zeros(len(distances_1)-1)
    #
    for i in range(0, len(distances_1)-1):
        dist_inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.total'), [distances_1[i], distances_1[i+1]], inds_1)
        print('The number of stars with iron abundances above -2.5 is {0}'.format(np.sum(part['star'].prop('metallicity.iron',dist_inds_1) > -2.5)))
        print('The number of stars with |Z| < 3 is {0}'.format(np.sum(np.abs(part['star'].prop('star.host1.distance.principal.cylindrical',dist_inds_1)[:,2]) < 3)))
        jphi = (part['star'].prop('star.host1.distance.principal',dist_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal',dist_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal',dist_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal',dist_inds_1)[:,1])/(8*data_1['vrot.z0'])
        prograde_1[i] = np.sum(jphi > 0)
        retrograde_1[i] = np.sum(jphi < 0)
        asymmetry_1[i] = prograde_1[i]/retrograde_1[i]
    #
    d_1 = dict()
    d_1['asymmetry'] = asymmetry_1
    d_1['num.prograde'] = prograde_1
    d_1['num.retrograde'] = retrograde_1
    d_1['distance.array'] = distances_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_asym_full', dict_or_array_to_write=d_1, verbose=True)

if num_gal == 2:
    ### Read in the z = 0 data
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #
    # Read in the old data to use the rotational velocity
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    #
    # Select metal-poor stars
    inds_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
    #inds_1 = ut.array.get_indices(np.abs(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2]), [3, np.inf], inds_1)
    #
    # Loop over radial bins
    distances_1 = np.array([0., 5., 10., 15., 20., 25., 30., 40., 50., 60., 70.])
    asymmetry_1 = np.zeros(len(distances_1)-1)
    prograde_1 = np.zeros(len(distances_1)-1)
    retrograde_1 = np.zeros(len(distances_1)-1)
    #
    for i in range(0, len(distances_1)-1):
        dist_inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.total'), [distances_1[i], distances_1[i+1]], inds_1)
        print('The number of stars with iron abundances above -2.5 is {0}'.format(np.sum(part['star'].prop('metallicity.iron',dist_inds_1) > -2.5)))
        print('The number of stars with |Z| < 3 is {0}'.format(np.sum(np.abs(part['star'].prop('star.host1.distance.principal.cylindrical',dist_inds_1)[:,2]) < 3)))
        jphi = (part['star'].prop('star.host1.distance.principal',dist_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal',dist_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal',dist_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal',dist_inds_1)[:,1])/(8*data_1['vrot.z0'])
        prograde_1[i] = np.sum(jphi > 0)
        retrograde_1[i] = np.sum(jphi < 0)
        asymmetry_1[i] = prograde_1[i]/retrograde_1[i]
    #
    d_1 = dict()
    d_1['asymmetry'] = asymmetry_1
    d_1['num.prograde'] = prograde_1
    d_1['num.retrograde'] = retrograde_1
    d_1['distance.array'] = distances_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_asym_full', dict_or_array_to_write=d_1, verbose=True)
    #
    #
    # Read in the old data to use the rotational velocity
    data_2 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    #
    # Select metal-poor stars
    inds_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
    #inds_2 = ut.array.get_indices(np.abs(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2]), [3, np.inf], inds_2)
    #
    # Loop over radial bins
    distances_2 = np.array([0., 5., 10., 15., 20., 25., 30., 40., 50., 60., 70.])
    asymmetry_2 = np.zeros(len(distances_2)-1)
    prograde_2 = np.zeros(len(distances_2)-1)
    retrograde_2 = np.zeros(len(distances_2)-1)
    #
    for i in range(0, len(distances_2)-1):
        dist_inds_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [distances_2[i], distances_2[i+1]], inds_2)
        print('The number of stars with iron abundances above -2.5 is {0}'.format(np.sum(part['star'].prop('metallicity.iron',dist_inds_2) > -2.5)))
        print('The number of stars with |Z| < 3 is {0}'.format(np.sum(np.abs(part['star'].prop('star.host2.distance.principal.cylindrical',dist_inds_2)[:,2]) < 3)))
        jphi = (part['star'].prop('star.host2.distance.principal',dist_inds_2)[:,0]*part['star'].prop('star.host2.velocity.principal',dist_inds_2)[:,1] - part['star'].prop('star.host2.velocity.principal',dist_inds_2)[:,0]*part['star'].prop('star.host2.distance.principal',dist_inds_2)[:,1])/(8*data_2['vrot.z0'])
        prograde_2[i] = np.sum(jphi > 0)
        retrograde_2[i] = np.sum(jphi < 0)
        asymmetry_2[i] = prograde_2[i]/retrograde_2[i]
    #
    d_2 = dict()
    d_2['asymmetry'] = asymmetry_2
    d_2['num.prograde'] = prograde_2
    d_2['num.retrograde'] = retrograde_2
    d_2['distance.array'] = distances_2
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_halo_asym_full', dict_or_array_to_write=d_2, verbose=True)
