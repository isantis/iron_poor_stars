#!/usr/bin/python3

"""
 =======================================
 = Metallicity vs "Asymmetry" (binned) =
 =======================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot fraction of prograde to retrograde stars vs different metallicities
        - This is done for Jphi > 0 / Jphi < 0

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
gal1 = 'Romulus'
#
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
#
if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    #
    # Read in the particle data
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=False, assign_formation_coordinates=True)
    #
    # Calculate the rotational speed of the galaxy using all stars in spatial selection
    # Get indices of all stars in spatial selection
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds_1)
    #
    # Set up empty array to save to
    asym_fe_poor_1 = np.zeros(len(metallicities)-1)
    # Loop over metallicity
    for i in range(0, len(metallicities)-1):
        # Select stars below metallicity threshold
        metal_inds_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_1)
        #
        if len(metal_inds_1) != 0:
            if len(metal_inds_1) == 1:
                # Calculate asymmetry
                jphi_1 = (part['star'].prop('star.host1.distance.principal', metal_inds_1)[0][0]*part['star'].prop('star.host1.velocity.principal', metal_inds_1)[0][1] - part['star'].prop('star.host1.velocity.principal', metal_inds_1)[0][0]*part['star'].prop('star.host1.distance.principal', metal_inds_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0)
                neg_mask_1 = (jphi_1 < 0)
            else:
                # Calculate asymmetry
                jphi_1 = (part['star'].prop('star.host1.distance.principal', metal_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal', metal_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal', metal_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal', metal_inds_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0)
                neg_mask_1 = (jphi_1 < 0)
                #
            if np.sum(neg_mask_1) != 0:
                asym_fe_poor_1[i] =  np.sum(pos_mask_1)/np.sum(neg_mask_1)
            else:
                asym_fe_poor_1[i] = np.nan
        else:
            asym_fe_poor_1[i] = np.nan
    #
    # Save the data so that I can plot it later with asymmetry_vs_metallicity_plot.py
    d_1 = dict()
    d_1['asymmetry.fe.poor'] = asym_fe_poor_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_fe_poor_bin_lite', dict_or_array_to_write=d_1, verbose=True)

if num_gal == 2:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    #
    # Read in the particle data
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=False, assign_formation_coordinates=True)
    #
    # Calculate the rotational speed of the galaxy using all stars in spatial selection
    # Get indices of all stars in spatial selection
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds_1)
    #
    inds_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12])
    inds_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], inds_2)
    #
    # Set up empty array to save to
    asym_fe_poor_1 = np.zeros(len(metallicities)-1)
    asym_fe_poor_2 = np.zeros(len(metallicities)-1)
    # Loop over metallicity
    for i in range(0, len(metallicities)-1):
        # Select stars below metallicity threshold
        metal_inds_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_1)
        #
        if len(metal_inds_1) != 0:
            if len(metal_inds_1) == 1:
                # Calculate asymmetry
                jphi_1 = (part['star'].prop('star.host1.distance.principal', metal_inds_1)[0][0]*part['star'].prop('star.host1.velocity.principal', metal_inds_1)[0][1] - part['star'].prop('star.host1.velocity.principal', metal_inds_1)[0][0]*part['star'].prop('star.host1.distance.principal', metal_inds_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0)
                neg_mask_1 = (jphi_1 < 0)
            else:
                # Calculate asymmetry
                jphi_1 = (part['star'].prop('star.host1.distance.principal', metal_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal', metal_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal', metal_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal', metal_inds_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0)
                neg_mask_1 = (jphi_1 < 0)
            #
            if np.sum(neg_mask_1) != 0:
                asym_fe_poor_1[i] =  np.sum(pos_mask_1)/np.sum(neg_mask_1)
            else:
                asym_fe_poor_1[i] = np.nan
        else:
            asym_fe_poor_1[i] = np.nan
        #
        metal_inds_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_2)
        #
        if len(metal_inds_2) != 0:
            if len(metal_inds_2) == 1:
                # Calculate asymmetry
                jphi_2 = (part['star'].prop('star.host2.distance.principal', metal_inds_2)[0][0]*part['star'].prop('star.host2.velocity.principal', metal_inds_2)[0][1] - part['star'].prop('star.host2.velocity.principal', metal_inds_2)[0][0]*part['star'].prop('star.host2.distance.principal', metal_inds_2)[0][1])/(8*data_2['vrot.z0'])
                pos_mask_2 = (jphi_2 > 0)
                neg_mask_2 = (jphi_2 < 0)
            else:
                jphi_2 = (part['star'].prop('star.host2.distance.principal', metal_inds_2)[:,0]*part['star'].prop('star.host2.velocity.principal', metal_inds_2)[:,1] - part['star'].prop('star.host2.velocity.principal', metal_inds_2)[:,0]*part['star'].prop('star.host2.distance.principal', metal_inds_2)[:,1])/(8*data_2['vrot.z0'])
                pos_mask_2 = (jphi_2 > 0)
                neg_mask_2 = (jphi_2 < 0)
            #
            if np.sum(neg_mask_2) != 0:
                asym_fe_poor_2[i] =  np.sum(pos_mask_2)/np.sum(neg_mask_2)
            else:
                asym_fe_poor_2[i] = np.nan
        else:
            asym_fe_poor_2[i] = np.nan
    #
    # Save the data so that I can plot it later with asymmetry_vs_metallicity_plot.py
    d_1 = dict()
    d_1['asymmetry.fe.poor'] = asym_fe_poor_1
    #
    d_2 = dict()
    d_2['asymmetry.fe.poor'] = asym_fe_poor_2
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_fe_poor_bin_lite', dict_or_array_to_write=d_1, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asym_fe_poor_bin_lite', dict_or_array_to_write=d_2, verbose=True)
