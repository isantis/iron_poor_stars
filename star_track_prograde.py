#!/usr/bin/python3

"""
 =====================================================
 = Progenitor halo star particle tracking (Prograde) =
 =====================================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2020

 GOAL: Use the data from "star_full_track.py" and find the unique halo contributors
       for only prograde stars and prograde stars with the Sestito selection.

"""

## Import all of the tools for analysis
import time
script_start = time.time()

import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    ### Analysis
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_dictionary')
    star_data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_star_dictionary')
    print('Read in hdf5 files')
    #
    # Read in the halo tree
    start = time.time()
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=True, host_number=num_gal)
    end = time.time()
    print('Read in halo tree (with pointers) in',end-start,'seconds')

    # Set rotation speed
    v_rot1 = data_1['vrot.z0']

    # Set up main halo indices
    main_halo_inds = halt.prop('progenitor.main.indices', halt['host.index'][0]) # Goes from z = 0 back in time
    print('Set main halo progenitor indices')

    # Create an array of snapshots to loop through. This goes from 600 to 1
    ss = np.flip(np.arange(601))
    # Get Luminous halo indices
    lum_ind = ut.array.get_indices(halt['star.mass'], [1e-6, np.inf])
    # Get list of luminous halo indices for each snapshot
    # This has length 601 (from 600 to 0)
    halo_inds = [ut.array.get_indices(halt['snapshot'], ss[i], lum_ind) for i in range(0, len(ss))]

    # Create a mask for ALL prograde stars
    prograde_mask = (data_1['vel.3d.cyl'][:,1] > 0)
    all_pro_sample = star_data_1['star.memb.halo.desc.inds'][prograde_mask]


    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(all_pro_sample)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(all_pro_sample[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (all_pro_sample[i][j] == all_pro_sample[i][j+1]) and (all_pro_sample[i][j+1] == all_pro_sample[i][j+2]):
                three_count_star_halos.append(all_pro_sample[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0])
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_prograde = dict()
    halo_dict_prograde['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))

    # Save all of the properties to the dictionary as well
    halo_dict_prograde['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_prograde['num.contr'] = np.asarray(num_contr3)
    halo_dict_prograde['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_1['indices'])
    halo_dict_prograde['num.contr.frac.pro'] = np.asarray(num_contr3)/len(data_1['indices'][prograde_mask])
    halo_dict_prograde['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_prograde['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_prograde['num.unassigned.pro'] = np.asarray(num_not_in_halos_3count)
    halo_dict_prograde['num.unassigned.frac.pro'] = np.asarray(num_not_in_halos_3count/len(data_1['indices'][prograde_mask]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')

    ########################################################################################

    # Create a mask for stars in the Sestito selection
    jz = np.abs(data_1['pos.3d.cyl'][:,2]*data_1['vel.3d.cyl'][:,2])
    jphi = (data_1['pos.3d.car'][:,0]*data_1['vel.3d.car'][:,1] - data_1['vel.3d.car'][:,0]*data_1['pos.3d.car'][:,1])/(8*data_1['vrot.z0'])
    pos_mask = (jphi > 0.5) & (jphi < 1.2) & (jz > 0) & (jz < 437.5)
    sestito_pro_sample = star_data_1['star.memb.halo.desc.inds'][pos_mask]
    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(sestito_pro_sample)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(sestito_pro_sample[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (sestito_pro_sample[i][j] == sestito_pro_sample[i][j+1]) and (sestito_pro_sample[i][j+1] == sestito_pro_sample[i][j+2]):
                three_count_star_halos.append(sestito_pro_sample[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0]) # Only want to choose the most recent halo
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_sestito = dict()
    halo_dict_sestito['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))
    # Save all of the properties to the dictionary as well
    halo_dict_sestito['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_sestito['num.contr'] = np.asarray(num_contr3)
    halo_dict_sestito['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_1['indices'])
    halo_dict_sestito['num.contr.frac.pro.sest'] = np.asarray(num_contr3)/len(data_1['indices'][pos_mask])
    halo_dict_sestito['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_sestito['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_sestito['num.unassigned.pro.sest'] = np.asarray(num_not_in_halos_3count)
    halo_dict_sestito['num.unassigned.frac.pro.sest'] = np.asarray(num_not_in_halos_3count/len(data_1['indices'][pos_mask]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    # Save data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_all_prograde_data', dict_or_array_to_write=halo_dict_prograde, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data', dict_or_array_to_write=halo_dict_sestito, verbose=True)

    print('All DONE!')

#####################################################################################################

if num_gal == 2:
    ### Analysis
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_dictionary')
    star_data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_star_dictionary')
    #
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    halo_data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_halo_dictionary')
    star_data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_star_dictionary')
    print('Read in hdf5 files')
    #
    # Read in the halo tree
    start = time.time()
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=True, host_number=num_gal)
    end = time.time()
    print('Read in halo tree (with pointers) in',end-start,'seconds')

    # Set up main halo indices
    main_halo_inds_1 = halt.prop('progenitor.main.indices', halt['host.index'][0]) # Goes from z = 0 back in time
    main_halo_inds_2 = halt.prop('progenitor.main.indices', halt['host2.index'][0]) # Goes from z = 0 back in time
    print('Set main halo progenitor indices')

    # Create an array of snapshots to loop through. This goes from 600 to 1
    ss = np.flip(np.arange(601))
    # Get Luminous halo indices
    lum_ind = ut.array.get_indices(halt['star.mass'], [1e-6, np.inf])
    # Get list of luminous halo indices for each snapshot
    # This has length 601 (from 600 to 0)
    halo_inds = [ut.array.get_indices(halt['snapshot'], ss[i], lum_ind) for i in range(0, len(ss))]

    # Create a mask for ALL prograde stars
    prograde_mask_1 = (data_1['vel.3d.cyl'][:,1] > 0)
    prograde_mask_2 = (data_2['vel.3d.cyl'][:,1] > 0)
    all_pro_sample_1 = star_data_1['star.memb.halo.desc.inds'][prograde_mask_1]
    all_pro_sample_2 = star_data_2['star.memb.halo.desc.inds'][prograde_mask_2]

    ####### GALAXY 1
    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(all_pro_sample_1)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(all_pro_sample_1[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (all_pro_sample_1[i][j] == all_pro_sample_1[i][j+1]) and (all_pro_sample_1[i][j+1] == all_pro_sample_1[i][j+2]):
                three_count_star_halos.append(all_pro_sample_1[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0])
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_prograde_1 = dict()
    halo_dict_prograde_1['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))
    # Save all of the properties to the dictionary as well
    halo_dict_prograde_1['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_prograde_1['num.contr'] = np.asarray(num_contr3)
    halo_dict_prograde_1['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_1['indices'])
    halo_dict_prograde_1['num.contr.frac.pro'] = np.asarray(num_contr3)/len(data_1['indices'][prograde_mask_1])
    halo_dict_prograde_1['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_prograde_1['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_prograde_1['num.unassigned.pro'] = np.asarray(num_not_in_halos_3count)
    halo_dict_prograde_1['num.unassigned.frac.pro'] = np.asarray(num_not_in_halos_3count/len(data_1['indices'][prograde_mask_1]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')

    ########################################################################################

    # Create a mask for stars in the Sestito selection
    jz_1 = np.abs(data_1['pos.3d.cyl'][:,2]*data_1['vel.3d.cyl'][:,2])
    jphi_1 = (data_1['pos.3d.car'][:,0]*data_1['vel.3d.car'][:,1] - data_1['vel.3d.car'][:,0]*data_1['pos.3d.car'][:,1])/(8*data_1['vrot.z0'])
    pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
    sestito_pro_sample_1 = star_data_1['star.memb.halo.desc.inds'][pos_mask_1]
    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(sestito_pro_sample_1)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(sestito_pro_sample_1[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (sestito_pro_sample_1[i][j] == sestito_pro_sample_1[i][j+1]) and (sestito_pro_sample_1[i][j+1] == sestito_pro_sample_1[i][j+2]):
                three_count_star_halos.append(sestito_pro_sample_1[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0])
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_sestito_1 = dict()
    halo_dict_sestito_1['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))
    # Save all of the properties to the dictionary as well
    halo_dict_sestito_1['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_sestito_1['num.contr'] = np.asarray(num_contr3)
    halo_dict_sestito_1['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_1['indices'])
    halo_dict_sestito_1['num.contr.frac.pro.sest'] = np.asarray(num_contr3)/len(data_1['indices'][pos_mask_1])
    halo_dict_sestito_1['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_sestito_1['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_sestito_1['num.unassigned.pro.sest'] = np.asarray(num_not_in_halos_3count)
    halo_dict_sestito_1['num.unassigned.frac.pro.sest'] = np.asarray(num_not_in_halos_3count/len(data_1['indices'][pos_mask_1]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    # Save data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_all_prograde_data', dict_or_array_to_write=halo_dict_prograde_1, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data', dict_or_array_to_write=halo_dict_sestito_1, verbose=True)

    ####### GALAXY 2
    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(all_pro_sample_2)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(all_pro_sample_2[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (all_pro_sample_2[i][j] == all_pro_sample_2[i][j+1]) and (all_pro_sample_2[i][j+1] == all_pro_sample_2[i][j+2]):
                three_count_star_halos.append(all_pro_sample_2[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0])
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_prograde_2 = dict()
    halo_dict_prograde_2['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))
    # Save all of the properties to the dictionary as well
    halo_dict_prograde_2['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_prograde_2['num.contr'] = np.asarray(num_contr3)
    halo_dict_prograde_2['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_2['indices'])
    halo_dict_prograde_2['num.contr.frac.pro'] = np.asarray(num_contr3)/len(data_2['indices'][prograde_mask_2])
    halo_dict_prograde_2['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_prograde_2['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_prograde_2['num.unassigned.pro'] = np.asarray(num_not_in_halos_3count)
    halo_dict_prograde_2['num.unassigned.frac.pro'] = np.asarray(num_not_in_halos_3count/len(data_2['indices'][prograde_mask_2]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')

    ########################################################################################

    # Create a mask for stars in the Sestito selection

    jz_2 = np.abs(data_2['pos.3d.cyl'][:,2]*data_2['vel.3d.cyl'][:,2])
    jphi_2 = (data_2['pos.3d.car'][:,0]*data_2['vel.3d.car'][:,1] - data_2['vel.3d.car'][:,0]*data_2['pos.3d.car'][:,1])/(8*data_2['vrot.z0'])
    pos_mask_2 = (jphi_2 > 0.5) & (jphi_2 < 1.2) & (jz_2 > 0) & (jz_2 < 437.5)
    sestito_pro_sample_2 = star_data_2['star.memb.halo.desc.inds'][pos_mask_2]
    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_3count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    three_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_3count = []
    # Loop over the star's descendant array
    for i in range(0, len(sestito_pro_sample_2)):
        # Set up temporary arrays for each star
        three_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(sestito_pro_sample_2[i])-2):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (sestito_pro_sample_2[i][j] == sestito_pro_sample_2[i][j+1]) and (sestito_pro_sample_2[i][j+1] == sestito_pro_sample_2[i][j+2]):
                three_count_star_halos.append(sestito_pro_sample_2[i][j])
        # Append the temporary array to the master array
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = (three_count_star_halos_all[i] > 0)
            member_halos_3count.append(three_count_star_halos_all[i][tm][0])
        else:
            num_not_in_halos_3count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_3count = np.unique(member_halos_3count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_sestito_2 = dict()
    halo_dict_sestito_2['unique.halo'] = np.asarray(unique_halo_3count)
    halo_snaps3 = []
    num_contr3 = []
    bound_peak3 = []
    star_peak3 = []
    # Loop over all of the elements in the unique halo arrays
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # Count the number of stars that were associated with this halo
        num_contr3.append(np.sum(unique_halo_3count[a] == member_halos_3count))
    # Save all of the properties to the dictionary as well
    halo_dict_sestito_2['unique.snap'] = np.asarray(halo_snaps3)
    halo_dict_sestito_2['num.contr'] = np.asarray(num_contr3)
    halo_dict_sestito_2['num.contr.frac.all'] = np.asarray(num_contr3)/len(data_2['indices'])
    halo_dict_sestito_2['num.contr.frac.pro.sest'] = np.asarray(num_contr3)/len(data_2['indices'][pos_mask_2])
    halo_dict_sestito_2['mass.bound.peak'] = np.asarray(bound_peak3)
    halo_dict_sestito_2['star.mass.peak'] = np.asarray(star_peak3)
    halo_dict_sestito_2['num.unassigned.pro.sest'] = np.asarray(num_not_in_halos_3count)
    halo_dict_sestito_2['num.unassigned.frac.pro.sest'] = np.asarray(num_not_in_halos_3count/len(data_2['indices'][pos_mask_2]))
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    # Save data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_all_prograde_data', dict_or_array_to_write=halo_dict_prograde_2, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_prograde_data', dict_or_array_to_write=halo_dict_sestito_2, verbose=True)

    print('All DONE!')
