#!/usr/bin/python3

"""
 ===================
 = Merger gas mass =
 ===================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Find the mass of gas within R_90 of the halo that deposits the most prograde
       metal-poor stars.

       Manually saving the data in an array in "asymmetry_vs_prop_plot.py"

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt


### Set path and initial parameters
gal1 = 'm12b'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']

if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    #
    # Find the halo ID & snapshot of the most major prograde merger
    halo_snap_1 = halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    halo_id_1 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    #
    ###### INSERT LOOP HERE
    # Find the snapshots that are 100, 200, and 300 Myr before the merger
    snap_100_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.1)][-1]
    snap_200_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.2)][-1]
    snap_300_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.3)][-1]
    #
    time_merger = snaps['time'][halo_snap_1]
    time_100_1 = snaps['time'][snap_100_1]
    time_200_1 = snaps['time'][snap_200_1]
    time_300_1 = snaps['time'][snap_300_1]
    #
    # Find the halo indices 100, 200, and 300 Myr before the merger
    halo_100_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_100_1]
    halo_200_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_200_1]
    halo_300_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_300_1]
    #
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part_100 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_100_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    part_200 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_200_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    part_300 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_300_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    mask_100_1 = (np.linalg.norm(part_100['gas']['position'] - halt['position'][halo_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][halo_100_1])
    mask_200_1 = (np.linalg.norm(part_200['gas']['position'] - halt['position'][halo_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][halo_200_1])
    mask_300_1 = (np.linalg.norm(part_300['gas']['position'] - halt['position'][halo_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][halo_300_1])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # 100 Myr
    v_mask_100_1 = (np.linalg.norm(part_100['gas']['velocity'][mask_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('star.vel.std.50', halo_100_1))
    v_mask_100_2 = (np.linalg.norm(part_100['gas']['velocity'][mask_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('vel.std', halo_100_1))
    v_mask_100_3 = (np.linalg.norm(part_100['gas']['velocity'][mask_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('vel.circ.max', halo_100_1))
    # 200 Myr
    v_mask_200_1 = (np.linalg.norm(part_200['gas']['velocity'][mask_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('star.vel.std.50', halo_200_1))
    v_mask_200_2 = (np.linalg.norm(part_200['gas']['velocity'][mask_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('vel.std', halo_200_1))
    v_mask_200_3 = (np.linalg.norm(part_200['gas']['velocity'][mask_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('vel.circ.max', halo_200_1))
    # 300 Myr
    v_mask_300_1 = (np.linalg.norm(part_300['gas']['velocity'][mask_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('star.vel.std.50', halo_300_1))
    v_mask_300_2 = (np.linalg.norm(part_300['gas']['velocity'][mask_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('vel.std', halo_300_1))
    v_mask_300_3 = (np.linalg.norm(part_300['gas']['velocity'][mask_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('vel.circ.max', halo_300_1))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    d_1 = dict()
    # 100 Myr
    d_1['100.Myr.star.disp'] = np.sum(part_100['gas']['mass'][mask_100_1][v_mask_100_1])
    d_1['100.Myr.dark.disp'] = np.sum(part_100['gas']['mass'][mask_100_1][v_mask_100_2])
    d_1['100.Myr.dark.circ'] = np.sum(part_100['gas']['mass'][mask_100_1][v_mask_100_3])
    # 200 Myr
    d_1['200.Myr.star.disp'] = np.sum(part_200['gas']['mass'][mask_200_1][v_mask_200_1])
    d_1['200.Myr.dark.disp'] = np.sum(part_200['gas']['mass'][mask_200_1][v_mask_200_2])
    d_1['200.Myr.dark.circ'] = np.sum(part_200['gas']['mass'][mask_200_1][v_mask_200_3])
    # 300 Myr
    d_1['300.Myr.star.disp'] = np.sum(part_300['gas']['mass'][mask_300_1][v_mask_300_1])
    d_1['300.Myr.dark.disp'] = np.sum(part_300['gas']['mass'][mask_300_1][v_mask_300_2])
    d_1['300.Myr.dark.circ'] = np.sum(part_300['gas']['mass'][mask_300_1][v_mask_300_3])
    #
    # Save the dictionary to a file
    #ut.io.file_hdf5()
    #
    # Create a plot showing the gas masses at the different times for the different velocity selections
    times = np.array([time_100_1, time_200_1, time_300_1])
    #
    fig = plt.figure(figsize=(10, 8))
    #
    plt.vlines(x=time_merger, ymin=0, ymax=1e12, color='k', linestyle=':')
    #
    plt.scatter(times[0], d_1['100.Myr.star.disp'], c=colors[0], marker='*', s=55, label='star.vel.std.50')
    plt.scatter(times[0], d_1['100.Myr.dark.disp'], c=colors[0], marker='o', s=55, label='vel.std')
    plt.scatter(times[0], d_1['100.Myr.dark.circ'], c=colors[0], marker='s', s=55, label='vel.circ.max')
    #
    plt.scatter(times[1], d_1['200.Myr.star.disp'], c=colors[1], marker='*', s=55)
    plt.scatter(times[1], d_1['200.Myr.dark.disp'], c=colors[1], marker='o', s=55)
    plt.scatter(times[1], d_1['200.Myr.dark.circ'], c=colors[1], marker='s', s=55)
    #
    plt.scatter(times[2], d_1['300.Myr.star.disp'], c=colors[3], marker='*', s=55)
    plt.scatter(times[2], d_1['300.Myr.dark.disp'], c=colors[3], marker='o', s=55)
    plt.scatter(times[2], d_1['300.Myr.dark.circ'], c=colors[3], marker='s', s=55)
    #
    # Label stuff
    plt.xlabel('time [Gyr]', fontsize=20)
    plt.ylabel('Gas Mass within R$_{\\rm 90}$', fontsize=20)
    plt.title(gal1, fontsize=24)
    plt.legend(prop={'size': 18}, loc='best')
    plt.tick_params(axis='x', which='major', labelsize=20)
    plt.tick_params(axis='y', which='major', labelsize=20)
    plt.xlim(time_merger+0.1, times[2]-0.1)
    plt.ylim(1e7, 1e10)
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal1+'_merger_gas_masses.pdf')
    plt.close()
    #
    """
    # Create time step array
    times = np.array([0.1, 0.2, 0.3])
    snapshots_1 = np.zeros(len(times))
    halos_1 = np.zeros(len(times))
    #
    # Loop through the three times
    for i in range(0, len(times)):
        # Get snapshot some time before the merger
        snap_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-times[i])][-1]
        #
        # Find the halo index at this snapshot
        halo_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_1]
    """
