#!/usr/bin/python3

"""
 ===============================
 = Merger to insitu comparison =
 ===============================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot the fractions of the most major, 2nd most, and 3rd most major
       merger to the fraction of in-situ star formation

"""

# Load in the tools
import numpy as np
import matplotlib
from matplotlib import pyplot as plt


### Set path and initial parameters
loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
print('Set paths')

colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']

# Set up arrays for the ratios of mergers/insitu fractions
one = np.array([2.151, 1.569, 2.254, 4.429, 1.241, 1.000, 1.287, 0.914, 2.860, 10.895, 2.256, 0.489])
two = np.array([1.062, 1.361, 0.800, 2.905, 0.494, 0.702, 0.437, 0.464, 1.950, 3.684, 1.090, 0.348])
thr = np.array([0.392, 1.014, 0.438, 2.238, 0.482, 0.667, 0.291, 0.424, 0.909, 1.070, 1.000, 0.275])
#
# Calculate the median values
one_med = np.nanmedian(one)
two_med = np.nanmedian(two)
thr_med = np.nanmedian(thr)

# Find the one and two sigma scatter for these points
onesigp = 84.13
onesigm = 15.87
twosigp = 100
twosigm = 0
#twosigp = 97.72
#twosigm = 2.28
# major merger scatter
sigma_one_op = np.nanpercentile(one, onesigp)
sigma_one_om = np.nanpercentile(one, onesigm)
sigma_one_tp = np.nanpercentile(one, twosigp)
sigma_one_tm = np.nanpercentile(one, twosigm)
# 2nd major merger scatter
sigma_two_op = np.nanpercentile(two, onesigp)
sigma_two_om = np.nanpercentile(two, onesigm)
sigma_two_tp = np.nanpercentile(two, twosigp)
sigma_two_tm = np.nanpercentile(two, twosigm)
# 3rd major merger scatter
sigma_thr_op = np.nanpercentile(thr, onesigp)
sigma_thr_om = np.nanpercentile(thr, onesigm)
sigma_thr_tp = np.nanpercentile(thr, twosigp)
sigma_thr_tm = np.nanpercentile(thr, twosigm)

# Plot the data
xs = np.arange(1, 4)
#
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
# errorbars
plt.errorbar(xs[0], one_med, yerr=np.array([[one_med-sigma_one_tm], [sigma_one_tp-one_med]]), color=colors[0], lw=8, alpha=0.30)
plt.errorbar(xs[0], one_med, yerr=np.array([[one_med-sigma_one_om], [sigma_one_op-one_med]]), color=colors[0], lw=8, alpha=0.80)
plt.arrow(xs[0]+0.1, 3.5, 0, 0.35, width=0.03, head_width=0.07, head_length=0.1, length_includes_head=True, color=colors[0], alpha=1.0,zorder=10)
plt.errorbar(xs[1], two_med, yerr=np.array([[two_med-sigma_two_tm], [sigma_two_tp-two_med]]), color=colors[1], lw=8, alpha=0.30)
plt.errorbar(xs[1], two_med, yerr=np.array([[two_med-sigma_two_om], [sigma_two_op-two_med]]), color=colors[1], lw=8, alpha=0.80)
plt.errorbar(xs[2], thr_med, yerr=np.array([[thr_med-sigma_thr_tm], [sigma_thr_tp-thr_med]]), color=colors[3], lw=8, alpha=0.30)
plt.errorbar(xs[2], thr_med, yerr=np.array([[thr_med-sigma_thr_om], [sigma_thr_op-thr_med]]), color=colors[3], lw=8, alpha=0.80)
# Plot median points
plt.scatter(xs[0], one_med, color=colors[0], s=200, marker='s')
plt.scatter(xs[1], two_med, color=colors[1], s=200, marker='s')
plt.scatter(xs[2], thr_med, color=colors[3], s=200, marker='s')
#
plt.hlines(y=1, xmin=0, xmax=4, color='k', linestyle=':', alpha=0.25)
# Label stuff
my_xticks1 = ['Primary','Secondary','Tertiary']
my_xs = np.arange(1, 4)
plt.xticks(my_xs, my_xticks1, rotation=0)
plt.ylabel('Prograde $M_{\\rm merger}/M_{\\rm in-situ}$', fontsize=28)
plt.xlabel('Merger', fontsize=26)
plt.tick_params(axis='x', which='major', labelsize=22)
plt.tick_params(axis='y', which='major', labelsize=26)
ax.tick_params(axis='x', which='minor', bottom=False)
ax.tick_params(axis='x', which='minor', top=False)
plt.xlim(0.5, 3.5)
plt.yticks(np.arange(0, 11, 1))
plt.ylim(0, 4)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_vs_insitu.pdf')
plt.close()


# plot all of the points instead of scatter
full_data = np.vstack((one, two, thr))
xs = np.arange(1, 4)
#
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
# errorbars
plt.plot(xs, full_data[:,0], color=colors[0], marker='o', markersize=10, label='m12b')
plt.plot(xs, full_data[:,1], color=colors[1], marker='o', markersize=10, label='m12c')
plt.plot(xs, full_data[:,2], color=colors[6], marker='o', markersize=10, label='m12f')
plt.plot(xs, full_data[:,3], color=colors[3], marker='o', markersize=10, label='m12i')
plt.plot(xs, full_data[:,4], color=colors[4], marker='o', markersize=10, label='m12m')
plt.plot(xs, full_data[:,5], color=colors[5], marker='o', markersize=10, label='m12w')
plt.plot(xs, full_data[:,6], color=colors[0], marker='s', markersize=10, label='Romeo')
plt.plot(xs, full_data[:,7], color=colors[1], marker='s', markersize=10, label='Juliet')
plt.plot(xs, full_data[:,8], color=colors[6], marker='s', markersize=10, label='Thelma')
plt.plot(xs, full_data[:,9], color=colors[3], marker='s', markersize=10, label='Louise')
plt.plot(xs, full_data[:,10], color=colors[4], marker='s', markersize=10, label='Romulus')
plt.plot(xs, full_data[:,11], color=colors[5], marker='s', markersize=10, label='Remus')
#
plt.hlines(y=1, xmin=0, xmax=4, color='k', linestyle=':', alpha=0.25)
# Label stuff
my_xticks1 = ['Primary','Secondary','Tertiary']
my_xs = np.arange(1, 4)
plt.xticks(my_xs, my_xticks1, rotation=0)
plt.ylabel('Prograde $M_{\\rm merger}/M_{\\rm in-situ}$', fontsize=28)
plt.xlabel('Merger', fontsize=26)
plt.tick_params(axis='x', which='major', labelsize=22)
plt.tick_params(axis='y', which='major', labelsize=26)
ax.tick_params(axis='x', which='minor', bottom=False)
ax.tick_params(axis='x', which='minor', top=False)
plt.legend(ncol=2, prop={'size': 18}, loc='best')
plt.xlim(0.5, 3.5)
plt.yticks(np.arange(0, 12, 1))
plt.ylim(ymin=0, ymax=3)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_vs_insitu_all_zoom.pdf')
plt.close()
