#!/usr/bin/python3

"""
 ======================
 = Merger order plots =
 ======================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot the order of the three major mergers.

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt


### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus

loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
    simulation_dir = '/Users/isaiahsantistevan/simulation/galaxies/m12m_res7100'
else:
    plt.rcParams["font.family"] = "serif"
    home_dir = '/home1/05400/ibsantis/scripts'
print('Set paths')

# Get the time dictionary
snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)

### Read in data for each host
halo_ses_pro_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_sestito_prograde_data')
halo_ses_pro_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_sestito_prograde_data')
halo_ses_pro_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_sestito_prograde_data')
halo_ses_pro_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_sestito_prograde_data')
halo_ses_pro_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_sestito_prograde_data')
halo_ses_pro_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_sestito_prograde_data')
halo_ses_pro_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_sestito_prograde_data')
halo_ses_pro_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_sestito_prograde_data')
halo_ses_pro_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_sestito_prograde_data')
halo_ses_pro_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_sestito_prograde_data')
halo_ses_pro_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_sestito_prograde_data')
halo_ses_pro_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_sestito_prograde_data')
#
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asymmetry_r412_vz_MOIz_all_fe_poor')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asymmetry_r412_vz_MOIz_all_fe_poor')
#
# z = 0 asymmetry
asym_tot = np.array([asym_m12b['asymmetry'][0], asym_m12c['asymmetry'][0], asym_m12f['asymmetry'][0], asym_m12i['asymmetry'][0], asym_m12m['asymmetry'][0], asym_m12w['asymmetry'][0], asym_Romeo['asymmetry'][0], asym_Juliet['asymmetry'][0], asym_Thelma['asymmetry'][0], asym_Louise['asymmetry'][0], asym_Romulus['asymmetry'][0], asym_Remus['asymmetry'][0]])

# Get the times of the primary, secondary, and tertiary mergers
# Example of how this was done is below...
"""
m1_snap_m12b = halo_ses_pro_m12b['unique.snap'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[0])[0][0]]
m2_snap_m12b = halo_ses_pro_m12b['unique.snap'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[1])[0][0]]
m3_snap_m12b = halo_ses_pro_m12b['unique.snap'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[2])[0][0]]
mergers_m12b = snaps['time'][np.array([m1_snap_m12b, m2_snap_m12b, m3_snap_m12b])]
mergers_rank_m12b = np.argsort(mergers_m12b)+1
"""
m12b_order = np.array([2,3,1])
m12c_order = np.array([3,1,2])
m12f_order = np.array([3,1,2])
m12i_order = np.array([3,1,2])
m12m_order = np.array([3,2,1])
m12w_order = np.array([2,1,3])
Romeo_order = np.array([3,1,2])
Juliet_order = np.array([1,2,3])
Thelma_order = np.array([2,3,1])
Louise_order = np.array([1,3,2])
Romulus_order = np.array([2,3,1])
Remus_order = np.array([1,2,3])

# Make the figure
xs = np.array([1, 2, 3])
s1 = 150
s2 = 100
s3 = 50
#
plt.figure(1)
plt.figure(figsize=(10, 8))
#
# Plot the points for each host
plt.scatter(xs[0], asym_tot[0], c='None', edgecolors=colors[0], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[1], asym_tot[0], c='None', edgecolors=colors[0], marker='o', s=s3, alpha=0.7)
plt.scatter(xs[2], asym_tot[0], c=colors[0], marker='o', s=s1, alpha=0.7, label='m12b')
#
plt.scatter(xs[0], asym_tot[1], c='None', edgecolors=colors[1], marker='o', s=s3, alpha=0.7)
plt.scatter(xs[1], asym_tot[1], c=colors[1], marker='o', s=s1, alpha=0.7, label='m12c')
plt.scatter(xs[2], asym_tot[1], c='None', edgecolors=colors[1], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[2], c='None', edgecolors=colors[6], marker='o', s=s3, alpha=0.7)
plt.scatter(xs[1], asym_tot[2], c=colors[6], marker='o', s=s1, alpha=0.7, label='m12f')
plt.scatter(xs[2], asym_tot[2], c='None', edgecolors=colors[6], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[3], c='None', edgecolors=colors[3], marker='o', s=s3, alpha=0.7)
plt.scatter(xs[1], asym_tot[3], c=colors[3], marker='o', s=s1, alpha=0.7, label='m12i')
plt.scatter(xs[2], asym_tot[3], c='None', edgecolors=colors[3], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[4], c='None', edgecolors=colors[4], marker='o', s=s3, alpha=0.7)
plt.scatter(xs[1], asym_tot[4], c='None', edgecolors=colors[4], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[2], asym_tot[4], c=colors[4], marker='o', s=s1, alpha=0.7, label='m12m')
plt.arrow(xs[0], 3.2, 0, 0.25, width=.025, head_width=0.07, head_length=0.1, length_includes_head=True, color=colors[4], fill=False, alpha=1.0, zorder=10)
plt.arrow(xs[1], 3.2, 0, 0.25, width=.025, head_width=0.07, head_length=0.1, length_includes_head=True, color=colors[4], fill=False, hatch='xxxxx', alpha=1.0, zorder=10)
plt.arrow(xs[2], 3.2, 0, 0.25, width=.025, head_width=0.07, head_length=0.1, length_includes_head=True, color=colors[4], alpha=1.0, zorder=10)
#
plt.scatter(xs[0], asym_tot[5], c='None', edgecolors=colors[5], marker='o', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[1], asym_tot[5], c=colors[5], marker='o', s=s1, alpha=0.7, label='m12w')
plt.scatter(xs[2], asym_tot[5], c='None', edgecolors=colors[5], marker='o', s=s3, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[6], c='None', edgecolors=colors[0], marker='s', s=s3, alpha=0.7)
plt.scatter(xs[1], asym_tot[6], c=colors[0], marker='s', s=s1, alpha=0.7, label='Romeo')
plt.scatter(xs[2], asym_tot[6], c='None', edgecolors=colors[0], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[7], c=colors[1], marker='s', s=s1, alpha=0.7, label='Juliet')
plt.scatter(xs[1], asym_tot[7], c='None', edgecolors=colors[1], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[2], asym_tot[7], c='None', edgecolors=colors[1], marker='s', s=s3, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[8], c='None', edgecolors=colors[6], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[1], asym_tot[8], c='None', edgecolors=colors[6], marker='s', s=s3, alpha=0.7)
plt.scatter(xs[2], asym_tot[8], c=colors[6], marker='s', s=s1, alpha=0.7, label='Thelma')
#
plt.scatter(xs[0], asym_tot[9], c=colors[3], marker='s', s=s1, alpha=0.7, label='Louise')
plt.scatter(xs[1], asym_tot[9], c='None', edgecolors=colors[3], marker='s', s=s3, alpha=0.7)
plt.scatter(xs[2], asym_tot[9], c='None', edgecolors=colors[3], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
#
plt.scatter(xs[0], asym_tot[10], c='None', edgecolors=colors[4], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[1], asym_tot[10], c='None', edgecolors=colors[4], marker='s', s=s3, alpha=0.7)
plt.scatter(xs[2], asym_tot[10], c=colors[4], marker='s', s=s1, alpha=0.7, label='Romulus')
#
plt.scatter(xs[0], asym_tot[11], c=colors[5], marker='s', s=s1, alpha=0.7, label='Remus')
plt.scatter(xs[1], asym_tot[11], c='None', edgecolors=colors[5], marker='s', hatch='xxxxx', s=s2, alpha=0.7)
plt.scatter(xs[2], asym_tot[11], c='None', edgecolors=colors[5], marker='s', s=s3, alpha=0.7)
#
my_xticks1 = ['First','Second','Third']
my_xs = np.arange(1, 4)
plt.xticks(my_xs, my_xticks1, rotation=25)
plt.yticks(np.arange(1, 10, 1))
plt.ylim(0.5,3.5)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
plt.legend(ncol=2, prop={'size': 18}, loc='best', bbox_to_anchor=(0.5,0.4))
plt.tick_params(axis='both', which='major', labelsize=24)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_order_vs_asymmetry.pdf')
plt.close()
