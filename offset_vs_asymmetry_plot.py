#!/usr/bin/python3

"""
 ==================================
 = Asymmetry vs offset angle plot =
 ==================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot the offset angles vs asymmetry values

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import gridspec
from brokenaxes import brokenaxes


### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus

loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
    snaps = ut.simulation.read_snapshot_times(directory=home_dir+'/galaxies/m12m_res7100/')
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    snaps = ut.simulation.read_snapshot_times(directory='/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/m12b_res7100/')
print('Set paths')

### Read in data for each host
# m12b
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_primary_merger')
halo_ses_pro_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_sestito_prograde_data')
# m12c
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_primary_merger')
halo_ses_pro_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_sestito_prograde_data')
# m12f
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_primary_merger')
halo_ses_pro_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_sestito_prograde_data')
# m12i
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_primary_merger')
halo_ses_pro_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_sestito_prograde_data')
# m12m
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_primary_merger')
halo_ses_pro_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_sestito_prograde_data')
# m12w
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_primary_merger')
halo_ses_pro_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_sestito_prograde_data')
# Romeo
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_primary_merger')
halo_ses_pro_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_sestito_prograde_data')
# Juliet
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_primary_merger')
halo_ses_pro_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_sestito_prograde_data')
# Thelma
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_primary_merger')
halo_ses_pro_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_sestito_prograde_data')
# Louise
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_primary_merger')
halo_ses_pro_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_sestito_prograde_data')
# Romulus
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_primary_merger')
halo_ses_pro_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_sestito_prograde_data')
# Remus
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asymmetry_r412_vz_MOIz_all_fe_poor')
merger_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_primary_merger')
halo_ses_pro_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_sestito_prograde_data')

# Read in snapshot dictionary and set lookback time array
lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)

# Get the merger times in terms of lookback time
merger_tlb_m12b = snaps['time'][600] - snaps['time'][halo_ses_pro_m12b['unique.snap'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[0])[0][0]]]
merger_tlb_m12c = snaps['time'][600] - snaps['time'][halo_ses_pro_m12c['unique.snap'][np.where(halo_ses_pro_m12c['num.contr'] == np.flip(np.sort(halo_ses_pro_m12c['num.contr']))[0])[0][0]]]
merger_tlb_m12f = snaps['time'][600] - snaps['time'][halo_ses_pro_m12f['unique.snap'][np.where(halo_ses_pro_m12f['num.contr'] == np.flip(np.sort(halo_ses_pro_m12f['num.contr']))[0])[0][0]]]
merger_tlb_m12i = snaps['time'][600] - snaps['time'][halo_ses_pro_m12i['unique.snap'][np.where(halo_ses_pro_m12i['num.contr'] == np.flip(np.sort(halo_ses_pro_m12i['num.contr']))[0])[0][0]]]
merger_tlb_m12m = snaps['time'][600] - snaps['time'][halo_ses_pro_m12m['unique.snap'][np.where(halo_ses_pro_m12m['num.contr'] == np.flip(np.sort(halo_ses_pro_m12m['num.contr']))[0])[0][0]]]
merger_tlb_m12w = snaps['time'][600] - snaps['time'][halo_ses_pro_m12w['unique.snap'][np.where(halo_ses_pro_m12w['num.contr'] == np.flip(np.sort(halo_ses_pro_m12w['num.contr']))[0])[0][0]]]
merger_tlb_Romeo = snaps['time'][600] - snaps['time'][halo_ses_pro_Romeo['unique.snap'][np.where(halo_ses_pro_Romeo['num.contr'] == np.flip(np.sort(halo_ses_pro_Romeo['num.contr']))[0])[0][0]]]
merger_tlb_Juliet = snaps['time'][600] - snaps['time'][halo_ses_pro_Juliet['unique.snap'][np.where(halo_ses_pro_Juliet['num.contr'] == np.flip(np.sort(halo_ses_pro_Juliet['num.contr']))[0])[0][0]]]
merger_tlb_Thelma = snaps['time'][600] - snaps['time'][halo_ses_pro_Thelma['unique.snap'][np.where(halo_ses_pro_Thelma['num.contr'] == np.flip(np.sort(halo_ses_pro_Thelma['num.contr']))[0])[0][0]]]
merger_tlb_Louise = snaps['time'][600] - snaps['time'][halo_ses_pro_Louise['unique.snap'][np.where(halo_ses_pro_Louise['num.contr'] == np.flip(np.sort(halo_ses_pro_Louise['num.contr']))[0])[0][0]]]
merger_tlb_Romulus = snaps['time'][600] - snaps['time'][halo_ses_pro_Romulus['unique.snap'][np.where(halo_ses_pro_Romulus['num.contr'] == np.flip(np.sort(halo_ses_pro_Romulus['num.contr']))[0])[0][0]]]
merger_tlb_Remus = snaps['time'][600] - snaps['time'][halo_ses_pro_Remus['unique.snap'][np.where(halo_ses_pro_Remus['num.contr'] == np.flip(np.sort(halo_ses_pro_Remus['num.contr']))[0])[0][0]]]

# Mask out lookback times before the merger
mask_tlb_m12b = merger_m12b['tlb.pro'] > merger_tlb_m12b
mask_tlb_m12c = merger_m12c['tlb.pro'] > merger_tlb_m12c
mask_tlb_m12f = merger_m12f['tlb.pro'] > merger_tlb_m12f
mask_tlb_m12i = merger_m12i['tlb.pro'] > merger_tlb_m12i
mask_tlb_m12m = merger_m12m['tlb.pro'] > merger_tlb_m12m
mask_tlb_m12w = merger_m12w['tlb.pro'] > merger_tlb_m12w
mask_tlb_Romeo = merger_Romeo['tlb.pro'] > merger_tlb_Romeo
mask_tlb_Juliet = merger_Juliet['tlb.pro'] > merger_tlb_Juliet
mask_tlb_Thelma = merger_Thelma['tlb.pro'] > merger_tlb_Thelma
mask_tlb_Louise = merger_Louise['tlb.pro'] > merger_tlb_Louise
mask_tlb_Romulus = merger_Romulus['tlb.pro'] > merger_tlb_Romulus
mask_tlb_Remus = merger_Remus['tlb.pro'] > merger_tlb_Remus
#
# Get the times that are within 500 Myr before the merger
mask_m12b = (merger_m12b['tlb.pro'][mask_tlb_m12b] >= merger_m12b['tlb.pro'][mask_tlb_m12b][-1]) & (merger_m12b['tlb.pro'][mask_tlb_m12b] < (merger_m12b['tlb.pro'][mask_tlb_m12b][-1] + 0.5))
mask_m12c = (merger_m12c['tlb.pro'][mask_tlb_m12c] >= merger_m12c['tlb.pro'][mask_tlb_m12c][-1]) & (merger_m12c['tlb.pro'][mask_tlb_m12c] < (merger_m12c['tlb.pro'][mask_tlb_m12c][-1] + 0.5))
mask_m12f = (merger_m12f['tlb.pro'][mask_tlb_m12f] >= merger_m12f['tlb.pro'][mask_tlb_m12f][-1]) & (merger_m12f['tlb.pro'][mask_tlb_m12f] < (merger_m12f['tlb.pro'][mask_tlb_m12f][-1] + 0.5))
mask_m12i = (merger_m12i['tlb.pro'][mask_tlb_m12i] >= merger_m12i['tlb.pro'][mask_tlb_m12i][-1]) & (merger_m12i['tlb.pro'][mask_tlb_m12i] < (merger_m12i['tlb.pro'][mask_tlb_m12i][-1] + 0.5))
mask_m12m = (merger_m12m['tlb.pro'][mask_tlb_m12m] >= merger_m12m['tlb.pro'][mask_tlb_m12m][-1]) & (merger_m12m['tlb.pro'][mask_tlb_m12m] < (merger_m12m['tlb.pro'][mask_tlb_m12m][-1] + 0.5))
mask_m12w = (merger_m12w['tlb.pro'][mask_tlb_m12w] >= merger_m12w['tlb.pro'][mask_tlb_m12w][-1]) & (merger_m12w['tlb.pro'][mask_tlb_m12w] < (merger_m12w['tlb.pro'][mask_tlb_m12w][-1] + 0.5))
mask_Romeo = (merger_Romeo['tlb.pro'][mask_tlb_Romeo] >= merger_Romeo['tlb.pro'][mask_tlb_Romeo][-1]) & (merger_Romeo['tlb.pro'][mask_tlb_Romeo] < (merger_Romeo['tlb.pro'][mask_tlb_Romeo][-1] + 0.5))
mask_Juliet = (merger_Juliet['tlb.pro'][mask_tlb_Juliet] >= merger_Juliet['tlb.pro'][mask_tlb_Juliet][-1]) & (merger_Juliet['tlb.pro'][mask_tlb_Juliet] < (merger_Juliet['tlb.pro'][mask_tlb_Juliet][-1] + 0.5))
mask_Thelma = (merger_Thelma['tlb.pro'][mask_tlb_Thelma] >= merger_Thelma['tlb.pro'][mask_tlb_Thelma][-1]) & (merger_Thelma['tlb.pro'][mask_tlb_Thelma] < (merger_Thelma['tlb.pro'][mask_tlb_Thelma][-1] + 0.5))
mask_Louise = (merger_Louise['tlb.pro'][mask_tlb_Louise] >= merger_Louise['tlb.pro'][mask_tlb_Louise][-1]) & (merger_Louise['tlb.pro'][mask_tlb_Louise] < (merger_Louise['tlb.pro'][mask_tlb_Louise][-1] + 0.5))
mask_Romulus = (merger_Romulus['tlb.pro'][mask_tlb_Romulus] >= merger_Romulus['tlb.pro'][mask_tlb_Romulus][-1]) & (merger_Romulus['tlb.pro'][mask_tlb_Romulus] < (merger_Romulus['tlb.pro'][mask_tlb_Romulus][-1] + 0.5))
mask_Remus = (merger_Remus['tlb.pro'][mask_tlb_Remus] >= merger_Remus['tlb.pro'][mask_tlb_Remus][-1]) & (merger_Remus['tlb.pro'][mask_tlb_Remus] < (merger_Remus['tlb.pro'][mask_tlb_Remus][-1] + 0.5))
#
# Calculate the median offset angle during these times
angle_m12b = np.median(merger_m12b['theta.pro'][mask_tlb_m12b][mask_m12b])
angle_m12c = np.median(merger_m12c['theta.pro'][mask_tlb_m12c][mask_m12c])
angle_m12f = np.median(merger_m12f['theta.pro'][mask_tlb_m12f][mask_m12f])
angle_m12i = np.median(merger_m12i['theta.pro'][mask_tlb_m12i][mask_m12i])
angle_m12m = np.median(merger_m12m['theta.pro'][mask_tlb_m12m][mask_m12m])
angle_m12w = np.median(merger_m12w['theta.pro'][mask_tlb_m12w][mask_m12w])
angle_Romeo = np.median(merger_Romeo['theta.pro'][mask_tlb_Romeo][mask_Romeo])
angle_Juliet = np.median(merger_Juliet['theta.pro'][mask_tlb_Juliet][mask_Juliet])
angle_Thelma = np.median(merger_Thelma['theta.pro'][mask_tlb_Thelma][mask_Thelma])
angle_Louise = np.median(merger_Louise['theta.pro'][mask_tlb_Louise][mask_Louise])
angle_Romulus = np.median(merger_Romulus['theta.pro'][mask_tlb_Romulus][mask_Romulus])
angle_Remus = np.median(merger_Remus['theta.pro'][mask_tlb_Remus][mask_Remus])


### Gather the data together
#
# z = 0 asymmetry
asym_tot = np.array([asym_m12b['asymmetry'][0], asym_m12c['asymmetry'][0], asym_m12f['asymmetry'][0], asym_m12i['asymmetry'][0], asym_m12m['asymmetry'][0], asym_m12w['asymmetry'][0], asym_Romeo['asymmetry'][0], asym_Juliet['asymmetry'][0], asym_Thelma['asymmetry'][0], asym_Louise['asymmetry'][0], asym_Romulus['asymmetry'][0], asym_Remus['asymmetry'][0]])
#
# Median offset angles
angles = np.array([angle_m12b, angle_m12c, angle_m12f, angle_m12i, angle_m12m, angle_m12w, angle_Romeo, angle_Juliet, angle_Thelma, angle_Louise, angle_Romulus, angle_Remus])


### Generate the plots
# Host stellar mass vs asymmetry
#plt.figure(1)
plt.figure(figsize=(10, 8))
plt.scatter(angles[0], asym_tot[0], c=colors[0], alpha = 0.8, marker='o', s=100, label='m12b')
plt.scatter(angles[1], asym_tot[1], c=colors[1], alpha = 0.8, marker='o', s=100, label='m12c')
plt.scatter(angles[2], asym_tot[2], c=colors[6], alpha = 0.8, marker='o', s=100, label='m12f')
plt.scatter(angles[3], asym_tot[3], c=colors[3], alpha = 0.8, marker='o', s=100, label='m12i')
plt.scatter(angles[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=100, label='m12m')
plt.arrow(angles[4], 3.18, 0, 0.3, width=1, head_width=3, head_length=0.1, length_includes_head=True, color=colors[4], alpha=1.0,zorder=10)
plt.scatter(angles[5], asym_tot[5], c=colors[5], alpha = 0.8, marker='o', s=100, label='m12w')
plt.scatter(angles[6], asym_tot[6], c=colors[0], alpha = 0.8, marker='s', s=100, label='Romeo')
plt.scatter(angles[7], asym_tot[7], c=colors[1], alpha = 0.8, marker='s', s=100, label='Juliet')
plt.scatter(angles[8], asym_tot[8], c=colors[6], alpha = 0.8, marker='s', s=100, label='Thelma')
plt.scatter(angles[9], asym_tot[9], c=colors[3], alpha = 0.8, marker='s', s=100, label='Louise')
plt.scatter(angles[10], asym_tot[10], c=colors[4], alpha = 0.8, marker='s', s=100, label='Romulus')
plt.scatter(angles[11], asym_tot[11], c=colors[5], alpha = 0.8, marker='s', s=100, label='Remus')
plt.yticks(np.arange(1, 10, 1))
plt.ylim(0.5, 3.5)
plt.xlabel('$\\rm \\theta_{\\rm offset, median}$', fontsize=32)
plt.ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=32)
plt.legend(ncol=2, prop={'size': 18}, loc='best', bbox_to_anchor=(0.5, 0.6))
plt.tick_params(axis='both', which='major', labelsize=24)
plt.text(21, 3.25, 'r${\\rm _s} = -0.01$', fontsize=18)
plt.text(21, 3.1, 'p${\\rm _s}$-value$ = 0.97$', fontsize=18)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/offset_angle_vs_asymmetry.pdf')
plt.close()
