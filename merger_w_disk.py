#!/usr/bin/python3

"""
 ====================================
 = Prograde/Retrograde merger orbit =
 ====================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2020

 GOAL: Find out how the orbit of the primary prograde/retrograde halo
       changes as it's merging into the host (over 20 snapshots).
         - Read in the data
         - Calculate z-comp of angular momentum vector of prograde/retrograde halo
         - Normalize the angular momentum vector
         - Rotate the angular momentum vector
         - Rotate a unit vector with the host rotation tensor
         - Take a dot product of the rotated unit and angular momentum vectors

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
gal_color = colors[4]
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    gal_color2 = colors[1]
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    gal_color2 = colors[3]
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    gal_color2 = colors[5]
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    halo_ses_ret_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_retrograde_data')
    #
    # Read in the particle data at z = 0
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)

    ########## Prograde
    print('Working on prograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    id_prim = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    #
    # Get progenitor of primary merger and snapshots arrays
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)) # Goes from past indices to id_prim
    sss_pro_past = halt.prop('snapshot')[id_prim_inds]
    sss_pro_future = np.arange(snap_prim+1, 601)
    #
    # Get the angular momentum of the halo in the past, going to the future
    angs_pro_past = []
    for i in range(0,len(sss_pro_past)):
        ell_x = halt.prop('host.distance.principal', id_prim_inds[i])[1]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[1]*halt.prop('host.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[1] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_pro_past.append(temp/np.linalg.norm(temp))
    angs_pro_past = np.asarray(angs_pro_past)

    # Rotate the unit vector and angular momentum vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_pro_past = np.asarray([part.hostz['rotation'][sss_pro_past[i]][0][2] for i in range(0, len(sss_pro_past))])
    #
    units_rot_pro_future = np.asarray([part.hostz['rotation'][sss_pro_future[i]][0][2] for i in range(0, len(sss_pro_future))])
    angs_pro_future = (angs_pro_past[-1])*np.ones((len(units_rot_pro_future),3))
    #
    # Combine the past and future vectors
    sss_pro = np.hstack((sss_pro_past, sss_pro_future))
    units_rot_pro = np.vstack((units_rot_pro_past, units_rot_pro_future))
    angs_pro = np.vstack((angs_pro_past, angs_pro_future))

    # Take the dot product of the two arrays
    cos_theta_pro = np.asarray([np.dot(units_rot_pro[i], angs_pro[i]) for i in range(0, len(units_rot_pro))])
    theta_pro = np.rad2deg(np.arccos(cos_theta_pro))
    #for i in range(0, len(theta_pro)):
    #    if theta_pro[i] > 90:
    #        theta_pro[i] = 180 - theta_pro[i]


    ######################## New Retrograde stuff
    print('Working on Retrograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_ret_1['unique.snap'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]
    id_prim = halo_ses_ret_1['unique.halo'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]
    #
    # Get snapshots of interest
    sss_ret_past = np.arange(snap_prim-50, snap_prim+1)
    sss_ret_future = np.arange(snap_prim+1, 601)
    #
    # Get progenitor of primary merger
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)[:51]) # Goes from past indices to id_prim
    #
    # Get the angular momentum of the halo at snap_prim and going back 20 snapshots
    angs_ret_past = []
    for i in range(0,len(sss_ret_past)):
        ell_x = halt.prop('host.distance.principal', id_prim_inds[i])[1]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[1]*halt.prop('host.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[1] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_ret_past.append(temp/np.linalg.norm(temp))
    angs_ret_past = np.asarray(angs_ret_past)

    # Rotate the unit vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_ret_past = np.asarray([part.hostz['rotation'][sss_ret_past[i]][0][2] for i in range(0, len(sss_ret_past))])
    #
    units_rot_ret_future = np.asarray([part.hostz['rotation'][sss_ret_future[i]][0][2] for i in range(0, len(sss_ret_future))])
    angs_ret_future = (angs_ret_past[-1])*np.ones((len(units_rot_ret_future),3))
    # Combine the past and future vectors
    sss_ret = np.hstack((sss_ret_past, sss_ret_future))
    units_rot_ret = np.vstack((units_rot_ret_past, units_rot_ret_future))
    angs_ret = np.vstack((angs_ret_past, angs_ret_future))

    # Take the dot product of the two arrays
    cos_theta_ret = np.asarray([np.dot(units_rot_ret[i], angs_ret[i]) for i in range(0, len(units_rot_ret))])
    theta_ret = np.rad2deg(np.arccos(cos_theta_ret))
    #for i in range(0, len(theta_ret)):
    #    if theta_ret[i] > 90:
    #        theta_ret[i] = 180 - theta_ret[i]


    ############################
    # Plot the dot product
    lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
    plt.rcParams["font.family"] = "serif"
    #
    merger_pro = snaps['time'][600] - snaps['time'][halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]]
    merger_ret = snaps['time'][600] - snaps['time'][halo_ses_ret_1['unique.snap'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]]
    #
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(211)
    plt.plot(lookback[sss_pro], theta_pro, color=gal_color, label='Prograde')
    plt.arrow(x=merger_pro, y=5, dx=0, dy=20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_pro][0], lookback[sss_pro][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal1, fontsize=24)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    ax = plt.subplot(212)
    plt.plot(lookback[sss_ret], theta_ret, color=gal_color, label='Retrograde')
    plt.arrow(x=merger_ret, y=60, dx=0, dy=-20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_ret][0], lookback[sss_ret][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_both_'+gal1+'.pdf')
    plt.close()
    #
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(111)
    plt.plot(lookback[sss_pro], theta_pro, color=gal_color, label='Prograde')
    plt.arrow(x=merger_pro, y=5, dx=0, dy=20, width=0.03, head_width=0.15, head_length=5, length_includes_head=True, color='k', alpha=1.0,zorder=10)
    plt.xlim(lookback[sss_pro][0], lookback[sss_pro][-1])
    plt.ylim(0, 180)
    plt.yticks(np.arange(0, 181, 20))
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    plt.title(gal1, fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_'+gal1+'.pdf')
    plt.close()

    #####################
    # Plot how stable the disk is over time
    disk_ref = part.hostz['rotation'][600][0][2]
    disk_cos_theta = np.asarray([np.dot(disk_ref, part.hostz['rotation'][i][0][2]) for i in range(0, 600)])
    disk_theta = np.rad2deg(np.arccos(disk_cos_theta))
    #for i in range(0, len(disk_theta)):
    #    if disk_theta[i] > 90:
    #        disk_theta[i] = 180 - disk_theta[i]

    plt.figure(3)
    plt.figure(figsize=(10, 8))
    plt.plot(lookback[0:600], disk_theta, color=gal_color)
    plt.xlim(lookback[0:600][0], lookback[0:600][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal1+' disk evolution', fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/disk_direction_'+gal1+'.pdf')
    plt.close()

    #####
    # Throw everything into a dictionary
    d = dict()
    d['dot.pro'] = cos_theta_pro
    d['theta.pro'] = theta_pro
    d['snap.pro'] = sss_pro
    d['tlb.pro'] = lookback[sss_pro]
    d['dot.ret'] = cos_theta_ret
    d['theta.ret'] = theta_ret
    d['snap.ret'] = sss_ret
    d['tlb.ret'] = lookback[sss_ret]
    #
    #Save the data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_primary_merger', dict_or_array_to_write=d, verbose=True)

if num_gal == 2:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    halo_ses_ret_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_retrograde_data')
    #
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    halo_ses_pro_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_prograde_data')
    halo_ses_ret_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_retrograde_data')
    #
    # Read in the particle data at z = 0
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)

    # Galaxy 1
    ########## Prograde
    print('Working on prograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    id_prim = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    #
    # Get progenitor of primary merger
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)) # Goes from past indices to id_prim
    sss_pro_past = halt.prop('snapshot')[id_prim_inds]
    sss_pro_future = np.arange(snap_prim+1, 601)
    #
    # Get the angular momentum of the halo in the past, going to the future
    angs_pro_past = []
    for i in range(0,len(sss_pro_past)):
        ell_x = halt.prop('host.distance.principal', id_prim_inds[i])[1]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[1]*halt.prop('host.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[1] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_pro_past.append(temp/np.linalg.norm(temp))
    angs_pro_past = np.asarray(angs_pro_past)

    # Rotate the unit vector and angular momentum vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_pro_past = np.asarray([part.hostz['rotation'][sss_pro_past[i]][0][2] for i in range(0, len(sss_pro_past))])
    #
    units_rot_pro_future = np.asarray([part.hostz['rotation'][sss_pro_future[i]][0][2] for i in range(0, len(sss_pro_future))])
    angs_pro_future = (angs_pro_past[-1])*np.ones((len(units_rot_pro_future),3))
    #
    # Combine the past and future vectors
    sss_pro_1 = np.hstack((sss_pro_past, sss_pro_future))
    units_rot_pro = np.vstack((units_rot_pro_past, units_rot_pro_future))
    angs_pro = np.vstack((angs_pro_past, angs_pro_future))

    # Take the dot product of the two arrays
    cos_theta_pro_1 = np.asarray([np.dot(units_rot_pro[i], angs_pro[i]) for i in range(0, len(units_rot_pro))])
    theta_pro_1 = np.rad2deg(np.arccos(cos_theta_pro_1))
    #for i in range(0, len(theta_pro_1)):
    #    if theta_pro_1[i] > 90:
    #        theta_pro_1[i] = 180 - theta_pro_1[i]


    ######################## New Retrograde stuff
    print('Working on Retrograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_ret_1['unique.snap'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]
    id_prim = halo_ses_ret_1['unique.halo'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]
    #
    # Get snapshots of interest
    sss_ret_past = np.arange(snap_prim-50, snap_prim+1)
    sss_ret_future = np.arange(snap_prim+1, 601)
    #
    # Get progenitor of primary merger
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)[:51]) # Goes from past indices to id_prim
    #
    # Get the angular momentum of the halo at snap_prim and going back 20 snapshots
    angs_ret_past = []
    for i in range(0,len(sss_ret_past)):
        ell_x = halt.prop('host.distance.principal', id_prim_inds[i])[1]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[1]*halt.prop('host.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[2] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host.distance.principal', id_prim_inds[i])[0]*halt.prop('host.velocity.principal', id_prim_inds[i])[1] - halt.prop('host.velocity.principal', id_prim_inds[i])[0]*halt.prop('host.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_ret_past.append(temp/np.linalg.norm(temp))
    angs_ret_past = np.asarray(angs_ret_past)

    # Rotate the unit vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_ret_past = np.asarray([part.hostz['rotation'][sss_ret_past[i]][0][2] for i in range(0, len(sss_ret_past))])
    #
    units_rot_ret_future = np.asarray([part.hostz['rotation'][sss_ret_future[i]][0][2] for i in range(0, len(sss_ret_future))])
    angs_ret_future = (angs_ret_past[-1])*np.ones((len(units_rot_ret_future),3))
    # Combine the past and future vectors
    sss_ret_1 = np.hstack((sss_ret_past, sss_ret_future))
    units_rot_ret = np.vstack((units_rot_ret_past, units_rot_ret_future))
    angs_ret = np.vstack((angs_ret_past, angs_ret_future))

    # Take the dot product of the two arrays
    cos_theta_ret_1 = np.asarray([np.dot(units_rot_ret[i], angs_ret[i]) for i in range(0, len(units_rot_ret))])
    theta_ret_1 = np.rad2deg(np.arccos(cos_theta_ret_1))
    #for i in range(0, len(theta_ret_1)):
    #    if theta_ret_1[i] > 90:
    #        theta_ret_1[i] = 180 - theta_ret_1[i]


    ############################
    # Plot the dot product
    lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
    plt.rcParams["font.family"] = "serif"
    #
    merger_pro = snaps['time'][600] - snaps['time'][halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]]
    merger_ret = snaps['time'][600] - snaps['time'][halo_ses_ret_1['unique.snap'][np.where(halo_ses_ret_1['num.contr'] == np.flip(np.sort(halo_ses_ret_1['num.contr']))[0])[0][0]]]
    #
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(211)
    plt.plot(lookback[sss_pro_1], theta_pro_1, color=gal_color, label='Prograde')
    plt.arrow(x=merger_pro, y=5, dx=0, dy=20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_pro_1][0], lookback[sss_pro_1][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal1, fontsize=24)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    ax = plt.subplot(212)
    plt.plot(lookback[sss_ret_1], theta_ret_1, color=gal_color, label='Retrograde')
    plt.arrow(x=merger_ret, y=60, dx=0, dy=-20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_ret_1][0], lookback[sss_ret_1][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_both_'+gal1+'.pdf')
    plt.close()
    #
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(111)
    plt.plot(lookback[sss_pro_1], theta_pro_1, color=gal_color, label='Prograde')
    plt.arrow(x=merger_pro, y=55, dx=0, dy=-20, width=0.03, head_width=0.15, head_length=5, length_includes_head=True, color='k', alpha=1.0,zorder=10)
    plt.xlim(lookback[sss_pro_1][0], lookback[sss_pro_1][-1])
    plt.ylim(0, 180)
    plt.yticks(np.arange(0, 181, 20))
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    plt.title(gal1, fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_'+gal1+'.pdf')
    plt.close()

    #####################
    # Plot how stable the disk is over time
    disk_ref = part.hostz['rotation'][600][0][2]
    disk_cos_theta = np.asarray([np.dot(disk_ref, part.hostz['rotation'][i][0][2]) for i in range(0, 600)])
    disk_theta = np.rad2deg(np.arccos(disk_cos_theta))
    #for i in range(0, len(disk_theta)):
    #    if disk_theta[i] > 90:
    #        disk_theta[i] = 180 - disk_theta[i]

    plt.figure(3)
    plt.figure(figsize=(10, 8))
    plt.plot(lookback[0:600], disk_theta, color=gal_color)
    plt.xlim(lookback[0:600][0], lookback[0:600][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal1+' disk evolution', fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/disk_direction_'+gal1+'.pdf')
    plt.close()

    #####
    # Throw everything into a dictionary
    d_1 = dict()
    d_1['dot.pro'] = cos_theta_pro_1
    d_1['theta.pro'] = theta_pro_1
    d_1['snap.pro'] = sss_pro_1
    d_1['tlb.pro'] = lookback[sss_pro_1]
    d_1['dot.ret'] = cos_theta_ret_1
    d_1['theta.ret'] = theta_ret_1
    d_1['snap.ret'] = sss_ret_1
    d_1['tlb.ret'] = lookback[sss_ret_1]
    #
    #Save the data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_primary_merger', dict_or_array_to_write=d_1, verbose=True)

    # Galaxy 2
    ########## Prograde
    print('Working on prograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    id_prim = halo_ses_pro_2['unique.halo'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    #
    # Get progenitor of primary merger
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)) # Goes from past indices to id_prim
    sss_pro_past = halt.prop('snapshot')[id_prim_inds]
    sss_pro_future = np.arange(snap_prim+1, 601)
    #
    # Get the angular momentum of the halo in the past, going to the future
    angs_pro_past = []
    for i in range(0,len(sss_pro_past)):
        ell_x = halt.prop('host2.distance.principal', id_prim_inds[i])[1]*halt.prop('host2.velocity.principal', id_prim_inds[i])[2] - halt.prop('host2.velocity.principal', id_prim_inds[i])[1]*halt.prop('host2.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host2.distance.principal', id_prim_inds[i])[0]*halt.prop('host2.velocity.principal', id_prim_inds[i])[2] - halt.prop('host2.velocity.principal', id_prim_inds[i])[0]*halt.prop('host2.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host2.distance.principal', id_prim_inds[i])[0]*halt.prop('host2.velocity.principal', id_prim_inds[i])[1] - halt.prop('host2.velocity.principal', id_prim_inds[i])[0]*halt.prop('host2.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_pro_past.append(temp/np.linalg.norm(temp))
    angs_pro_past = np.asarray(angs_pro_past)

    # Rotate the unit vector and angular momentum vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_pro_past = np.asarray([part.hostz['rotation'][sss_pro_past[i]][1][2] for i in range(0, len(sss_pro_past))])
    #
    units_rot_pro_future = np.asarray([part.hostz['rotation'][sss_pro_future[i]][1][2] for i in range(0, len(sss_pro_future))])
    angs_pro_future = (angs_pro_past[-1])*np.ones((len(units_rot_pro_future),3))
    #
    # Combine the past and future vectors
    sss_pro_2 = np.hstack((sss_pro_past, sss_pro_future))
    units_rot_pro = np.vstack((units_rot_pro_past, units_rot_pro_future))
    angs_pro = np.vstack((angs_pro_past, angs_pro_future))

    # Take the dot product of the two arrays
    cos_theta_pro_2 = np.asarray([np.dot(units_rot_pro[i], angs_pro[i]) for i in range(0, len(units_rot_pro))])
    theta_pro_2 = np.rad2deg(np.arccos(cos_theta_pro_2))
    #for i in range(0, len(theta_pro_2)):
    #    if theta_pro_2[i] > 90:
    #        theta_pro_2[i] = 180 - theta_pro_2[i]


    ######################## New Retrograde stuff
    print('Working on Retrograde now!')

    # Get the snapshot and id of halo that contributes the most stars
    snap_prim = halo_ses_ret_2['unique.snap'][np.where(halo_ses_ret_2['num.contr'] == np.flip(np.sort(halo_ses_ret_2['num.contr']))[0])[0][0]]
    id_prim = halo_ses_ret_2['unique.halo'][np.where(halo_ses_ret_2['num.contr'] == np.flip(np.sort(halo_ses_ret_2['num.contr']))[0])[0][0]]
    #
    # Get snapshots of interest
    sss_ret_past = np.arange(snap_prim-50, snap_prim+1)
    sss_ret_future = np.arange(snap_prim+1, 601)
    #
    # Get progenitor of primary merger
    id_prim_inds = np.flip(halt.prop('progenitor.main.indices',id_prim)[:51]) # Goes from past indices to id_prim
    #
    # Get the angular momentum of the halo at snap_prim and going back 20 snapshots
    angs_ret_past = []
    for i in range(0,len(sss_ret_past)):
        ell_x = halt.prop('host2.distance.principal', id_prim_inds[i])[1]*halt.prop('host2.velocity.principal', id_prim_inds[i])[2] - halt.prop('host2.velocity.principal', id_prim_inds[i])[1]*halt.prop('host2.distance.principal', id_prim_inds[i])[2]
        ell_y = (-1)*(halt.prop('host2.distance.principal', id_prim_inds[i])[0]*halt.prop('host2.velocity.principal', id_prim_inds[i])[2] - halt.prop('host2.velocity.principal', id_prim_inds[i])[0]*halt.prop('host2.distance.principal', id_prim_inds[i])[2])
        ell_z = halt.prop('host2.distance.principal', id_prim_inds[i])[0]*halt.prop('host2.velocity.principal', id_prim_inds[i])[1] - halt.prop('host2.velocity.principal', id_prim_inds[i])[0]*halt.prop('host2.distance.principal', id_prim_inds[i])[1]
        temp = [ell_x, ell_y, ell_z]
        angs_ret_past.append(temp/np.linalg.norm(temp))
    angs_ret_past = np.asarray(angs_ret_past)

    # Rotate the unit vector for all snapshots
    unit = np.array([0.0, 0.0, 1.0])
    units_rot_ret_past = np.asarray([part.hostz['rotation'][sss_ret_past[i]][1][2] for i in range(0, len(sss_ret_past))])
    #
    units_rot_ret_future = np.asarray([part.hostz['rotation'][sss_ret_future[i]][1][2] for i in range(0, len(sss_ret_future))])
    angs_ret_future = (angs_ret_past[-1])*np.ones((len(units_rot_ret_future),3))
    # Combine the past and future vectors
    sss_ret_2 = np.hstack((sss_ret_past, sss_ret_future))
    units_rot_ret = np.vstack((units_rot_ret_past, units_rot_ret_future))
    angs_ret = np.vstack((angs_ret_past, angs_ret_future))

    # Take the dot product of the two arrays
    cos_theta_ret_2 = np.asarray([np.dot(units_rot_ret[i], angs_ret[i]) for i in range(0, len(units_rot_ret))])
    theta_ret_2 = np.rad2deg(np.arccos(cos_theta_ret_2))
    #for i in range(0, len(theta_ret_2)):
    #    if theta_ret_2[i] > 90:
    #        theta_ret_2[i] = 180 - theta_ret_2[i]


    ############################
    # Plot the dot product
    lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
    plt.rcParams["font.family"] = "serif"
    #
    merger_pro = snaps['time'][600] - snaps['time'][halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]]
    merger_ret = snaps['time'][600] - snaps['time'][halo_ses_ret_2['unique.snap'][np.where(halo_ses_ret_2['num.contr'] == np.flip(np.sort(halo_ses_ret_2['num.contr']))[0])[0][0]]]
    #
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(211)
    plt.plot(lookback[sss_pro_2], theta_pro_2, color=gal_color2, label='Prograde')
    plt.arrow(x=merger_pro, y=5, dx=0, dy=20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_pro_2][0], lookback[sss_pro_2][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal2, fontsize=24)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    ax = plt.subplot(212)
    plt.plot(lookback[sss_ret_2], theta_ret_2, color=gal_color2, label='Retrograde')
    plt.arrow(x=merger_ret, y=60, dx=0, dy=-20, width=0.05, head_width=0.2, head_length=5, length_includes_head=True, color='k', alpha=1.0)
    plt.xlim(lookback[sss_ret_2][0], lookback[sss_ret_2][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.legend(prop={'size': 16})
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_both_'+gal2+'.pdf')
    plt.close()
    #
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    ax = plt.subplot(111)
    plt.plot(lookback[sss_pro_2], theta_pro_2, color=gal_color2, label='Prograde')
    plt.arrow(x=merger_pro, y=5, dx=0, dy=20, width=0.03, head_width=0.15, head_length=5, length_includes_head=True, color='k', alpha=1.0,zorder=10)
    plt.xlim(lookback[sss_pro_2][0], lookback[sss_pro_2][-1])
    plt.ylim(0, 180)
    plt.yticks(np.arange(0, 181, 20))
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    plt.title(gal2, fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_directions_'+gal2+'.pdf')
    plt.close()

    #####################
    # Plot how stable the disk is over time
    disk_ref = part.hostz['rotation'][600][1][2]
    disk_cos_theta = np.asarray([np.dot(disk_ref, part.hostz['rotation'][i][1][2]) for i in range(0, 600)])
    disk_theta = np.rad2deg(np.arccos(disk_cos_theta))
    #for i in range(0, len(disk_theta)):
    #    if disk_theta[i] > 90:
    #        disk_theta[i] = 180 - disk_theta[i]

    plt.figure(3)
    plt.figure(figsize=(10, 8))
    plt.plot(lookback[0:600], disk_theta, color=gal_color2)
    plt.xlim(lookback[0:600][0], lookback[0:600][-1])
    plt.yticks(np.arange(0, 181, 20))
    plt.ylim(0, 180)
    plt.xlabel('Lookback time [Gyr]', fontsize=28)
    plt.ylabel('$\\theta_{\\rm offset}$', fontsize=24)
    plt.title(gal2+' disk evolution', fontsize=24)
    plt.tick_params(axis='both', which='both', labelsize=20, width=1.5)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/disk_direction_'+gal2+'.pdf')
    plt.close()

    #####
    # Throw everything into a dictionary
    d_2 = dict()
    d_2['dot.pro'] = cos_theta_pro_2
    d_2['theta.pro'] = theta_pro_2
    d_2['snap.pro'] = sss_pro_2
    d_2['tlb.pro'] = lookback[sss_pro_2]
    d_2['dot.ret'] = cos_theta_ret_2
    d_2['theta.ret'] = theta_ret_2
    d_2['snap.ret'] = sss_ret_2
    d_2['tlb.ret'] = lookback[sss_ret_2]
    #
    #Save the data to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_primary_merger', dict_or_array_to_write=d_2, verbose=True)
