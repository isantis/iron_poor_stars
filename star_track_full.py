#!/usr/bin/python3

"""
 ==========================================
 = Progenitor halo star particle tracking =
 ==========================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2020

 GOAL: Read in the halo tree
       Find out how many halos deposited the metal-poor stars into the host
       Find:
        - Halo index
        - Halo snapshot
        - Halo peak stellar mass
        - Halo peak bound mass
        - Number of stars each halo contributed to host

"""

## Import all of the tools for analysis
import time
script_start = time.time()

import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal ==1:
    ### Analysis
    # Read in the metal-poor data
    data = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    star_ind = data['indices']
    print('Read in galaxy data')
    # Read in the halo tree
    start = time.time()
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=True, host_number=num_gal)
    end = time.time()
    print('Read in halo tree (with pointers) in',end-start,'seconds')

    # Set up main halo indices
    main_halo_inds = halt.prop('progenitor.main.indices', halt['host.index'][0]) # Goes from z = 0 back in time
    print('Set main halo progenitor indices')

    # Create an array of snapshots to loop through. This goes from 600 to 1
    ss = np.flip(np.arange(601))
    # Get Luminous halo indices
    lum_ind = ut.array.get_indices(halt['star.mass'], [1e-6, np.inf])
    # Get list of luminous halo indices for each snapshot
    # This has length 601 (from 600 to 0)
    halo_inds = [ut.array.get_indices(halt['snapshot'], ss[i], lum_ind) for i in range(0, len(ss))]

    """
    For each star of interest, save an array where each element correpsonds to the
    halo index that it was a member of at that snapshot
    """
    start = time.time()
    # Initialize empty array
    star_memb_halo_inds_all = []
    # Loop over all star particles
    for n in range(0, len(star_ind)):
        # Set up array for each star particle
        star_memb_halo_inds = (-1)*np.ones(601, int)
        # Set first element to host index because they are in the host at z = 0
        star_memb_halo_inds[0] = main_halo_inds[0]
        # Loop over the other snapshots
        for i in range(1, 598): # want it to stop at 598 cause theres no pointers (or stars) at snapshot 1
            # Loop over the luminous halos at that snapshot
            for j in range(0, len(halo_inds[i])):
                # Look for the halo that the star was a member of and save it
                if star_ind[n] in halt['star.z0.indices'][halo_inds[i][j]]:
                    star_memb_halo_inds[i] = halo_inds[i][j]
        # Append each star's array to the total array
        star_memb_halo_inds_all.append(star_memb_halo_inds)
    end = time.time()
    print('Finished saving each star\'s halo member index at each snapshot in', end-start, 'seconds')


    """
    For each star, now we want a new list where we find the descendant halos of the
    halos in 'star_memb_halo_inds_all[i]'

    Each element of this new array will be the halo's last descendant before
    merging into the host
    """
    start = time.time()
    # Loop over all star particles
    star_memb_halo_inds_desc_all = []
    for n in range(0, len(star_ind)):
        # Set temporary array for each star and create an array to save descendants to
        temp_star = np.asarray(star_memb_halo_inds_all)[n]
        temp_star_forward = (-1)*np.ones(len(temp_star), int)
        # Loop over all of the elements in each star's array
        # This is essentially a loop over all snapshots (or indices of the halos the star was a member of)
        for i in range(0, len(temp_star)):
            # If the halo index is the host, set descendant index to ZERO
            if temp_star[i] in main_halo_inds:
                temp_star_forward[i] = 0
            # If the halo index is not the host, and isn't null, find its last descendant before merging
            elif temp_star[i] not in main_halo_inds and temp_star[i] != -1:
                # Get list of descendant indices (starts at current snapshot and goes forward in time)
                check = halt.prop('descendant.indices', temp_star[i])
                # Mask out the indices that are the host
                mask = np.asarray([check[j] not in main_halo_inds for j in range(0, len(check))])
                # Apply mask and grab the last element, which is the element furthest in time before merging
                temp_star_forward[i] = check[mask][-1]
            # If the index is null, set descendant index to null
            else:
                temp_star_forward[i] = -1
        # Append each descendant array to master array
        star_memb_halo_inds_desc_all.append(temp_star_forward)
    end = time.time()
    print('Finished getting progenitor halos for all stars in',end-start,'seconds')


    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_2count = 0
    num_not_in_halos_3count = 0
    num_not_in_halos_4count = 0
    num_not_in_halos_5count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    two_count_star_halos_all = []
    three_count_star_halos_all = []
    four_count_star_halos_all = []
    five_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_2count = []
    member_halos_3count = []
    member_halos_4count = []
    member_halos_5count = []
    # Loop over the star's descendant array
    for i in range(0, len(star_memb_halo_inds_desc_all)):
        # Set up temporary arrays for each star
        two_count_star_halos = []
        three_count_star_halos = []
        four_count_star_halos = []
        five_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(star_memb_halo_inds_desc_all[i])-4):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]):
                two_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]):
                three_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 4 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]):
                four_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 5 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]) and (star_memb_halo_inds_desc_all[i][j+3] == star_memb_halo_inds_desc_all[i][j+4]):
                five_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
        # Append the temporary array to the master array
        two_count_star_halos_all.append(np.asarray(two_count_star_halos))
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        four_count_star_halos_all.append(np.asarray(four_count_star_halos))
        five_count_star_halos_all.append(np.asarray(five_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Two count
        if np.any(two_count_star_halos_all[i] > 0):
            tm = np.unique(two_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(two_count_star_halos_all[i])[tm])):
                member_halos_2count.append(np.unique(two_count_star_halos_all[i])[tm][k])
        # If the arrays only contain the host or nulls, count this as a case where
        # the star did not satisfy the 'multiple snapshot' criteria
        else:
            num_not_in_halos_2count += 1
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = np.unique(three_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(three_count_star_halos_all[i])[tm])):
                member_halos_3count.append(np.unique(three_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_3count += 1
        # Four count
        if np.any(four_count_star_halos_all[i] > 0):
            tm = np.unique(four_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(four_count_star_halos_all[i])[tm])):
                member_halos_4count.append(np.unique(four_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_4count += 1
        # Five count
        if np.any(five_count_star_halos_all[i] > 0):
            tm = np.unique(five_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(five_count_star_halos_all[i])[tm])):
                member_halos_5count.append(np.unique(five_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_5count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_2count = np.unique(member_halos_2count)
    unique_halo_3count = np.unique(member_halos_3count)
    unique_halo_4count = np.unique(member_halos_4count)
    unique_halo_5count = np.unique(member_halos_5count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict = dict()
    halo_dict['unique.halo.2'] = unique_halo_2count
    halo_dict['unique.halo.3'] = unique_halo_3count
    halo_dict['unique.halo.4'] = unique_halo_4count
    halo_dict['unique.halo.5'] = unique_halo_5count
    halo_snaps2 = []
    halo_snaps3 = []
    halo_snaps4 = []
    halo_snaps5 = []
    num_contr2 = []
    num_contr3 = []
    num_contr4 = []
    num_contr5 = []
    bound_peak2 = []
    bound_peak3 = []
    bound_peak4 = []
    bound_peak5 = []
    star_peak2 = []
    star_peak3 = []
    star_peak4 = []
    star_peak5 = []
    # Loop over all of the elements in the unique halo arrays
    # 2 snapshot criteria
    for a in range(0, len(unique_halo_2count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_2count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps2.append(snap)
        bound_peak2.append(halt.prop('mass.bound.peak', halo))
        star_peak2.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind)):
                if star_ind[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr2.append(count)
        else:
            num_contr2.append(count)
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind)):
                if star_ind[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr3.append(count)
        else:
            num_contr3.append(count)
    # 4 snapshot criteria
    for a in range(0, len(unique_halo_4count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_4count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps4.append(snap)
        bound_peak4.append(halt.prop('mass.bound.peak', halo))
        star_peak4.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind)):
                if star_ind[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr4.append(count)
        else:
            num_contr4.append(count)
    # 5 snapshot criteria
    for a in range(0, len(unique_halo_5count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_5count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps5.append(snap)
        bound_peak5.append(halt.prop('mass.bound.peak', halo))
        star_peak5.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind)):
                if star_ind[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr5.append(count)
        else:
            num_contr5.append(count)
    # Save all of the properties to the dictionary as well
    halo_dict['unique.snap.2'] = np.asarray(halo_snaps2)
    halo_dict['unique.snap.3'] = np.asarray(halo_snaps3)
    halo_dict['unique.snap.4'] = np.asarray(halo_snaps4)
    halo_dict['unique.snap.5'] = np.asarray(halo_snaps5)
    halo_dict['num.contr.2'] = np.asarray(num_contr2)
    halo_dict['num.contr.3'] = np.asarray(num_contr3)
    halo_dict['num.contr.4'] = np.asarray(num_contr4)
    halo_dict['num.contr.5'] = np.asarray(num_contr5)
    halo_dict['num.contr.frac.2'] = np.asarray(num_contr2)/len(star_ind)
    halo_dict['num.contr.frac.3'] = np.asarray(num_contr3)/len(star_ind)
    halo_dict['num.contr.frac.4'] = np.asarray(num_contr4)/len(star_ind)
    halo_dict['num.contr.frac.5'] = np.asarray(num_contr5)/len(star_ind)
    halo_dict['mass.bound.peak.2'] = np.asarray(bound_peak2)
    halo_dict['mass.bound.peak.3'] = np.asarray(bound_peak3)
    halo_dict['mass.bound.peak.4'] = np.asarray(bound_peak4)
    halo_dict['mass.bound.peak.5'] = np.asarray(bound_peak5)
    halo_dict['star.mass.peak.2'] = np.asarray(star_peak2)
    halo_dict['star.mass.peak.3'] = np.asarray(star_peak3)
    halo_dict['star.mass.peak.4'] = np.asarray(star_peak4)
    halo_dict['star.mass.peak.5'] = np.asarray(star_peak5)
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    """
    Save what you want to dictionaries and write to files
    """
    # Gather all of the star data
    star_dict = dict()
    star_dict['star.memb.halo.inds'] = np.asarray(star_memb_halo_inds_all)
    star_dict['star.memb.halo.desc.inds'] = np.asarray(star_memb_halo_inds_desc_all)
    star_dict['no.2.count'] = np.asarray(num_not_in_halos_2count)
    star_dict['no.3.count'] = np.asarray(num_not_in_halos_3count)
    star_dict['no.4.count'] = np.asarray(num_not_in_halos_4count)
    star_dict['no.5.count'] = np.asarray(num_not_in_halos_5count)
    star_dict['no.2.frac'] = np.asarray(num_not_in_halos_2count/len(star_ind))
    star_dict['no.3.frac'] = np.asarray(num_not_in_halos_3count/len(star_ind))
    star_dict['no.4.frac'] = np.asarray(num_not_in_halos_4count/len(star_ind))
    star_dict['no.5.frac'] = np.asarray(num_not_in_halos_5count/len(star_ind))

    # Save to file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_dictionary', dict_or_array_to_write=halo_dict, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_star_dictionary', dict_or_array_to_write=star_dict, verbose=True)

    script_end = time.time()
    print('Total script time is', script_end-script_start,'seconds')

    print('All done!')

###############################################################################

if num_gal == 2:
    ### Analysis
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    star_ind_1 = data_1['indices']
    #
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    star_ind_2 = data_2['indices']
    print('Read in pickle files')
    #
    # Read in the halo tree
    start = time.time()
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=True, host_number=num_gal)
    end = time.time()
    print('Read in halo tree (with pointers) in',end-start,'seconds')

    # Set up main halo indices
    main_halo_inds_1 = halt.prop('progenitor.main.indices', halt['host.index'][0]) # Goes from z = 0 back in time
    main_halo_inds_2 = halt.prop('progenitor.main.indices', halt['host2.index'][0]) # Goes from z = 0 back in time
    print('Set main halo progenitor indices')

    # Create an array of snapshots to loop through. This goes from 600 to 1
    ss = np.flip(np.arange(601))
    # Get Luminous halo indices
    lum_ind = ut.array.get_indices(halt['star.mass'], [1e-6, np.inf])
    # Get list of luminous halo indices for each snapshot
    # This has length 601 (from 600 to 0)
    halo_inds = [ut.array.get_indices(halt['snapshot'], ss[i], lum_ind) for i in range(0, len(ss))]

    ########## Galaxy 1
    """
    For each star of interest, save an array where each element correpsonds to the
    halo index that it was a member of at that snapshot
    """
    start = time.time()
    # Initialize empty array
    star_memb_halo_inds_all = []
    # Loop over all star particles
    for n in range(0, len(star_ind_1)):
        # Set up array for each star particle
        star_memb_halo_inds = (-1)*np.ones(601, int)
        # Set first element to host index because they are in the host at z = 0
        star_memb_halo_inds[0] = main_halo_inds_1[0]
        # Loop over the other snapshots
        for i in range(1, 598): # want it to stop at 598 cause theres no pointers (or stars) at snapshot 1
            # Loop over the luminous halos at that snapshot
            for j in range(0, len(halo_inds[i])):
                # Look for the halo that the star was a member of and save it
                if star_ind_1[n] in halt['star.z0.indices'][halo_inds[i][j]]:
                    star_memb_halo_inds[i] = halo_inds[i][j]
        # Append each star's array to the total array
        star_memb_halo_inds_all.append(star_memb_halo_inds)
    end = time.time()
    print('Finished saving each star\'s halo member index at each snapshot in', end-start, 'seconds')


    """
    For each star, now we want a new list where we find the descendant halos of the
    halos in 'star_memb_halo_inds_all[i]'

    Each element of this new array will be the halo's last descendant before
    merging into the host
    """
    start = time.time()
    # Loop over all star particles
    star_memb_halo_inds_desc_all = []
    for n in range(0, len(star_ind_1)):
        # Set temporary array for each star and create an array to save descendants to
        temp_star = np.asarray(star_memb_halo_inds_all)[n]
        temp_star_forward = (-1)*np.ones(len(temp_star), int)
        # Loop over all of the elements in each star's array
        # This is essentially a loop over all snapshots (or indices of the halos the star was a member of)
        for i in range(0, len(temp_star)):
            # If the halo index is the host, set descendant index to ZERO
            if temp_star[i] in main_halo_inds_1:
                temp_star_forward[i] = 0
            # If the halo index is not the host, and isn't null, find its last descendant before merging
            elif temp_star[i] not in main_halo_inds_1 and temp_star[i] != -1:
                # Get list of descendant indices (starts at current snapshot and goes forward in time)
                check = halt.prop('descendant.indices', temp_star[i])
                # Mask out the indices that are the host
                mask = np.asarray([check[j] not in main_halo_inds_1 for j in range(0, len(check))])
                # Apply mask and grab the last element, which is the element furthest in time before merging
                temp_star_forward[i] = check[mask][-1]
            # If the index is null, set descendant index to null
            else:
                temp_star_forward[i] = -1
        # Append each descendant array to master array
        star_memb_halo_inds_desc_all.append(temp_star_forward)
    end = time.time()
    print('Finished getting progenitor halos for all stars in',end-start,'seconds')


    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_2count = 0
    num_not_in_halos_3count = 0
    num_not_in_halos_4count = 0
    num_not_in_halos_5count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    two_count_star_halos_all = []
    three_count_star_halos_all = []
    four_count_star_halos_all = []
    five_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_2count = []
    member_halos_3count = []
    member_halos_4count = []
    member_halos_5count = []
    # Loop over the star's descendant array
    for i in range(0, len(star_memb_halo_inds_desc_all)):
        # Set up temporary arrays for each star
        two_count_star_halos = []
        three_count_star_halos = []
        four_count_star_halos = []
        five_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(star_memb_halo_inds_desc_all[i])-4):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]):
                two_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]):
                three_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 4 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]):
                four_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 5 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]) and (star_memb_halo_inds_desc_all[i][j+3] == star_memb_halo_inds_desc_all[i][j+4]):
                five_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
        # Append the temporary array to the master array
        two_count_star_halos_all.append(np.asarray(two_count_star_halos))
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        four_count_star_halos_all.append(np.asarray(four_count_star_halos))
        five_count_star_halos_all.append(np.asarray(five_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Two count
        if np.any(two_count_star_halos_all[i] > 0):
            tm = np.unique(two_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(two_count_star_halos_all[i])[tm])):
                member_halos_2count.append(np.unique(two_count_star_halos_all[i])[tm][k])
        # If the arrays only contain the host or nulls, count this as a case where
        # the star did not satisfy the 'multiple snapshot' criteria
        else:
            num_not_in_halos_2count += 1
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = np.unique(three_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(three_count_star_halos_all[i])[tm])):
                member_halos_3count.append(np.unique(three_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_3count += 1
        # Four count
        if np.any(four_count_star_halos_all[i] > 0):
            tm = np.unique(four_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(four_count_star_halos_all[i])[tm])):
                member_halos_4count.append(np.unique(four_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_4count += 1
        # Five count
        if np.any(five_count_star_halos_all[i] > 0):
            tm = np.unique(five_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(five_count_star_halos_all[i])[tm])):
                member_halos_5count.append(np.unique(five_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_5count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_2count = np.unique(member_halos_2count)
    unique_halo_3count = np.unique(member_halos_3count)
    unique_halo_4count = np.unique(member_halos_4count)
    unique_halo_5count = np.unique(member_halos_5count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_1 = dict()
    halo_dict_1['unique.halo.2'] = np.asarray(unique_halo_2count)
    halo_dict_1['unique.halo.3'] = np.asarray(unique_halo_3count)
    halo_dict_1['unique.halo.4'] = np.asarray(unique_halo_4count)
    halo_dict_1['unique.halo.5'] = np.asarray(unique_halo_5count)
    halo_snaps2 = []
    halo_snaps3 = []
    halo_snaps4 = []
    halo_snaps5 = []
    num_contr2 = []
    num_contr3 = []
    num_contr4 = []
    num_contr5 = []
    bound_peak2 = []
    bound_peak3 = []
    bound_peak4 = []
    bound_peak5 = []
    star_peak2 = []
    star_peak3 = []
    star_peak4 = []
    star_peak5 = []
    # Loop over all of the elements in the unique halo arrays
    # 2 snapshot criteria
    for a in range(0, len(unique_halo_2count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_2count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps2.append(snap)
        bound_peak2.append(halt.prop('mass.bound.peak', halo))
        star_peak2.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_1)):
                if star_ind_1[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr2.append(count)
        else:
            num_contr2.append(count)
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_1)):
                if star_ind_1[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr3.append(count)
        else:
            num_contr3.append(count)
    # 4 snapshot criteria
    for a in range(0, len(unique_halo_4count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_4count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps4.append(snap)
        bound_peak4.append(halt.prop('mass.bound.peak', halo))
        star_peak4.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_1)):
                if star_ind_1[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr4.append(count)
        else:
            num_contr4.append(count)
    # 5 snapshot criteria
    for a in range(0, len(unique_halo_5count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_5count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps5.append(snap)
        bound_peak5.append(halt.prop('mass.bound.peak', halo))
        star_peak5.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_1)):
                if star_ind_1[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr5.append(count)
        else:
            num_contr5.append(count)
    # Save all of the properties to the dictionary as well
    halo_dict_1['unique.snap.2'] = np.asarray(halo_snaps2)
    halo_dict_1['unique.snap.3'] = np.asarray(halo_snaps3)
    halo_dict_1['unique.snap.4'] = np.asarray(halo_snaps4)
    halo_dict_1['unique.snap.5'] = np.asarray(halo_snaps5)
    halo_dict_1['num.contr.2'] = np.asarray(num_contr2)
    halo_dict_1['num.contr.3'] = np.asarray(num_contr3)
    halo_dict_1['num.contr.4'] = np.asarray(num_contr4)
    halo_dict_1['num.contr.5'] = np.asarray(num_contr5)
    halo_dict_1['num.contr.frac.2'] = np.asarray(num_contr2)/len(star_ind_1)
    halo_dict_1['num.contr.frac.3'] = np.asarray(num_contr3)/len(star_ind_1)
    halo_dict_1['num.contr.frac.4'] = np.asarray(num_contr4)/len(star_ind_1)
    halo_dict_1['num.contr.frac.5'] = np.asarray(num_contr5)/len(star_ind_1)
    halo_dict_1['mass.bound.peak.2'] = np.asarray(bound_peak2)
    halo_dict_1['mass.bound.peak.3'] = np.asarray(bound_peak3)
    halo_dict_1['mass.bound.peak.4'] = np.asarray(bound_peak4)
    halo_dict_1['mass.bound.peak.5'] = np.asarray(bound_peak5)
    halo_dict_1['star.mass.peak.2'] = np.asarray(star_peak2)
    halo_dict_1['star.mass.peak.3'] = np.asarray(star_peak3)
    halo_dict_1['star.mass.peak.4'] = np.asarray(star_peak4)
    halo_dict_1['star.mass.peak.5'] = np.asarray(star_peak5)
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    """
    Save what you want to dictionaries and write to files
    """
    # Gather all of the star data
    star_dict_1 = dict()
    star_dict_1['star.memb.halo.inds'] = np.asarray(star_memb_halo_inds_all)
    star_dict_1['star.memb.halo.desc.inds'] = np.asarray(star_memb_halo_inds_desc_all)
    star_dict_1['no.2.count'] = np.asarray(num_not_in_halos_2count)
    star_dict_1['no.3.count'] = np.asarray(num_not_in_halos_3count)
    star_dict_1['no.4.count'] = np.asarray(num_not_in_halos_4count)
    star_dict_1['no.5.count'] = np.asarray(num_not_in_halos_5count)
    star_dict_1['no.2.frac'] = np.asarray(num_not_in_halos_2count/len(star_ind_1))
    star_dict_1['no.3.frac'] = np.asarray(num_not_in_halos_3count/len(star_ind_1))
    star_dict_1['no.4.frac'] = np.asarray(num_not_in_halos_4count/len(star_ind_1))
    star_dict_1['no.5.frac'] = np.asarray(num_not_in_halos_5count/len(star_ind_1))
    # Save to file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_halo_dictionary', dict_or_array_to_write=halo_dict_1, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_star_dictionary', dict_or_array_to_write=star_dict_1, verbose=True)

    ########## Galaxy 2
    """
    For each star of interest, save an array where each element correpsonds to the
    halo index that it was a member of at that snapshot
    """
    start = time.time()
    # Initialize empty array
    star_memb_halo_inds_all = []
    # Loop over all star particles
    for n in range(0, len(star_ind_2)):
        # Set up array for each star particle
        star_memb_halo_inds = (-1)*np.ones(601, int)
        # Set first element to host index because they are in the host at z = 0
        star_memb_halo_inds[0] = main_halo_inds_2[0]
        # Loop over the other snapshots
        for i in range(1, 598): # want it to stop at 598 cause theres no pointers (or stars) at snapshot 1
            # Loop over the luminous halos at that snapshot
            for j in range(0, len(halo_inds[i])):
                # Look for the halo that the star was a member of and save it
                if star_ind_2[n] in halt['star.z0.indices'][halo_inds[i][j]]:
                    star_memb_halo_inds[i] = halo_inds[i][j]
        # Append each star's array to the total array
        star_memb_halo_inds_all.append(star_memb_halo_inds)
    end = time.time()
    print('Finished saving each star\'s halo member index at each snapshot in', end-start, 'seconds')


    """
    For each star, now we want a new list where we find the descendant halos of the
    halos in 'star_memb_halo_inds_all[i]'

    Each element of this new array will be the halo's last descendant before
    merging into the host
    """
    start = time.time()
    # Loop over all star particles
    star_memb_halo_inds_desc_all = []
    for n in range(0, len(star_ind_2)):
        # Set temporary array for each star and create an array to save descendants to
        temp_star = np.asarray(star_memb_halo_inds_all)[n]
        temp_star_forward = (-1)*np.ones(len(temp_star), int)
        # Loop over all of the elements in each star's array
        # This is essentially a loop over all snapshots (or indices of the halos the star was a member of)
        for i in range(0, len(temp_star)):
            # If the halo index is the host, set descendant index to ZERO
            if temp_star[i] in main_halo_inds_2:
                temp_star_forward[i] = 0
            # If the halo index is not the host, and isn't null, find its last descendant before merging
            elif temp_star[i] not in main_halo_inds_2 and temp_star[i] != -1:
                # Get list of descendant indices (starts at current snapshot and goes forward in time)
                check = halt.prop('descendant.indices', temp_star[i])
                # Mask out the indices that are the host
                mask = np.asarray([check[j] not in main_halo_inds_2 for j in range(0, len(check))])
                # Apply mask and grab the last element, which is the element furthest in time before merging
                temp_star_forward[i] = check[mask][-1]
            # If the index is null, set descendant index to null
            else:
                temp_star_forward[i] = -1
        # Append each descendant array to master array
        star_memb_halo_inds_desc_all.append(temp_star_forward)
    end = time.time()
    print('Finished getting progenitor halos for all stars in',end-start,'seconds')


    """
    - Find the unique halos that our stars were members of.
      Want to see how many stars these halos contributed.
    - Find the number of stars that don't satisfy the 'multiple snapshot' criteria.
      This includes the stars that do not form in any halo, and subsequently
      merge into the host.
    """
    start = time.time()
    # Set up counters to see how many stars do not satisfy the 'multiple snapshot' criteria
    num_not_in_halos_2count = 0
    num_not_in_halos_3count = 0
    num_not_in_halos_4count = 0
    num_not_in_halos_5count = 0
    # Set up empty arrays to save the halo indices of stars that satisfy the
    # multiple snapshot criteria
    two_count_star_halos_all = []
    three_count_star_halos_all = []
    four_count_star_halos_all = []
    five_count_star_halos_all = []
    # Set up empty arrays to select only the unique halos in the above arrays
    # This will contain repeating indices
    member_halos_2count = []
    member_halos_3count = []
    member_halos_4count = []
    member_halos_5count = []
    # Loop over the star's descendant array
    for i in range(0, len(star_memb_halo_inds_desc_all)):
        # Set up temporary arrays for each star
        two_count_star_halos = []
        three_count_star_halos = []
        four_count_star_halos = []
        five_count_star_halos = []
        # Loop over snapshots (or member halo descendant indices)
        # Want this to exclude the last 4 elements because of our '5 snapshot in a row criteria'
        for j in range(0, len(star_memb_halo_inds_desc_all[i])-4):
            # If stars were members of a halo for 2 snapshots in a row, save the index
            # These will contain repeating elements
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]):
                two_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 3 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]):
                three_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 4 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]):
                four_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
            # If stars were members of a halo for 5 snapshots in a row, save the index
            if (star_memb_halo_inds_desc_all[i][j] == star_memb_halo_inds_desc_all[i][j+1]) and (star_memb_halo_inds_desc_all[i][j+1] == star_memb_halo_inds_desc_all[i][j+2]) and (star_memb_halo_inds_desc_all[i][j+2] == star_memb_halo_inds_desc_all[i][j+3]) and (star_memb_halo_inds_desc_all[i][j+3] == star_memb_halo_inds_desc_all[i][j+4]):
                five_count_star_halos.append(star_memb_halo_inds_desc_all[i][j])
        # Append the temporary array to the master array
        two_count_star_halos_all.append(np.asarray(two_count_star_halos))
        three_count_star_halos_all.append(np.asarray(three_count_star_halos))
        four_count_star_halos_all.append(np.asarray(four_count_star_halos))
        five_count_star_halos_all.append(np.asarray(five_count_star_halos))
        # If the arrays contain values that aren't the host or null, print out
        # the values to the empty unique arrays.
        # Two count
        if np.any(two_count_star_halos_all[i] > 0):
            tm = np.unique(two_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(two_count_star_halos_all[i])[tm])):
                member_halos_2count.append(np.unique(two_count_star_halos_all[i])[tm][k])
        # If the arrays only contain the host or nulls, count this as a case where
        # the star did not satisfy the 'multiple snapshot' criteria
        else:
            num_not_in_halos_2count += 1
        # Three count
        if np.any(three_count_star_halos_all[i] > 0):
            tm = np.unique(three_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(three_count_star_halos_all[i])[tm])):
                member_halos_3count.append(np.unique(three_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_3count += 1
        # Four count
        if np.any(four_count_star_halos_all[i] > 0):
            tm = np.unique(four_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(four_count_star_halos_all[i])[tm])):
                member_halos_4count.append(np.unique(four_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_4count += 1
        # Five count
        if np.any(five_count_star_halos_all[i] > 0):
            tm = np.unique(five_count_star_halos_all[i]) > 0
            for k in range(0, len(np.unique(five_count_star_halos_all[i])[tm])):
                member_halos_5count.append(np.unique(five_count_star_halos_all[i])[tm][k])
        else:
            num_not_in_halos_5count += 1
    # Select only the unique elements from the 'member_halos*' arrays
    unique_halo_2count = np.unique(member_halos_2count)
    unique_halo_3count = np.unique(member_halos_3count)
    unique_halo_4count = np.unique(member_halos_4count)
    unique_halo_5count = np.unique(member_halos_5count)
    end = time.time()
    print('Finished counting unique halos in',end-start,'seconds')


    """
    Now, for each halo in the unique arrays, find out how many star particles it
    contributed to the host
    """
    start = time.time()
    # Set up empty dictionary for the unique halo properties
    halo_dict_2 = dict()
    halo_dict_2['unique.halo.2'] = np.asarray(unique_halo_2count)
    halo_dict_2['unique.halo.3'] = np.asarray(unique_halo_3count)
    halo_dict_2['unique.halo.4'] = np.asarray(unique_halo_4count)
    halo_dict_2['unique.halo.5'] = np.asarray(unique_halo_5count)
    halo_snaps2 = []
    halo_snaps3 = []
    halo_snaps4 = []
    halo_snaps5 = []
    num_contr2 = []
    num_contr3 = []
    num_contr4 = []
    num_contr5 = []
    bound_peak2 = []
    bound_peak3 = []
    bound_peak4 = []
    bound_peak5 = []
    star_peak2 = []
    star_peak3 = []
    star_peak4 = []
    star_peak5 = []
    # Loop over all of the elements in the unique halo arrays
    # 2 snapshot criteria
    for a in range(0, len(unique_halo_2count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_2count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps2.append(snap)
        bound_peak2.append(halt.prop('mass.bound.peak', halo))
        star_peak2.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_2)):
                if star_ind_2[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr2.append(count)
        else:
            num_contr2.append(count)
    # 3 snapshot criteria
    for a in range(0, len(unique_halo_3count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_3count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps3.append(snap)
        bound_peak3.append(halt.prop('mass.bound.peak', halo))
        star_peak3.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_2)):
                if star_ind_2[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr3.append(count)
        else:
            num_contr3.append(count)
    # 4 snapshot criteria
    for a in range(0, len(unique_halo_4count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_4count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps4.append(snap)
        bound_peak4.append(halt.prop('mass.bound.peak', halo))
        star_peak4.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_2)):
                if star_ind_2[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr4.append(count)
        else:
            num_contr4.append(count)
    # 5 snapshot criteria
    for a in range(0, len(unique_halo_5count)):
        # Set up counter to count the number of stars this halo contributes
        count = 0
        halo = unique_halo_5count[a]
        snap = halt['snapshot'][halo]
        # Save the snapshot, peak bound mass, and peak stellar mass of this halo
        halo_snaps5.append(snap)
        bound_peak5.append(halt.prop('mass.bound.peak', halo))
        star_peak5.append(halt.prop('star.mass.peak', halo))
        # If the snapshot isn't 600, read in the pointers and loop through the
        # metal-poor stars to see if they were members of this halo
        if snap != 600:
            for b in range(0, len(star_ind_2)):
                if star_ind_2[b] in halt['star.z0.indices'][halo]:
                    count += 1
            num_contr5.append(count)
        else:
            num_contr5.append(count)
    # Save all of the properties to the dictionary as well
    halo_dict_2['unique.snap.2'] = np.asarray(halo_snaps2)
    halo_dict_2['unique.snap.3'] = np.asarray(halo_snaps3)
    halo_dict_2['unique.snap.4'] = np.asarray(halo_snaps4)
    halo_dict_2['unique.snap.5'] = np.asarray(halo_snaps5)
    halo_dict_2['num.contr.2'] = np.asarray(num_contr2)
    halo_dict_2['num.contr.3'] = np.asarray(num_contr3)
    halo_dict_2['num.contr.4'] = np.asarray(num_contr4)
    halo_dict_2['num.contr.5'] = np.asarray(num_contr5)
    halo_dict_2['num.contr.frac.2'] = np.asarray(num_contr2)/len(star_ind_2)
    halo_dict_2['num.contr.frac.3'] = np.asarray(num_contr3)/len(star_ind_2)
    halo_dict_2['num.contr.frac.4'] = np.asarray(num_contr4)/len(star_ind_2)
    halo_dict_2['num.contr.frac.5'] = np.asarray(num_contr5)/len(star_ind_2)
    halo_dict_2['mass.bound.peak.2'] = np.asarray(bound_peak2)
    halo_dict_2['mass.bound.peak.3'] = np.asarray(bound_peak3)
    halo_dict_2['mass.bound.peak.4'] = np.asarray(bound_peak4)
    halo_dict_2['mass.bound.peak.5'] = np.asarray(bound_peak5)
    halo_dict_2['star.mass.peak.2'] = np.asarray(star_peak2)
    halo_dict_2['star.mass.peak.3'] = np.asarray(star_peak3)
    halo_dict_2['star.mass.peak.4'] = np.asarray(star_peak4)
    halo_dict_2['star.mass.peak.5'] = np.asarray(star_peak5)
    end = time.time()
    print('Finished counting the unique halo contributions in', end-start, 'seconds')


    """
    Save what you want to dictionaries and write to files
    """
    # Gather all of the star data
    star_dict_2 = dict()
    star_dict_2['star.memb.halo.inds'] = np.asarray(star_memb_halo_inds_all)
    star_dict_2['star.memb.halo.desc.inds'] = np.asarray(star_memb_halo_inds_desc_all)
    star_dict_2['no.2.count'] = np.asarray(num_not_in_halos_2count)
    star_dict_2['no.3.count'] = np.asarray(num_not_in_halos_3count)
    star_dict_2['no.4.count'] = np.asarray(num_not_in_halos_4count)
    star_dict_2['no.5.count'] = np.asarray(num_not_in_halos_5count)
    star_dict_2['no.2.frac'] = np.asarray(num_not_in_halos_2count/len(star_ind_2))
    star_dict_2['no.3.frac'] = np.asarray(num_not_in_halos_3count/len(star_ind_2))
    star_dict_2['no.4.frac'] = np.asarray(num_not_in_halos_4count/len(star_ind_2))
    star_dict_2['no.5.frac'] = np.asarray(num_not_in_halos_5count/len(star_ind_2))
    # Save to file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_halo_dictionary', dict_or_array_to_write=halo_dict_2, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_star_dictionary', dict_or_array_to_write=star_dict_2, verbose=True)

    print('All DONE!')
