#!/usr/bin/python3

"""
 ============================
 = Metallicity vs Asymmetry =
 ============================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot [Fe/H] < X (x-axis) vs asymmetry (y-axis) for all hosts

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
#
loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    plt.rcParams["font.family"] = "serif"
print('Set paths')

## Read in the data
# Cumulative
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_fe_poor')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_fe_poor')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_fe_poor')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_fe_poor')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_fe_poor')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_fe_poor')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_fe_poor')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_fe_poor')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_fe_poor')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_fe_poor')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_fe_poor')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_fe_poor')
# Binned
asym_m12b_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_fe_poor_bin')
asym_m12c_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_fe_poor_bin')
asym_m12f_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_fe_poor_bin')
asym_m12i_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_fe_poor_bin')
asym_m12m_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_fe_poor_bin')
asym_m12w_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_fe_poor_bin')
asym_Romeo_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_fe_poor_bin')
asym_Juliet_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_fe_poor_bin')
asym_Thelma_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_fe_poor_bin')
asym_Louise_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_fe_poor_bin')
asym_Romulus_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_fe_poor_bin')
asym_Remus_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_fe_poor_bin')
#
metallicities = np.arange(-3.75, 2.25, 0.25)

# Calculate the median asymmetry for each metallicity
# Cumulative plot
# Stack the data
asym_all = np.vstack((asym_m12b['asymmetry.fe.poor'],asym_m12c['asymmetry.fe.poor'],asym_m12f['asymmetry.fe.poor'],asym_m12i['asymmetry.fe.poor'],asym_m12m['asymmetry.fe.poor'],asym_m12w['asymmetry.fe.poor'],asym_Romeo['asymmetry.fe.poor'],asym_Juliet['asymmetry.fe.poor'],asym_Thelma['asymmetry.fe.poor'],asym_Louise['asymmetry.fe.poor'],asym_Romulus['asymmetry.fe.poor'],asym_Remus['asymmetry.fe.poor']))
asym_med = np.nanmedian(asym_all, axis=0)
asym_iso = np.vstack((asym_m12b['asymmetry.fe.poor'],asym_m12c['asymmetry.fe.poor'],asym_m12f['asymmetry.fe.poor'],asym_m12i['asymmetry.fe.poor'],asym_m12m['asymmetry.fe.poor'],asym_m12w['asymmetry.fe.poor']))
asym_iso_med = np.nanmedian(asym_iso, axis=0)
asym_lg = np.vstack((asym_Romeo['asymmetry.fe.poor'],asym_Juliet['asymmetry.fe.poor'],asym_Thelma['asymmetry.fe.poor'],asym_Louise['asymmetry.fe.poor'],asym_Romulus['asymmetry.fe.poor'],asym_Remus['asymmetry.fe.poor']))
asym_lg_med = np.nanmedian(asym_lg, axis=0)
#
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
plt.plot(metallicities[:14], asym_m12b['asymmetry.fe.poor'][:14], color=colors[0], label='m12b', alpha=0.5)
plt.plot(metallicities[:14], asym_m12c['asymmetry.fe.poor'][:14], color=colors[1], label='m12c', alpha=0.5)
plt.plot(metallicities[:14], asym_m12f['asymmetry.fe.poor'][:14], color=colors[6], label='m12f', alpha=0.5)
plt.plot(metallicities[:14], asym_m12i['asymmetry.fe.poor'][:14], color=colors[3], label='m12i', alpha=0.5)
plt.plot(metallicities[:14], asym_m12m['asymmetry.fe.poor'][:14], color=colors[4], label='m12m', alpha=0.5)
plt.plot(metallicities[:14], asym_m12w['asymmetry.fe.poor'][:14], color=colors[5], label='m12w', alpha=0.5)
plt.plot(metallicities[:14], asym_Romeo['asymmetry.fe.poor'][:14], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
plt.plot(metallicities[:14], asym_Juliet['asymmetry.fe.poor'][:14], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
plt.plot(metallicities[:14], asym_Thelma['asymmetry.fe.poor'][:14], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
plt.plot(metallicities[:14], asym_Louise['asymmetry.fe.poor'][:14], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
plt.plot(metallicities[:14], asym_Romulus['asymmetry.fe.poor'][:14], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
plt.plot(metallicities[:14], asym_Remus['asymmetry.fe.poor'][:14], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
plt.plot(metallicities[:14], asym_med[:14], color='k', label='median', alpha=0.8, linewidth=7)
plt.plot(metallicities[:14], asym_iso_med[:14], color='k', label='Iso median', alpha=0.5, linewidth=7)
plt.plot(metallicities[:14], asym_lg_med[:14], color='k', label='LG median', alpha=0.5, linestyle=':', linewidth=7)
plt.scatter(-3, 1.9, s=300, marker='x', c='k', label='MW (S19)')
plt.scatter(-2.5, 2.014, s=300, marker='x', c='k')
plt.scatter(-2, 2.32, s=300, marker='x', c='k')
plt.plot(metallicities[:14], asym_med[:14], linestyle='', label=' ')
plt.plot(metallicities[:14], asym_med[:14], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$(< [Fe/H])', fontsize=30)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10"])
y_locator = FixedLocator([1, 10])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_all.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_bin = np.vstack((asym_m12b_bin['asymmetry.fe.poor'],asym_m12c_bin['asymmetry.fe.poor'],asym_m12f_bin['asymmetry.fe.poor'],asym_m12i_bin['asymmetry.fe.poor'],asym_m12m_bin['asymmetry.fe.poor'],asym_m12w_bin['asymmetry.fe.poor'],asym_Romeo_bin['asymmetry.fe.poor'],asym_Juliet_bin['asymmetry.fe.poor'],asym_Thelma_bin['asymmetry.fe.poor'],asym_Louise_bin['asymmetry.fe.poor'],asym_Romulus_bin['asymmetry.fe.poor'],asym_Remus_bin['asymmetry.fe.poor']))
asym_med_bin = np.nanmedian(asym_all_bin, axis=0)
asym_iso_bin = np.vstack((asym_m12b_bin['asymmetry.fe.poor'],asym_m12c_bin['asymmetry.fe.poor'],asym_m12f_bin['asymmetry.fe.poor'],asym_m12i_bin['asymmetry.fe.poor'],asym_m12m_bin['asymmetry.fe.poor'],asym_m12w_bin['asymmetry.fe.poor']))
asym_iso_med_bin = np.nanmedian(asym_iso_bin, axis=0)
asym_lg_bin = np.vstack((asym_Romeo_bin['asymmetry.fe.poor'],asym_Juliet_bin['asymmetry.fe.poor'],asym_Thelma_bin['asymmetry.fe.poor'],asym_Louise_bin['asymmetry.fe.poor'],asym_Romulus_bin['asymmetry.fe.poor'],asym_Remus_bin['asymmetry.fe.poor']))
asym_lg_med_bin = np.nanmedian(asym_lg_bin, axis=0)
metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
metallicities_bin = metallicities+0.25
#
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(metallicities_bin[:8], asym_m12b_bin['asymmetry.fe.poor'][:8], color=colors[0], label='m12b', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12c_bin['asymmetry.fe.poor'][:8], color=colors[1], label='m12c', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12f_bin['asymmetry.fe.poor'][:8], color=colors[6], label='m12f', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12i_bin['asymmetry.fe.poor'][:8], color=colors[3], label='m12i', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12m_bin['asymmetry.fe.poor'][:8], color=colors[4], label='m12m', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12w_bin['asymmetry.fe.poor'][:8], color=colors[5], label='m12w', alpha=0.5)
ax.plot(metallicities_bin[:8], asym_Romeo_bin['asymmetry.fe.poor'][:8], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Juliet_bin['asymmetry.fe.poor'][:8], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Thelma_bin['asymmetry.fe.poor'][:8], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Louise_bin['asymmetry.fe.poor'][:8], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Romulus_bin['asymmetry.fe.poor'][:8], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Remus_bin['asymmetry.fe.poor'][:8], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.scatter(-2.25, 2.408, s=300, marker='x', c='k', label='MW')
ax.scatter(-2.75, 2.000, s=300, marker='x', c='k')
ax.scatter(-3.5, 1.857, s=300, marker='x', c='k')
#
ax.plot(metallicities_bin[:8], asym_med_bin[:8], color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(metallicities_bin[:8], asym_iso_med_bin[:8], color='k', label='Iso median', alpha=0.5, linewidth=7)
ax.plot(metallicities_bin[:8], asym_lg_med_bin[:8], color='k', linestyle=':', label='LG median', alpha=0.5, linewidth=7)
ax.plot(metallicities_bin[:8], asym_med_bin[:8], linestyle='', label=' ')
ax.plot(metallicities_bin[:8], asym_med_bin[:8], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
#ax.axes.xaxis.set_ticklabels([])
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
#ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_all_bin.pdf')
plt.close()
