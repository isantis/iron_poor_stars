#!/usr/bin/python3

"""
 ===================================
 = Metallicity vs Asymmetry (lite) =
 ===================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot [Fe/H] < X (x-axis) vs asymmetry (y-axis) for all hosts

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
#
loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    plt.rcParams["font.family"] = "serif"
print('Set paths')

## Read in the data
# Cumulative
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_fe_poor_lite')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_fe_poor_lite')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_fe_poor_lite')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_fe_poor_lite')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_fe_poor_lite')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_fe_poor_lite')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_fe_poor_lite')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_fe_poor_lite')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_fe_poor_lite')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_fe_poor_lite')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_fe_poor_lite')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_fe_poor_lite')
# Binned
asym_m12b_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_fe_poor_bin_lite')
asym_m12c_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_fe_poor_bin_lite')
asym_m12f_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_fe_poor_bin_lite')
asym_m12i_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_fe_poor_bin_lite')
asym_m12m_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_fe_poor_bin_lite')
asym_m12w_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_fe_poor_bin_lite')
asym_Romeo_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_fe_poor_bin_lite')
asym_Juliet_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_fe_poor_bin_lite')
asym_Thelma_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_fe_poor_bin_lite')
asym_Louise_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_fe_poor_bin_lite')
asym_Romulus_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_fe_poor_bin_lite')
asym_Remus_bin = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_fe_poor_bin_lite')
#
metallicities = np.array([-3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])

# Calculate the median asymmetry for each metallicity
# Cumulative plot
# Stack the data
asym_all = np.vstack((asym_m12b['asymmetry.fe.poor'],asym_m12c['asymmetry.fe.poor'],asym_m12f['asymmetry.fe.poor'],asym_m12i['asymmetry.fe.poor'],asym_m12m['asymmetry.fe.poor'],asym_m12w['asymmetry.fe.poor'],asym_Romeo['asymmetry.fe.poor'],asym_Juliet['asymmetry.fe.poor'],asym_Thelma['asymmetry.fe.poor'],asym_Louise['asymmetry.fe.poor'],asym_Romulus['asymmetry.fe.poor'],asym_Remus['asymmetry.fe.poor']))
asym_med = np.median(asym_all, axis=0)
#
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
plt.plot(metallicities[:7], asym_m12b['asymmetry.fe.poor'][:7], color=colors[0], label='m12b', alpha=0.5)
plt.plot(metallicities[:7], asym_m12c['asymmetry.fe.poor'][:7], color=colors[1], label='m12c', alpha=0.5)
plt.plot(metallicities[:7], asym_m12f['asymmetry.fe.poor'][:7], color=colors[6], label='m12f', alpha=0.5)
plt.plot(metallicities[:7], asym_m12i['asymmetry.fe.poor'][:7], color=colors[3], label='m12i', alpha=0.5)
plt.plot(metallicities[:7], asym_m12m['asymmetry.fe.poor'][:7], color=colors[4], label='m12m', alpha=0.5)
plt.plot(metallicities[:7], asym_m12w['asymmetry.fe.poor'][:7], color=colors[5], label='m12w', alpha=0.5)
plt.plot(metallicities[:7], asym_Romeo['asymmetry.fe.poor'][:7], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
plt.plot(metallicities[:7], asym_Juliet['asymmetry.fe.poor'][:7], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
plt.plot(metallicities[:7], asym_Thelma['asymmetry.fe.poor'][:7], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
plt.plot(metallicities[:7], asym_Louise['asymmetry.fe.poor'][:7], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
plt.plot(metallicities[:7], asym_Romulus['asymmetry.fe.poor'][:7], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
plt.plot(metallicities[:7], asym_Remus['asymmetry.fe.poor'][:7], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
plt.plot(metallicities[:7], asym_med[:7], color='k', label='median', alpha=0.8, linewidth=7)
plt.plot(metallicities[:7], asym_med[:7], linestyle='', label=' ')
plt.plot(metallicities[:7], asym_med[:7], linestyle='', label=' ')
plt.plot(metallicities[:7], asym_med[:7], linestyle='', label=' ')
plt.plot(metallicities[:7], asym_med[:7], linestyle='', label=' ')
plt.plot(metallicities[:7], asym_med[:7], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$(< [Fe/H])', fontsize=30)
plt.title('Lite version', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10"])
y_locator = FixedLocator([1, 10])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_all_lite.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_bin = np.vstack((asym_m12b_bin['asymmetry.fe.poor'],asym_m12c_bin['asymmetry.fe.poor'],asym_m12f_bin['asymmetry.fe.poor'],asym_m12i_bin['asymmetry.fe.poor'],asym_m12m_bin['asymmetry.fe.poor'],asym_m12w_bin['asymmetry.fe.poor'],asym_Romeo_bin['asymmetry.fe.poor'],asym_Juliet_bin['asymmetry.fe.poor'],asym_Thelma_bin['asymmetry.fe.poor'],asym_Louise_bin['asymmetry.fe.poor'],asym_Romulus_bin['asymmetry.fe.poor'],asym_Remus_bin['asymmetry.fe.poor']))
asym_med_bin = np.median(asym_all_bin, axis=0)
metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
metallicities_bin = metallicities+0.25
#
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(metallicities_bin[:8], asym_m12b_bin['asymmetry.fe.poor'][:8], color=colors[0], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12c_bin['asymmetry.fe.poor'][:8], color=colors[1], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12f_bin['asymmetry.fe.poor'][:8], color=colors[6], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12i_bin['asymmetry.fe.poor'][:8], color=colors[3], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12m_bin['asymmetry.fe.poor'][:8], color=colors[4], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_m12w_bin['asymmetry.fe.poor'][:8], color=colors[5], alpha=0.5)
ax.plot(metallicities_bin[:8], asym_Romeo_bin['asymmetry.fe.poor'][:8], color=colors[0], alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Juliet_bin['asymmetry.fe.poor'][:8], color=colors[1], alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Thelma_bin['asymmetry.fe.poor'][:8], color=colors[6], alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Louise_bin['asymmetry.fe.poor'][:8], color=colors[3], alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Romulus_bin['asymmetry.fe.poor'][:8], color=colors[4], alpha=0.5, linestyle=':')
ax.plot(metallicities_bin[:8], asym_Remus_bin['asymmetry.fe.poor'][:8], color=colors[5], alpha=0.5, linestyle=':')
#
ax.plot(metallicities_bin[:8], asym_med_bin[:8], color='k', label='$J_{\phi}>0/J_{\phi}<0$', alpha=0.8, linewidth=7)
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
#ax.axes.xaxis.set_ticklabels([])
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#plt.title('Lite version', fontsize=32)
#
ax.legend(prop={'size': 24}, loc='best')
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_all_bin_lite.pdf')
plt.close()
