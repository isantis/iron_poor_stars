#!/usr/bin/python3

"""
 ==========================
 = Merger Angular Momenta =
 ==========================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2021

 GOAL: Find the L vectors of the three mergers
       Save the angle between them

"""

import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    halo_ses_ret_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_retrograde_data')
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)

    ########## Galaxy 1
    #
    # Get the snapshot and id of halo that contributes the most stars
    id_1 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    id_2 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[1])[0][0]]
    id_3 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[2])[0][0]]
    #
    # Get the angular momentum of the halos
    # Primary
    ell_x = halt.prop('host.distance.principal', id_1)[1]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[1]*halt.prop('host.distance.principal', id_1)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[2])
    ell_z = halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[1] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[1]
    ang_1 = np.array([ell_x, ell_y, ell_z])
    #
    # Secondary
    ell_x = halt.prop('host.distance.principal', id_2)[1]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[1]*halt.prop('host.distance.principal', id_2)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[2])
    ell_z = halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[1] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[1]
    ang_2 = np.array([ell_x, ell_y, ell_z])
    #
    # Tertiary
    ell_x = halt.prop('host.distance.principal', id_3)[1]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[1]*halt.prop('host.distance.principal', id_3)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[2])
    ell_z = halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[1] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[1]
    ang_3 = np.array([ell_x, ell_y, ell_z])
    #
    #
    # Normalize the vectors, find the cos(theta) and theta
    ell1 = ang_1/np.linalg.norm(ang_1)
    ell2 = ang_2/np.linalg.norm(ang_2)
    ell3 = ang_3/np.linalg.norm(ang_3)
    #
    cos_12 = np.dot(ell1, ell2)
    cos_13 = np.dot(ell1, ell3)
    cos_23 = np.dot(ell2, ell3)
    #
    theta_12 = np.rad2deg(np.arccos(cos_12))
    theta_13 = np.rad2deg(np.arccos(cos_13))
    theta_23 = np.rad2deg(np.arccos(cos_23))
    #
    #
    # Save to a dictionary
    d_1 = dict()
    d_1['ell.1'] = ell1
    d_1['ell.2'] = ell2
    d_1['ell.3'] = ell3
    d_1['cos.1.2'] = cos_12
    d_1['cos.1.3'] = cos_13
    d_1['cos.2.3'] = cos_23
    d_1['theta.1.2'] = theta_12
    d_1['theta.1.3'] = theta_13
    d_1['theta.2.3'] = theta_23
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_ells_n_angles', dict_or_array_to_write=d_1, verbose=True)

if num_gal == 2:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    halo_ses_ret_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_retrograde_data')
    #
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    halo_ses_pro_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_prograde_data')
    halo_ses_ret_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_retrograde_data')
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)

    ########## Galaxy 1
    #
    # Get the snapshot and id of halo that contributes the most stars
    id_1 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    id_2 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[1])[0][0]]
    id_3 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[2])[0][0]]
    #
    # Get the angular momentum of the halos
    # Primary
    ell_x = halt.prop('host.distance.principal', id_1)[1]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[1]*halt.prop('host.distance.principal', id_1)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[2])
    ell_z = halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[1] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[1]
    ang_1 = np.array([ell_x, ell_y, ell_z])
    #
    # Secondary
    ell_x = halt.prop('host.distance.principal', id_2)[1]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[1]*halt.prop('host.distance.principal', id_2)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[2])
    ell_z = halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[1] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[1]
    ang_2 = np.array([ell_x, ell_y, ell_z])
    #
    # Tertiary
    ell_x = halt.prop('host.distance.principal', id_3)[1]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[1]*halt.prop('host.distance.principal', id_3)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[2])
    ell_z = halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[1] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[1]
    ang_3 = np.array([ell_x, ell_y, ell_z])
    #
    #
    # Normalize the vectors, find the cos(theta) and theta
    ell1 = ang_1/np.linalg.norm(ang_1)
    ell2 = ang_2/np.linalg.norm(ang_2)
    ell3 = ang_3/np.linalg.norm(ang_3)
    #
    cos_12 = np.dot(ell1, ell2)
    cos_13 = np.dot(ell1, ell3)
    cos_23 = np.dot(ell2, ell3)
    #
    theta_12 = np.rad2deg(np.arccos(cos_12))
    theta_13 = np.rad2deg(np.arccos(cos_13))
    theta_23 = np.rad2deg(np.arccos(cos_23))
    #
    #
    # Save to a dictionary
    d_1 = dict()
    d_1['ell.1'] = ell1
    d_1['ell.2'] = ell2
    d_1['ell.3'] = ell3
    d_1['cos.1.2'] = cos_12
    d_1['cos.1.3'] = cos_13
    d_1['cos.2.3'] = cos_23
    d_1['theta.1.2'] = theta_12
    d_1['theta.1.3'] = theta_13
    d_1['theta.2.3'] = theta_23
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_ells_n_angles', dict_or_array_to_write=d_1, verbose=True)
    #
    #
    # Galaxy 2
    # Get the snapshot and id of halo that contributes the most stars
    id_1 = halo_ses_pro_2['unique.halo'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    id_2 = halo_ses_pro_2['unique.halo'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[1])[0][0]]
    id_3 = halo_ses_pro_2['unique.halo'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[2])[0][0]]
    #
    # Get the angular momentum of the halos
    # Primary
    ell_x = halt.prop('host.distance.principal', id_1)[1]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[1]*halt.prop('host.distance.principal', id_1)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[2] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[2])
    ell_z = halt.prop('host.distance.principal', id_1)[0]*halt.prop('host.velocity.principal', id_1)[1] - halt.prop('host.velocity.principal', id_1)[0]*halt.prop('host.distance.principal', id_1)[1]
    ang_1 = np.array([ell_x, ell_y, ell_z])
    #
    # Secondary
    ell_x = halt.prop('host.distance.principal', id_2)[1]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[1]*halt.prop('host.distance.principal', id_2)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[2] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[2])
    ell_z = halt.prop('host.distance.principal', id_2)[0]*halt.prop('host.velocity.principal', id_2)[1] - halt.prop('host.velocity.principal', id_2)[0]*halt.prop('host.distance.principal', id_2)[1]
    ang_2 = np.array([ell_x, ell_y, ell_z])
    #
    # Tertiary
    ell_x = halt.prop('host.distance.principal', id_3)[1]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[1]*halt.prop('host.distance.principal', id_3)[2]
    ell_y = (-1)*(halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[2] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[2])
    ell_z = halt.prop('host.distance.principal', id_3)[0]*halt.prop('host.velocity.principal', id_3)[1] - halt.prop('host.velocity.principal', id_3)[0]*halt.prop('host.distance.principal', id_3)[1]
    ang_3 = np.array([ell_x, ell_y, ell_z])
    #
    #
    # Normalize the vectors, find the cos(theta) and theta
    ell1 = ang_1/np.linalg.norm(ang_1)
    ell2 = ang_2/np.linalg.norm(ang_2)
    ell3 = ang_3/np.linalg.norm(ang_3)
    #
    cos_12 = np.dot(ell1, ell2)
    cos_13 = np.dot(ell1, ell3)
    cos_23 = np.dot(ell2, ell3)
    #
    theta_12 = np.rad2deg(np.arccos(cos_12))
    theta_13 = np.rad2deg(np.arccos(cos_13))
    theta_23 = np.rad2deg(np.arccos(cos_23))
    #
    #
    # Save to a dictionary
    d_2 = dict()
    d_2['ell.1'] = ell1
    d_2['ell.2'] = ell2
    d_2['ell.3'] = ell3
    d_2['cos.1.2'] = cos_12
    d_2['cos.1.3'] = cos_13
    d_2['cos.2.3'] = cos_23
    d_2['theta.1.2'] = theta_12
    d_2['theta.1.3'] = theta_13
    d_2['theta.2.3'] = theta_23
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_ells_n_angles', dict_or_array_to_write=d_2, verbose=True)
