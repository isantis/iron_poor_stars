#!/usr/bin/python3

"""
 ================================
 = Time Varying Asymmetry Ratio =
 ================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2020

 GOAL: Compute the asymmetry ratio from z = 0 and going back in steps of 0.5 Gyr

       - Use the rotation speed at z = 0
       - Use the MOI tensor at the particular redshift of interest

 NOTE: I modified this on June 22, 2020 to include ALL metal-poor stars, not just
       the ones we pre-select at z = 0

 NOTE: I modified this again on June 26, 2020 to include the new changes from Andrew's code
            - the way cylindrical coordinates are stored
            - the way the MOI tensors are stored
"""

## Import all of the tools for analysis
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
gal_color = colors[4]
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    gal_color2 = colors[1]
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    gal_color2 = colors[3]
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    gal_color2 = colors[5]
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    ### Read in the necessary data
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    star_ind_1 = data_1['indices']
    #
    # Snapshot array that corresponds to ~ 0.5 Gyr time spacing
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    snap_track = np.array([600, 569, 547, 525, 505, 484, 463, 443, 424, 404, 385, 365, 346, 327, 308, 289, 270, 250, 231, 211, 190, 169, 147, 124, 99, 72, 42])
    #
    # Define the MOI tensor at z = 0
    part_z0 = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #Iz0 = part_z0.host_rotations[0]
    #
    # Disk Selection
    # Get the asymmetry parameter at z = 0 first
    asymmetry_r412 = []
    #
    jz = np.abs(data_1['pos.3d.cyl'][:,2]*data_1['vel.3d.cyl'][:,2])
    jphi = (data_1['pos.3d.car'][:,0]*data_1['vel.3d.car'][:,1] - data_1['vel.3d.car'][:,0]*data_1['pos.3d.car'][:,1])/(8*data_1['vrot.z0'])
    pos_mask = (jphi > 0.5) & (jphi < 1.2) & (jz > 0) & (jz < 437.5)
    neg_mask = (jphi < -0.5) & (jphi > -1.2) & (jz > 0) & (jz < 437.5)
    asymmetry_r412.append(np.sum(data_1['masses'][pos_mask])/np.sum(data_1['masses'][neg_mask]))
    #
    # Get the initial number of metal-poor, and prograde/retrograde stars
    N_prograde_r412 = np.zeros(len(snap_track), int)
    N_retrograde_r412 = np.zeros(len(snap_track), int)
    #
    N_prograde_r412[0] = np.sum(pos_mask)
    N_retrograde_r412[0] = np.sum(neg_mask)
    #
    # Get the initial mass of metal-poor, prograde/retrograde stars
    M_prograde_r412 = np.zeros(len(snap_track), int)
    M_retrograde_r412 = np.zeros(len(snap_track), int)
    #
    M_prograde_r412[0] = np.sum(data_1['masses'][pos_mask])
    M_retrograde_r412[0] = np.sum(data_1['masses'][neg_mask])
    #
    # 15 kpc selection
    # Get the asymmetry parameter at z = 0 first
    asymmetry_r15 = []
    #
    inds = ut.array.get_indices(part_z0['star'].prop('metallicity.iron'), [-np.inf, -2.5])
    inds = ut.array.get_indices(part_z0['star'].prop('star.host1.distance.principal.total'), [0,15], inds)
    #
    jz = np.abs(part_z0['star'].prop('star.host1.distance.principal.cylindrical', inds)[:,2]*part_z0['star'].prop('star.host1.velocity.principal.cylindrical', inds)[:,2])
    jphi = (part_z0['star'].prop('star.host1.distance.principal', inds)[:,0]*part_z0['star'].prop('star.host1.velocity.principal', inds)[:,1] - part_z0['star'].prop('star.host1.velocity.principal', inds)[:,0]*part_z0['star'].prop('star.host1.distance.principal', inds)[:,1])/(8*data_1['vrot.z0'])
    pos_mask = (jphi > 0.5) & (jphi < 1.2) & (jz > 0) & (jz < 437.5)
    neg_mask = (jphi < -0.5) & (jphi > -1.2) & (jz > 0) & (jz < 437.5)
    asymmetry_r15.append(np.sum(part_z0['star']['mass'][inds][pos_mask])/np.sum(part_z0['star']['mass'][inds][neg_mask]))
    #
    # Get the initial number of metal-poor, and prograde/retrograde stars
    N_prograde_r15 = np.zeros(len(snap_track), int)
    N_retrograde_r15 = np.zeros(len(snap_track), int)
    #
    N_prograde_r15[0] = np.sum(pos_mask)
    N_retrograde_r15[0] = np.sum(neg_mask)
    #
    # Get the initial mass of metal-poor, prograde/retrograde stars
    M_prograde_r15 = np.zeros(len(snap_track), int)
    M_retrograde_r15 = np.zeros(len(snap_track), int)
    #
    M_prograde_r15[0] = np.sum(part_z0['star']['mass'][inds][pos_mask])
    M_retrograde_r15[0] = np.sum(part_z0['star']['mass'][inds][neg_mask])
    #
    for i in range(1, len(snap_track)):
        #
        # Read in the particles at a snapshot
        part = gizmo.io.Read.read_snapshots('star', 'snapshot', snap_track[i], simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=True) # Originally True when I wanted to only track the stars we select at z = 0
        print('Stars read in')
        #
        # Get the MOI tensor from the z = 0 dictionary; still using MOI(z)
        part.host['rotation'][0] = part_z0.hostz['rotation'][snap_track[i]][0]
        #
        # Find the rotational velocity of the galaxy at this snapshot
        # NOTE: This is only for the v_rot(z) - MOI(z) selection
        ind_rot = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
        ind_rot = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], ind_rot)
        v_rot_z = np.median(part['star'].prop('star.host1.velocity.principal.cylindrical', ind_rot)[:,1])
        #v_rot_z = data_1['vrot.z0'] # This is the rotational velocity at z = 0
        #
        """
        # This chunk corresponds to tracking the stars we select at z = 0
        #
        # Read in the pointers and get the star indices at this snapshot
        pointers = part.Pointer.get_pointers(species_name_from='star', species_names_to='star')
        stars_at_z = pointers[star_ind_1]
        ind_mask = (stars_at_z > 0)
        print('Pointers assigned')
        #
        # Mask the stars that are within the distance selections
        stars_at_z_r412 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12], stars_at_z[ind_mask])
        stars_at_z_r412 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412)
        ##
        stars_at_z_r15 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [0,15], stars_at_z[ind_mask])
        """


        # This chunk corresponds to using all metal-poor stars at each snapshot, not just the ones we select at z = 0
        #
        # Get the metal-poor stars in spatial selection window.
        stars_at_z_r412 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r412 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12], stars_at_z_r412)
        stars_at_z_r412 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412)
        #


        # Get the stars that are prograde and retrograde
        if len(stars_at_z_r412) > 1:
            jz_r412 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', stars_at_z_r412)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', stars_at_z_r412)[:,2])
            jphi_r412 = (part['star'].prop('star.host1.distance.principal', stars_at_z_r412)[:,0]*part['star'].prop('star.host1.velocity.principal', stars_at_z_r412)[:,1] - part['star'].prop('star.host1.velocity.principal', stars_at_z_r412)[:,0]*part['star'].prop('star.host1.distance.principal', stars_at_z_r412)[:,1])/(8*v_rot_z)
            pos_mask_r412 = (jphi_r412 > 0.5) & (jphi_r412 < 1.2) & (jz_r412 > 0) & (jz_r412 < 437.5)
            neg_mask_r412 = (jphi_r412 < -0.5) & (jphi_r412 > -1.2) & (jz_r412 > 0) & (jz_r412 < 437.5)
        elif len(stars_at_z_r412) == 1:
            jz_r412 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', stars_at_z_r412)[2]*part['star'].prop('star.host1.velocity.principal.cylindrical', stars_at_z_r412)[2])
            jphi_r412 = (part['star'].prop('star.host1.distance.principal', stars_at_z_r412)[0][0]*part['star'].prop('star.host1.velocity.principal', stars_at_z_r412)[0][1] - part['star'].prop('star.host1.velocity.principal', stars_at_z_r412)[0][0]*part['star'].prop('star.host1.distance.principal', stars_at_z_r412)[0][1])/(8*v_rot_z)
            pos_mask_r412 = (jphi_r412 > 0.5) & (jphi_r412 < 1.2) & (jz_r412 > 0) & (jz_r412 < 437.5)
            neg_mask_r412 = (jphi_r412 < -0.5) & (jphi_r412 > -1.2) & (jz_r412 > 0) & (jz_r412 < 437.5)
        elif len(stars_at_z_r412) == 0:
            pos_mask_r412 = False
            neg_mask_r412 = False
        #
        N_prograde_r412[i] = np.sum(pos_mask_r412)
        N_retrograde_r412[i] = np.sum(neg_mask_r412)
        #
        M_prograde_r412[i] = np.sum(part['star']['mass'][stars_at_z_r412][pos_mask_r412])
        M_retrograde_r412[i] = np.sum(part['star']['mass'][stars_at_z_r412][neg_mask_r412])
        #
        # Figure out what the asymmetry parameter is at this snapshot
        if np.sum(neg_mask_r412) != 0:
            asymmetry_r412.append(np.sum(part['star']['mass'][stars_at_z_r412][pos_mask_r412])/np.sum(part['star']['mass'][stars_at_z_r412][neg_mask_r412]))
        else:
            asymmetry_r412.append(1e6)


        # This chunk corresponds to using all metal-poor stars at each snapshot, not just the ones we select at z = 0
        #
        # Get the metal-poor stars in spatial selection window.
        stars_at_z_r15 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r15 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.total'), [0, 15], stars_at_z_r15)
        #


        # Get the stars that are prograde and retrograde
        jz_r15 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', stars_at_z_r15)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', stars_at_z_r15)[:,2])
        jphi_r15 = (part['star'].prop('star.host1.distance.principal', stars_at_z_r15)[:,0]*part['star'].prop('star.host1.velocity.principal', stars_at_z_r15)[:,1] - part['star'].prop('star.host1.velocity.principal', stars_at_z_r15)[:,0]*part['star'].prop('star.host1.distance.principal', stars_at_z_r15)[:,1])/(8*v_rot_z)
        pos_mask_r15 = (jphi_r15 > 0.5) & (jphi_r15 < 1.2) & (jz_r15 > 0) & (jz_r15 < 437.5)
        neg_mask_r15 = (jphi_r15 < -0.5) & (jphi_r15 > -1.2) & (jz_r15 > 0) & (jz_r15 < 437.5)
        #
        N_prograde_r15[i] = np.sum(pos_mask_r15)
        N_retrograde_r15[i] = np.sum(neg_mask_r15)
        #
        M_prograde_r15[i] = np.sum(part['star']['mass'][stars_at_z_r15][pos_mask_r15])
        M_retrograde_r15[i] = np.sum(part['star']['mass'][stars_at_z_r15][neg_mask_r15])
        #
        # Figure out what the asymmetry parameter is at this snapshot
        if np.sum(neg_mask_r15) != 0:
            asymmetry_r15.append(np.sum(part['star']['mass'][stars_at_z_r15][pos_mask_r15])/np.sum(part['star']['mass'][stars_at_z_r15][neg_mask_r15]))
        else:
            asymmetry_r15.append(1e6)

        print('Done with current snapshot')

    # Plot the asymmetry parameter vs time (or redshift)
    asymmetry_r412 = np.asarray(asymmetry_r412)
    asymmetry_r15 = np.asarray(asymmetry_r15)
    #
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r412 != 1e6], asymmetry_r412[asymmetry_r412 != 1e6], color=gal_color, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r412_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r412, color='b', linestyle='-', label='Prograde (disk selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r412, color='r', linestyle='-', label='Retrograde (disk selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r412_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()

    plt.figure(3)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r15 != 1e6], asymmetry_r15[asymmetry_r15 != 1e6], color=gal_color, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r15_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(4)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r15, color='b', linestyle='-', label='Prograde (15 kpc selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r15, color='r', linestyle='-', label='Retrograde (15 kpc selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r15_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()

    # Save the data to a dictionary and file
    dictionary = dict()
    dictionary['asymmetry'] = asymmetry_r412
    dictionary['asymmetry.snapshots'] = snap_track
    dictionary['num.prograde'] = N_prograde_r412
    dictionary['num.retrograde'] = N_retrograde_r412
    dictionary['mass.prograde'] = M_prograde_r412
    dictionary['mass.retrograde'] = M_retrograde_r412
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r412_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary, verbose=True)
    #
    dictionary2 = dict()
    dictionary2['asymmetry'] = asymmetry_r15
    dictionary2['asymmetry.snapshots'] = snap_track
    dictionary2['num.prograde'] = N_prograde_r15
    dictionary2['num.retrograde'] = N_retrograde_r15
    dictionary2['mass.prograde'] = M_prograde_r15
    dictionary2['mass.retrograde'] = M_retrograde_r15
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r15_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary2, verbose=True)

    print('All data saved!')

    """
    NOTE: For both the plots and pickle files, for the newer stuff, I titled them with '_z0' at the end of the names.
    """

if num_gal == 2:
    ### Read in the necessary data
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    data_2 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    star_ind_1 = data_1['indices']
    star_ind_2 = data_2['indices']
    #
    # Snapshot array that corresponds to ~ 0.5 Gyr time spacing
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    snap_track = np.array([600, 569, 547, 525, 505, 484, 463, 443, 424, 404, 385, 365, 346, 327, 308, 289, 270, 250, 231, 211, 190, 169, 147, 124, 99, 72, 42])
    #
    # Define the MOI tensor at z = 0
    part_z0 = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #Iz0 = part_z0.host_rotations[0]
    #
    # Disk selection
    # Get the asymmetry parameter at z = 0 first
    asymmetry_r412_1 = []
    asymmetry_r412_2 = []
    #
    jz_1 = np.abs(data_1['pos.3d.cyl'][:,2]*data_1['vel.3d.cyl'][:,2])
    jphi_1 = (data_1['pos.3d.car'][:,0]*data_1['vel.3d.car'][:,1] - data_1['vel.3d.car'][:,0]*data_1['pos.3d.car'][:,1])/(8*data_1['vrot.z0'])
    pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
    neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
    asymmetry_r412_1.append(np.sum(data_1['masses'][pos_mask_1])/np.sum(data_1['masses'][neg_mask_1]))
    #
    jz_2 = np.abs(data_2['pos.3d.cyl'][:,2]*data_2['vel.3d.cyl'][:,2])
    jphi_2 = (data_2['pos.3d.car'][:,0]*data_2['vel.3d.car'][:,1] - data_2['vel.3d.car'][:,0]*data_2['pos.3d.car'][:,1])/(8*data_2['vrot.z0'])
    pos_mask_2 = (jphi_2 > 0.5) & (jphi_2 < 1.2) & (jz_2 > 0) & (jz_2 < 437.5)
    neg_mask_2 = (jphi_2 < -0.5) & (jphi_2 > -1.2) & (jz_2 > 0) & (jz_2 < 437.5)
    asymmetry_r412_2.append(np.sum(data_2['masses'][pos_mask_2])/np.sum(data_2['masses'][neg_mask_2]))
    #
    # Get the initial number of metal-poor, and prograde/retrograde stars
    N_prograde_r412_1 = np.zeros(len(snap_track), int)
    N_retrograde_r412_1 = np.zeros(len(snap_track), int)
    N_prograde_r412_1[0] = np.sum(pos_mask_1)
    N_retrograde_r412_1[0] = np.sum(neg_mask_1)
    #
    N_prograde_r412_2 = np.zeros(len(snap_track), int)
    N_retrograde_r412_2 = np.zeros(len(snap_track), int)
    N_prograde_r412_2[0] = np.sum(pos_mask_2)
    N_retrograde_r412_2[0] = np.sum(neg_mask_2)
    #
    # Get the masses of metal-poor, prograde/retrograde stars
    M_prograde_r412_1 = np.zeros(len(snap_track), int)
    M_retrograde_r412_1 = np.zeros(len(snap_track), int)
    M_prograde_r412_1[0] = np.sum(data_1['masses'][pos_mask_1])
    M_retrograde_r412_1[0] = np.sum(data_1['masses'][neg_mask_1])
    #
    M_prograde_r412_2 = np.zeros(len(snap_track), int)
    M_retrograde_r412_2 = np.zeros(len(snap_track), int)
    M_prograde_r412_2[0] = np.sum(data_2['masses'][pos_mask_2])
    M_retrograde_r412_2[0] = np.sum(data_2['masses'][neg_mask_2])
    #
    # 15 kpc selection
    # Get the asymmetry parameter at z = 0 first
    asymmetry_r15_1 = []
    asymmetry_r15_2 = []
    #
    metal_inds = ut.array.get_indices(part_z0['star'].prop('metallicity.iron'), [-np.inf, -2.5])
    inds_1 = ut.array.get_indices(part_z0['star'].prop('star.host1.distance.principal.total'), [0,15], metal_inds)
    inds_2 = ut.array.get_indices(part_z0['star'].prop('star.host2.distance.principal.total'), [0,15], metal_inds)
    #
    jz_1 = np.abs(part_z0['star'].prop('star.host1.distance.principal.cylindrical', inds_1)[:,2]*part_z0['star'].prop('star.host1.velocity.principal.cylindrical', inds_1)[:,2])
    jphi_1 = (part_z0['star'].prop('star.host1.distance.principal', inds_1)[:,0]*part_z0['star'].prop('star.host1.velocity.principal', inds_1)[:,1] - part_z0['star'].prop('star.host1.velocity.principal', inds_1)[:,0]*part_z0['star'].prop('star.host1.distance.principal', inds_1)[:,1])/(8*data_1['vrot.z0'])
    pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
    neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
    asymmetry_r15_1.append(np.sum(part_z0['star']['mass'][inds_1][pos_mask_1])/np.sum(part_z0['star']['mass'][inds_1][neg_mask_1]))
    #
    jz_2 = np.abs(part_z0['star'].prop('star.host2.distance.principal.cylindrical', inds_2)[:,2]*part_z0['star'].prop('star.host2.velocity.principal.cylindrical', inds_2)[:,2])
    jphi_2 = (part_z0['star'].prop('star.host2.distance.principal', inds_2)[:,0]*part_z0['star'].prop('star.host2.velocity.principal', inds_2)[:,1] - part_z0['star'].prop('star.host2.velocity.principal', inds_2)[:,0]*part_z0['star'].prop('star.host2.distance.principal', inds_2)[:,1])/(8*data_2['vrot.z0'])
    pos_mask_2 = (jphi_2 > 0.5) & (jphi_2 < 1.2) & (jz_2 > 0) & (jz_2 < 437.5)
    neg_mask_2 = (jphi_2 < -0.5) & (jphi_2 > -1.2) & (jz_2 > 0) & (jz_2 < 437.5)
    asymmetry_r15_2.append(np.sum(part_z0['star']['mass'][inds_2][pos_mask_2])/np.sum(part_z0['star']['mass'][inds_2][neg_mask_2]))
    #
    # Get the initial number of metal-poor, and prograde/retrograde stars
    N_prograde_r15_1 = np.zeros(len(snap_track), int)
    N_retrograde_r15_1 = np.zeros(len(snap_track), int)
    N_prograde_r15_1[0] = np.sum(pos_mask_1)
    N_retrograde_r15_1[0] = np.sum(neg_mask_1)
    #
    N_prograde_r15_2 = np.zeros(len(snap_track), int)
    N_retrograde_r15_2 = np.zeros(len(snap_track), int)
    N_prograde_r15_2[0] = np.sum(pos_mask_2)
    N_retrograde_r15_2[0] = np.sum(neg_mask_2)
    #
    # Get the masses of metal-poor, prograde/retrograde stars
    M_prograde_r15_1 = np.zeros(len(snap_track), int)
    M_retrograde_r15_1 = np.zeros(len(snap_track), int)
    M_prograde_r15_1[0] = np.sum(part_z0['star']['mass'][inds_1][pos_mask_1])
    M_retrograde_r15_1[0] = np.sum(part_z0['star']['mass'][inds_1][neg_mask_1])
    #
    M_prograde_r15_2 = np.zeros(len(snap_track), int)
    M_retrograde_r15_2 = np.zeros(len(snap_track), int)
    M_prograde_r15_2[0] = np.sum(part_z0['star']['mass'][inds_2][pos_mask_2])
    M_retrograde_r15_2[0] = np.sum(part_z0['star']['mass'][inds_2][neg_mask_2])
    for i in range(1, len(snap_track)):
        #
        # Read in the particles at a snapshot
        part = gizmo.io.Read.read_snapshots('star', 'snapshot', snap_track[i], simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=True) # Originally True when I wanted to only track the stars we select at z = 0
        print('Stars read in')
        #
        # Get the MOI tensor from the z = 0 dictionary; still using MOI(z)
        part.host['rotation'][0] = part_z0.hostz['rotation'][snap_track[i]][0]
        part.host['rotation'][1] = part_z0.hostz['rotation'][snap_track[i]][1]
        #
        # Find the rotational velocity of the galaxy at this snapshot
        # NOTE: This is only for the v_rot(z) - MOI(z) selection
        ind_rot_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
        ind_rot_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], ind_rot_1)
        v_rot_z_1 = np.median(part['star'].prop('star.host1.velocity.principal.cylindrical', ind_rot_1)[:,1])
        #v_rot_z_1 = data_1['vrot.z0'] # This is the rotational velocity at z = 0
        #
        ind_rot_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12])
        ind_rot_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], ind_rot_2)
        v_rot_z_2 = np.median(part['star'].prop('star.host2.velocity.principal.cylindrical', ind_rot_2)[:,1])
        #v_rot_z_2 = data_2['vrot.z0'] # This is the rotational velocity at z = 0
        #
        """
        # This chunk is for tracking the z = 0 stars

        # Read in the pointers and get the star indices at this snapshot
        pointers = part.Pointer.get_pointers(species_name_from='star', species_names_to='star')
        stars_at_z_1 = pointers[star_ind_1]
        stars_at_z_2 = pointers[star_ind_2]
        ind_mask_1 = (stars_at_z_1 > 0)
        ind_mask_2 = (stars_at_z_2 > 0)
        print('Pointers assigned')
        #
        # Mask the stars that are within the distance selections
        stars_at_z_r412_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12], stars_at_z_1[ind_mask_1])
        stars_at_z_r412_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412_1)
        #
        stars_at_z_r412_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12], stars_at_z_2[ind_mask_2])
        stars_at_z_r412_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412_2)
        #
        stars_at_z_r15_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [0,15], stars_at_z_1[ind_mask_1])
        stars_at_z_r15_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [0,15], stars_at_z_2[ind_mask_2])
        """


        # This chunk is for using all metal-poor stars, not just the preselected ones at z = 0
        #
        # Disk selection
        # Get the metal-poor stars in spatial selection window.
        stars_at_z_r412_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r412_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12], stars_at_z_r412_1)
        stars_at_z_r412_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412_1)
        #
        stars_at_z_r412_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r412_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12], stars_at_z_r412_2)
        stars_at_z_r412_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], stars_at_z_r412_2)
        #

        # Get the stars that are prograde and retrograde
        jz_r412_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', stars_at_z_r412_1)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', stars_at_z_r412_1)[:,2])
        jphi_r412_1 = (part['star'].prop('star.host1.distance.principal', stars_at_z_r412_1)[:,0]*part['star'].prop('star.host1.velocity.principal', stars_at_z_r412_1)[:,1] - part['star'].prop('star.host1.velocity.principal', stars_at_z_r412_1)[:,0]*part['star'].prop('star.host1.distance.principal', stars_at_z_r412_1)[:,1])/(8*v_rot_z_1)
        pos_mask_r412_1 = (jphi_r412_1 > 0.5) & (jphi_r412_1 < 1.2) & (jz_r412_1 > 0) & (jz_r412_1 < 437.5)
        neg_mask_r412_1 = (jphi_r412_1 < -0.5) & (jphi_r412_1 > -1.2) & (jz_r412_1 > 0) & (jz_r412_1 < 437.5)
        #
        jz_r412_2 = np.abs(part['star'].prop('star.host2.distance.principal.cylindrical', stars_at_z_r412_2)[:,2]*part['star'].prop('star.host2.velocity.principal.cylindrical', stars_at_z_r412_2)[:,2])
        jphi_r412_2 = (part['star'].prop('star.host2.distance.principal', stars_at_z_r412_2)[:,0]*part['star'].prop('star.host2.velocity.principal', stars_at_z_r412_2)[:,1] - part['star'].prop('star.host2.velocity.principal', stars_at_z_r412_2)[:,0]*part['star'].prop('star.host2.distance.principal', stars_at_z_r412_2)[:,1])/(8*v_rot_z_2)
        pos_mask_r412_2 = (jphi_r412_2 > 0.5) & (jphi_r412_2 < 1.2) & (jz_r412_2 > 0) & (jz_r412_2 < 437.5)
        neg_mask_r412_2 = (jphi_r412_2 < -0.5) & (jphi_r412_2 > -1.2) & (jz_r412_2 > 0) & (jz_r412_2 < 437.5)
        #
        N_prograde_r412_1[i] = np.sum(pos_mask_r412_1)
        N_retrograde_r412_1[i] = np.sum(neg_mask_r412_1)
        M_prograde_r412_1[i] = np.sum(part['star']['mass'][stars_at_z_r412_1][pos_mask_r412_1])
        M_retrograde_r412_1[i] = np.sum(part['star']['mass'][stars_at_z_r412_1][neg_mask_r412_1])
        #
        N_prograde_r412_2[i] = np.sum(pos_mask_r412_2)
        N_retrograde_r412_2[i] = np.sum(neg_mask_r412_2)
        M_prograde_r412_2[i] = np.sum(part['star']['mass'][stars_at_z_r412_2][pos_mask_r412_2])
        M_retrograde_r412_2[i] = np.sum(part['star']['mass'][stars_at_z_r412_2][neg_mask_r412_2])
        #
        # Figure out what the asymmetry parameter is at this snapshot
        if np.sum(neg_mask_r412_1) != 0:
            asymmetry_r412_1.append(np.sum(part['star']['mass'][stars_at_z_r412_1][pos_mask_r412_1])/np.sum(part['star']['mass'][stars_at_z_r412_1][neg_mask_r412_1]))
        else:
            asymmetry_r412_1.append(1e6)
        #
        if np.sum(neg_mask_r412_2) != 0:
            asymmetry_r412_2.append(np.sum(part['star']['mass'][stars_at_z_r412_2][pos_mask_r412_2])/np.sum(part['star']['mass'][stars_at_z_r412_2][neg_mask_r412_2]))
        else:
            asymmetry_r412_2.append(1e6)


        # This chunk is for using all metal-poor stars, not just the preselected ones at z = 0
        # 15 kpc selection
        # Get the metal-poor stars in spatial selection window.
        stars_at_z_r15_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r15_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.total'), [0,15], stars_at_z_r15_1)
        #
        stars_at_z_r15_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5])
        stars_at_z_r15_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.total'), [0,15], stars_at_z_r15_2)
        #

        # Get the stars that are prograde and retrograde
        jz_r15_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', stars_at_z_r15_1)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', stars_at_z_r15_1)[:,2])
        jphi_r15_1 = (part['star'].prop('star.host1.distance.principal', stars_at_z_r15_1)[:,0]*part['star'].prop('star.host1.velocity.principal', stars_at_z_r15_1)[:,1] - part['star'].prop('star.host1.velocity.principal', stars_at_z_r15_1)[:,0]*part['star'].prop('star.host1.distance.principal', stars_at_z_r15_1)[:,1])/(8*v_rot_z_1)
        pos_mask_r15_1 = (jphi_r15_1 > 0.5) & (jphi_r15_1 < 1.2) & (jz_r15_1 > 0) & (jz_r15_1 < 437.5)
        neg_mask_r15_1 = (jphi_r15_1 < -0.5) & (jphi_r15_1 > -1.2) & (jz_r15_1 > 0) & (jz_r15_1 < 437.5)
        #
        jz_r15_2 = np.abs(part['star'].prop('star.host2.distance.principal.cylindrical', stars_at_z_r15_2)[:,2]*part['star'].prop('star.host2.velocity.principal.cylindrical', stars_at_z_r15_2)[:,2])
        jphi_r15_2 = (part['star'].prop('star.host2.distance.principal', stars_at_z_r15_2)[:,0]*part['star'].prop('star.host2.velocity.principal', stars_at_z_r15_2)[:,1] - part['star'].prop('star.host2.velocity.principal', stars_at_z_r15_2)[:,0]*part['star'].prop('star.host2.distance.principal', stars_at_z_r15_2)[:,1])/(8*v_rot_z_2)
        pos_mask_r15_2 = (jphi_r15_2 > 0.5) & (jphi_r15_2 < 1.2) & (jz_r15_2 > 0) & (jz_r15_2 < 437.5)
        neg_mask_r15_2 = (jphi_r15_2 < -0.5) & (jphi_r15_2 > -1.2) & (jz_r15_2 > 0) & (jz_r15_2 < 437.5)
        #
        N_prograde_r15_1[i] = np.sum(pos_mask_r15_1)
        N_retrograde_r15_1[i] = np.sum(neg_mask_r15_1)
        M_prograde_r15_1[i] = np.sum(part['star']['mass'][stars_at_z_r15_1][pos_mask_r15_1])
        M_retrograde_r15_1[i] = np.sum(part['star']['mass'][stars_at_z_r15_1][neg_mask_r15_1])
        #
        N_prograde_r15_2[i] = np.sum(pos_mask_r15_2)
        N_retrograde_r15_2[i] = np.sum(neg_mask_r15_2)
        M_prograde_r15_2[i] = np.sum(part['star']['mass'][stars_at_z_r15_2][pos_mask_r15_2])
        M_retrograde_r15_2[i] = np.sum(part['star']['mass'][stars_at_z_r15_2][neg_mask_r15_2])
        #
        # Figure out what the asymmetry parameter is at this snapshot
        if np.sum(neg_mask_r15_1) != 0:
            asymmetry_r15_1.append(np.sum(part['star']['mass'][stars_at_z_r15_1][pos_mask_r15_1])/np.sum(part['star']['mass'][stars_at_z_r15_1][neg_mask_r15_1]))
        else:
            asymmetry_r15_1.append(1e6)
        #
        if np.sum(neg_mask_r15_2) != 0:
            asymmetry_r15_2.append(np.sum(part['star']['mass'][stars_at_z_r15_2][pos_mask_r15_2])/np.sum(part['star']['mass'][stars_at_z_r15_2][neg_mask_r15_2]))
        else:
            asymmetry_r15_2.append(1e6)
        print('Done with current snapshot')

    # Plot the asymmetry parameter vs time (or redshift)
    asymmetry_r412_1 = np.asarray(asymmetry_r412_1)
    asymmetry_r412_2 = np.asarray(asymmetry_r412_2)
    #
    asymmetry_r15_1 = np.asarray(asymmetry_r15_1)
    asymmetry_r15_2 = np.asarray(asymmetry_r15_2)
    #
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r412_1 != 1e6], asymmetry_r412_1[asymmetry_r412_1 != 1e6], color=gal_color, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r412_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r412_1, color='b', linestyle='-', label='Prograde (disk selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r412_1, color='r', linestyle='-', label='Retrograde (disk selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r412_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(3)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r15_1 != 1e6], asymmetry_r15_1[asymmetry_r15_1 != 1e6], color=gal_color, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r15_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(4)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r15_1, color='b', linestyle='-', label='Prograde (15 kpc selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r15_1, color='r', linestyle='-', label='Retrograde (15 kpc selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal1+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r15_'+gal1+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(5)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r412_2 != 1e6], asymmetry_r412_2[asymmetry_r412_2 != 1e6], color=gal_color2, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal2+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r412_'+gal2+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(6)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r412_2, color='b', linestyle='-', label='Prograde (disk selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r412_2, color='r', linestyle='-', label='Retrograde (disk selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal2+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r412_'+gal2+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(7)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track][asymmetry_r15_2 != 1e6], asymmetry_r15_2[asymmetry_r15_2 != 1e6], color=gal_color2, linestyle='-')
    plt.hlines(1, 0, 14, 'k', 'dotted')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0,ymax=4)
    plt.title(gal2+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('M$_{\\rm pro}$/M$_{\\rm ret}$', fontsize=28)
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_r15_'+gal2+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()
    #
    plt.figure(8)
    plt.figure(figsize=(10, 8))
    plt.plot(snaps['time'][snap_track], N_prograde_r15_2, color='b', linestyle='-', label='Prograde (15 kpc selection)')
    plt.plot(snaps['time'][snap_track], N_retrograde_r15_2, color='r', linestyle='-', label='Retrograde (15 kpc selection)')
    plt.xlim(13.9,0)
    plt.ylim(ymin=0)
    plt.title(gal2+', v$_{\\rm rot}(z)$, MOI(z)', fontsize=24)
    plt.xlabel('time [Gyr]', fontsize=28)
    plt.ylabel('N', fontsize=28)
    plt.legend(prop={'size': 14})
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/sample_size_r15_'+gal2+'_vz_MOIz_all_fe_poor.pdf')
    plt.close()

    # Save the data to a dictionary and file
    dictionary_1 = dict()
    dictionary_1['asymmetry'] = asymmetry_r412_1
    dictionary_1['asymmetry.snapshots'] = snap_track
    dictionary_1['num.prograde'] = N_prograde_r412_1
    dictionary_1['num.retrograde'] = N_retrograde_r412_1
    dictionary_1['mass.prograde'] = M_prograde_r412_1
    dictionary_1['mass.retrograde'] = M_retrograde_r412_1
    #
    dictionary_12 = dict()
    dictionary_12['asymmetry'] = asymmetry_r15_1
    dictionary_12['asymmetry.snapshots'] = snap_track
    dictionary_12['num.prograde'] = N_prograde_r15_1
    dictionary_12['num.retrograde'] = N_retrograde_r15_1
    dictionary_12['mass.prograde'] = M_prograde_r15_1
    dictionary_12['mass.retrograde'] = M_retrograde_r15_1
    #
    dictionary_2 = dict()
    dictionary_2['asymmetry'] = asymmetry_r412_2
    dictionary_2['asymmetry.snapshots'] = snap_track
    dictionary_2['num.prograde'] = N_prograde_r412_2
    dictionary_2['num.retrograde'] = N_retrograde_r412_2
    dictionary_2['mass.prograde'] = M_prograde_r412_2
    dictionary_2['mass.retrograde'] = M_retrograde_r412_2
    #
    dictionary_22 = dict()
    dictionary_22['asymmetry'] = asymmetry_r15_2
    dictionary_22['asymmetry.snapshots'] = snap_track
    dictionary_22['num.prograde'] = N_prograde_r15_2
    dictionary_22['num.retrograde'] = N_retrograde_r15_2
    dictionary_22['mass.prograde'] = M_prograde_r15_2
    dictionary_22['mass.retrograde'] = M_retrograde_r15_2
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r412_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary_1, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asymmetry_r412_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary_2, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r15_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary_12, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asymmetry_r15_vz_MOIz_all_fe_poor', dict_or_array_to_write=dictionary_22, verbose=True)

    print('All data saved!')

    """
    NOTE: For both the plots and pickle files, for the newer stuff, I titled them with '_z0' at the end of the names.
    """
