#!/usr/bin/python3

"""
 ====================
 = Age vs Asymmetry =
 ====================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Select stars within the disk region and bin stars by age. Save to a file

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

ages = np.arange(5, 14.1, 0.333)
#
if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    #
    # Read in the particle data
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=False, assign_formation_coordinates=True)
    #
    # Get indices of all stars in spatial selection
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds_1)
    #
    # Set up empty array to save to
    asym_fe_poor_1 = np.zeros(len(ages)-1)
    abunds_1 = []
    # Loop over metallicity
    for i in range(0, len(ages)-1):
        # Select stars below metallicity threshold
        age_inds_1 = ut.array.get_indices(part['star'].prop('age'), [ages[i], ages[i+1]], inds_1)
        #
        if len(age_inds_1) != 0:
            abunds_1.append(part['star'].prop('metallicity.iron', age_inds_1))
            #
            if len(age_inds_1) == 1:
                # Calculate asymmetry
                jz_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', age_inds_1)[2]*part['star'].prop('star.host1.velocity.principal.cylindrical', age_inds_1)[2])
                jphi_1 = (part['star'].prop('star.host1.distance.principal', age_inds_1)[0][0]*part['star'].prop('star.host1.velocity.principal', age_inds_1)[0][1] - part['star'].prop('star.host1.velocity.principal', age_inds_1)[0][0]*part['star'].prop('star.host1.distance.principal', age_inds_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
                neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
            else:
                # Calculate asymmetry
                jz_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', age_inds_1)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', age_inds_1)[:,2])
                jphi_1 = (part['star'].prop('star.host1.distance.principal', age_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal', age_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal', age_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal', age_inds_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
                neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
            #
            if np.sum(neg_mask_1) != 0:
                asym_fe_poor_1[i] =  np.sum(pos_mask_1)/np.sum(neg_mask_1)
            else:
                asym_fe_poor_1[i] = np.nan
        else:
            asym_fe_poor_1[i] = np.nan
            abunds_1.append(np.nan)
    #
    # Save the data
    d_1 = dict()
    d_1['asymmetry.age'] = asym_fe_poor_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_age_no_fe_333', dict_or_array_to_write=d_1, verbose=True)
    #
    # Save the abundance list to a pickle file
    Filep1 = open(home_dir+"/iron_poor_data/hdf5_files/"+gal1+"_age_metals.p", "wb")
    pickle.dump(abunds_1, Filep1)
    Filep1.close()

if num_gal == 2:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    #
    # Read in the particle data
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_pointers=False, assign_formation_coordinates=True)
    #
    # Calculate the rotational speed of the galaxy using all stars in spatial selection
    # Get indices of all stars in spatial selection
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds_1 = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds_1)
    #
    inds_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12])
    inds_2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], inds_2)
    #
    # Set up empty array to save to
    asym_fe_poor_1 = np.zeros(len(ages)-1)
    asym_fe_poor_2 = np.zeros(len(ages)-1)
    #
    abunds_1 = []
    abunds_2 = []
    # Loop over metallicity
    for i in range(0, len(ages)-1):
        # Select stars below metallicity threshold
        age_inds_1 = ut.array.get_indices(part['star'].prop('age'), [ages[i], ages[i+1]], inds_1)
        #
        if len(age_inds_1) != 0:
            abunds_1.append(part['star'].prop('metallicity.iron', age_inds_1))
            #
            if len(age_inds_1) == 1:
                # Calculate asymmetry
                jz_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', age_inds_1)[2]*part['star'].prop('star.host1.velocity.principal.cylindrical', age_inds_1)[2])
                jphi_1 = (part['star'].prop('star.host1.distance.principal', age_inds_1)[0][0]*part['star'].prop('star.host1.velocity.principal', age_inds_1)[0][1] - part['star'].prop('star.host1.velocity.principal', age_inds_1)[0][0]*part['star'].prop('star.host1.distance.principal', age_inds_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
                neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
            else:
                # Calculate asymmetry
                jz_1 = np.abs(part['star'].prop('star.host1.distance.principal.cylindrical', age_inds_1)[:,2]*part['star'].prop('star.host1.velocity.principal.cylindrical', age_inds_1)[:,2])
                jphi_1 = (part['star'].prop('star.host1.distance.principal', age_inds_1)[:,0]*part['star'].prop('star.host1.velocity.principal', age_inds_1)[:,1] - part['star'].prop('star.host1.velocity.principal', age_inds_1)[:,0]*part['star'].prop('star.host1.distance.principal', age_inds_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask_1 = (jphi_1 > 0.5) & (jphi_1 < 1.2) & (jz_1 > 0) & (jz_1 < 437.5)
                neg_mask_1 = (jphi_1 < -0.5) & (jphi_1 > -1.2) & (jz_1 > 0) & (jz_1 < 437.5)
            #
            if np.sum(neg_mask_1) != 0:
                asym_fe_poor_1[i] =  np.sum(pos_mask_1)/np.sum(neg_mask_1)
            else:
                asym_fe_poor_1[i] = np.nan
            #
        else:
            asym_fe_poor_1[i] = np.nan
            abunds_1.append(np.nan)
        #
        age_inds_2 = ut.array.get_indices(part['star'].prop('age'), [ages[i], ages[i+1]], inds_2)
        #
        if len(age_inds_2) != 0:
            abunds_2.append(part['star'].prop('metallicity.iron', age_inds_2))
            #
            if len(age_inds_2) == 1:
                # Calculate asymmetry
                jz_2 = np.abs(part['star'].prop('star.host2.distance.principal.cylindrical', age_inds_2)[2]*part['star'].prop('star.host2.velocity.principal.cylindrical', age_inds_2)[2])
                jphi_2 = (part['star'].prop('star.host2.distance.principal', age_inds_2)[0][0]*part['star'].prop('star.host2.velocity.principal', age_inds_2)[0][1] - part['star'].prop('star.host2.velocity.principal', age_inds_2)[0][0]*part['star'].prop('star.host2.distance.principal', age_inds_2)[0][1])/(8*data_2['vrot.z0'])
                pos_mask_2 = (jphi_2 > 0.5) & (jphi_2 < 1.2) & (jz_2 > 0) & (jz_2 < 437.5)
                neg_mask_2 = (jphi_2 < -0.5) & (jphi_2 > -1.2) & (jz_2 > 0) & (jz_2 < 437.5)
            else:
                # Calculate asymmetry
                jz_2 = np.abs(part['star'].prop('star.host2.distance.principal.cylindrical', age_inds_2)[:,2]*part['star'].prop('star.host2.velocity.principal.cylindrical', age_inds_2)[:,2])
                jphi_2 = (part['star'].prop('star.host2.distance.principal', age_inds_2)[:,0]*part['star'].prop('star.host2.velocity.principal', age_inds_2)[:,1] - part['star'].prop('star.host2.velocity.principal', age_inds_2)[:,0]*part['star'].prop('star.host2.distance.principal', age_inds_2)[:,1])/(8*data_2['vrot.z0'])
                pos_mask_2 = (jphi_2 > 0.5) & (jphi_2 < 1.2) & (jz_2 > 0) & (jz_2 < 437.5)
                neg_mask_2 = (jphi_2 < -0.5) & (jphi_2 > -1.2) & (jz_2 > 0) & (jz_2 < 437.5)
            #
            if np.sum(neg_mask_2) != 0:
                asym_fe_poor_2[i] =  np.sum(pos_mask_2)/np.sum(neg_mask_2)
            else:
                asym_fe_poor_2[i] = np.nan
        else:
            asym_fe_poor_2[i] = np.nan
            abunds_2.append(np.nan)
    #
    # Save the data so that I can plot it later with asymmetry_vs_metallicity_plot.py
    d_1 = dict()
    d_1['asymmetry.age'] = asym_fe_poor_1
    #
    d_2 = dict()
    d_2['asymmetry.age'] = asym_fe_poor_2
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_age_no_fe_333', dict_or_array_to_write=d_1, verbose=True)
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asym_age_no_fe_333', dict_or_array_to_write=d_2, verbose=True)
    #
    # Save the abundance list to a pickle file
    Filep1 = open(home_dir+"/iron_poor_data/hdf5_files/"+gal1+"_age_metals.p", "wb")
    pickle.dump(abunds_1, Filep1)
    Filep1.close()
    Filep2 = open(home_dir+"/iron_poor_data/hdf5_files/"+gal2+"_age_metals.p", "wb")
    pickle.dump(abunds_2, Filep2)
    Filep2.close()
