#!/usr/bin/python3

"""
 ===============================
 = Asymmetry vs Property plots =
 ===============================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot the figures in the paper with broken axes.

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.ticker import StrMethodFormatter


### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus

loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
    simulation_dir = '/Users/isaiahsantistevan/simulation/galaxies/m12m_res7100'
    snaps = ut.simulation.read_snapshot_times(directory=home_dir+'/galaxies/m12m_res7100/')

else:
    plt.rcParams["font.family"] = "serif"
    home_dir = '/home1/05400/ibsantis/scripts'
print('Set paths')

### Read in data for each host
# m12b
halo_ses_pro_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_sestito_prograde_data')
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_gas_star_merger_mass')
gases_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_gas_checks')
# m12c
halo_ses_pro_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_sestito_prograde_data')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_gas_star_merger_mass')
gases_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_gas_checks')
# m12f
halo_ses_pro_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_sestito_prograde_data')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_gas_star_merger_mass')
gases_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_gas_checks')
# m12i
halo_ses_pro_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_sestito_prograde_data')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_gas_star_merger_mass')
gases_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_gas_checks')
# m12m
halo_ses_pro_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_sestito_prograde_data')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_gas_star_merger_mass')
gases_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_gas_checks')
# m12w
halo_ses_pro_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_sestito_prograde_data')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_gas_star_merger_mass')
gases_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_gas_checks')
# Romeo
halo_ses_pro_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_sestito_prograde_data')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_gas_star_merger_mass')
gases_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_gas_checks')
# Juliet
halo_ses_pro_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_sestito_prograde_data')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_gas_star_merger_mass')
gases_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_gas_checks')
# Thelma
halo_ses_pro_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_sestito_prograde_data')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_gas_star_merger_mass')
gases_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_gas_checks')
# Louise
halo_ses_pro_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_sestito_prograde_data')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_gas_star_merger_mass')
gases_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_gas_checks')
# Romulus
halo_ses_pro_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_sestito_prograde_data')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_gas_star_merger_mass')
gases_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_gas_checks')
# Remus
halo_ses_pro_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_sestito_prograde_data')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asymmetry_r412_vz_MOIz_all_fe_poor')
gas_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_gas_star_merger_mass')
gases_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_gas_checks')
# Read in the offset angle data
merger_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_primary_merger')
merger_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_primary_merger')
merger_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_primary_merger')
merger_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_primary_merger')
merger_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_primary_merger')
merger_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_primary_merger')
merger_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_primary_merger')
merger_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_primary_merger')
merger_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_primary_merger')
merger_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_primary_merger')
merger_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_primary_merger')
merger_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_primary_merger')


### Gather the data together
#
# z = 0 asymmetry
asym_tot = np.array([asym_m12b['asymmetry'][0], asym_m12c['asymmetry'][0], asym_m12f['asymmetry'][0], asym_m12i['asymmetry'][0], asym_m12m['asymmetry'][0], asym_m12w['asymmetry'][0], asym_Romeo['asymmetry'][0], asym_Juliet['asymmetry'][0], asym_Thelma['asymmetry'][0], asym_Louise['asymmetry'][0], asym_Romulus['asymmetry'][0], asym_Remus['asymmetry'][0]])
#
# Peak merger stellar mass
merger_mass_m12b = halo_ses_pro_m12b['star.mass.peak'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[0])[0][0]]
merger_mass_m12c = halo_ses_pro_m12c['star.mass.peak'][np.where(halo_ses_pro_m12c['num.contr'] == np.flip(np.sort(halo_ses_pro_m12c['num.contr']))[0])[0][0]]
merger_mass_m12f = halo_ses_pro_m12f['star.mass.peak'][np.where(halo_ses_pro_m12f['num.contr'] == np.flip(np.sort(halo_ses_pro_m12f['num.contr']))[0])[0][0]]
merger_mass_m12i = halo_ses_pro_m12i['star.mass.peak'][np.where(halo_ses_pro_m12i['num.contr'] == np.flip(np.sort(halo_ses_pro_m12i['num.contr']))[0])[0][0]]
merger_mass_m12m = halo_ses_pro_m12m['star.mass.peak'][np.where(halo_ses_pro_m12m['num.contr'] == np.flip(np.sort(halo_ses_pro_m12m['num.contr']))[0])[0][0]]
merger_mass_m12w = halo_ses_pro_m12w['star.mass.peak'][np.where(halo_ses_pro_m12w['num.contr'] == np.flip(np.sort(halo_ses_pro_m12w['num.contr']))[0])[0][0]]
merger_mass_Romeo = halo_ses_pro_Romeo['star.mass.peak'][np.where(halo_ses_pro_Romeo['num.contr'] == np.flip(np.sort(halo_ses_pro_Romeo['num.contr']))[0])[0][0]]
merger_mass_Juliet = halo_ses_pro_Juliet['star.mass.peak'][np.where(halo_ses_pro_Juliet['num.contr'] == np.flip(np.sort(halo_ses_pro_Juliet['num.contr']))[0])[0][0]]
merger_mass_Thelma = halo_ses_pro_Thelma['star.mass.peak'][np.where(halo_ses_pro_Thelma['num.contr'] == np.flip(np.sort(halo_ses_pro_Thelma['num.contr']))[0])[0][0]]
merger_mass_Louise = halo_ses_pro_Louise['star.mass.peak'][np.where(halo_ses_pro_Louise['num.contr'] == np.flip(np.sort(halo_ses_pro_Louise['num.contr']))[0])[0][0]]
merger_mass_Romulus = halo_ses_pro_Romulus['star.mass.peak'][np.where(halo_ses_pro_Romulus['num.contr'] == np.flip(np.sort(halo_ses_pro_Romulus['num.contr']))[0])[0][0]]
merger_mass_Remus = halo_ses_pro_Remus['star.mass.peak'][np.where(halo_ses_pro_Remus['num.contr'] == np.flip(np.sort(halo_ses_pro_Remus['num.contr']))[0])[0][0]]
merger_mass_tot = np.array([merger_mass_m12b, merger_mass_m12c, merger_mass_m12f, merger_mass_m12i, merger_mass_m12m, merger_mass_m12w, merger_mass_Romeo, merger_mass_Juliet, merger_mass_Thelma, merger_mass_Louise, merger_mass_Romulus, merger_mass_Remus])
#
# Merger gas mass
merger_mass_gas_tot = np.array([gas_m12b['300.Myr.gas.mass'][1], gas_m12c['300.Myr.gas.mass'][1], gas_m12f['300.Myr.gas.mass'][1], gas_m12i['300.Myr.gas.mass'][1], gas_m12m['300.Myr.gas.mass'][1], gas_m12w['300.Myr.gas.mass'][1], gas_Romeo['300.Myr.gas.mass'][1], gas_Juliet['300.Myr.gas.mass'][1], gas_Thelma['300.Myr.gas.mass'][1], gas_Louise['300.Myr.gas.mass'][1], gas_Romulus['300.Myr.gas.mass'][1], gas_Remus['300.Myr.gas.mass'][1]])
host_mass_gas_tot = np.array([gas_m12b['300.Myr.gas.mass.host'][1], gas_m12c['300.Myr.gas.mass.host'][1], gas_m12f['300.Myr.gas.mass.host'][1], gas_m12i['300.Myr.gas.mass.host'][1], gas_m12m['300.Myr.gas.mass.host'][1], gas_m12w['300.Myr.gas.mass.host'][1], gas_Romeo['300.Myr.gas.mass.host'][1], gas_Juliet['300.Myr.gas.mass.host'][1], gas_Thelma['300.Myr.gas.mass.host'][1], gas_Louise['300.Myr.gas.mass.host'][1], gas_Romulus['300.Myr.gas.mass.host'][1], gas_Remus['300.Myr.gas.mass.host'][1]])
merger_to_host_gas_mass = merger_mass_gas_tot/host_mass_gas_tot
#
# Stellar mass at 300 Myr
merger_mass_star_tot = np.array([gas_m12b['300.Myr.star.mass'][1], gas_m12c['300.Myr.star.mass'][1], gas_m12f['300.Myr.star.mass'][1], gas_m12i['300.Myr.star.mass'][1], gas_m12m['300.Myr.star.mass'][1], gas_m12w['300.Myr.star.mass'][1], gas_Romeo['300.Myr.star.mass'][1], gas_Juliet['300.Myr.star.mass'][1], gas_Thelma['300.Myr.star.mass'][1], gas_Louise['300.Myr.star.mass'][1], gas_Romulus['300.Myr.star.mass'][1], gas_Remus['300.Myr.star.mass'][1]])
#
# Host mass at merger time and merger peak mass time
gal_mass_at_merger = np.array([66680420000, 6190477000, 836417860, 4010100500, 10779446000, 7782403600, 838567400, 1215703700, 6062821000, 3749286400, 35851880000, 2091665000])
gal_mass_at_peak = np.array([52297950000, 2811530200, 418428320, 2675714800, 4758826500, 7446744600, 378752060, 301881250, 2001425800, 685157950, 20538417000, 889062600])
# Different masses (taken from my paper)
host_mass_tot = np.array([7.3e10, 5.1e10, 6.9e10, 5.5e10, 10e10, 4.8e10, 5.9e10, 3.3e10, 6.3e10, 2.3e10, 8.0e10, 4.0e10])
halo_mass_tot = np.array([1.4e12, 1.4e12, 1.7e12, 1.2e12, 1.6e12, 1.1e12, 1.3e12, 1.1e12, 1.4e12, 1.2e12, 2.1e12, 1.2e12])
gal_to_halo_mass = host_mass_tot/halo_mass_tot
merger_to_gal_mass = merger_mass_tot/gal_mass_at_merger
merger_to_gal_mass_peak = merger_mass_tot/gal_mass_at_peak
#
# Numbers of satellites above 1e5 for each host (taken from Jenna's paper)
n_sat = np.array([11, 23, 16, 12, 27, 22, 17, 20, 17, 23, 17, 13])
#
# Fraction from top merger
merger_contr_m12b = halo_ses_pro_m12b['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[0])[0][0]]
merger_contr_m12c = halo_ses_pro_m12c['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12c['num.contr'] == np.flip(np.sort(halo_ses_pro_m12c['num.contr']))[0])[0][0]]
merger_contr_m12f = halo_ses_pro_m12f['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12f['num.contr'] == np.flip(np.sort(halo_ses_pro_m12f['num.contr']))[0])[0][0]]
merger_contr_m12i = halo_ses_pro_m12i['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12i['num.contr'] == np.flip(np.sort(halo_ses_pro_m12i['num.contr']))[0])[0][0]]
merger_contr_m12m = halo_ses_pro_m12m['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12m['num.contr'] == np.flip(np.sort(halo_ses_pro_m12m['num.contr']))[0])[0][0]]
merger_contr_m12w = halo_ses_pro_m12w['num.contr.frac.pro.sest'][np.where(halo_ses_pro_m12w['num.contr'] == np.flip(np.sort(halo_ses_pro_m12w['num.contr']))[0])[0][0]]
merger_contr_Romeo = halo_ses_pro_Romeo['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Romeo['num.contr'] == np.flip(np.sort(halo_ses_pro_Romeo['num.contr']))[0])[0][0]]
merger_contr_Juliet = halo_ses_pro_Juliet['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Juliet['num.contr'] == np.flip(np.sort(halo_ses_pro_Juliet['num.contr']))[0])[0][0]]
merger_contr_Thelma = halo_ses_pro_Thelma['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Thelma['num.contr'] == np.flip(np.sort(halo_ses_pro_Thelma['num.contr']))[0])[0][0]]
merger_contr_Louise = halo_ses_pro_Louise['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Louise['num.contr'] == np.flip(np.sort(halo_ses_pro_Louise['num.contr']))[0])[0][0]]
merger_contr_Romulus = halo_ses_pro_Romulus['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Romulus['num.contr'] == np.flip(np.sort(halo_ses_pro_Romulus['num.contr']))[0])[0][0]]
merger_contr_Remus = halo_ses_pro_Remus['num.contr.frac.pro.sest'][np.where(halo_ses_pro_Remus['num.contr'] == np.flip(np.sort(halo_ses_pro_Remus['num.contr']))[0])[0][0]]
merger_contr_tot = np.array([merger_contr_m12b, merger_contr_m12c, merger_contr_m12f, merger_contr_m12i, merger_contr_m12m, merger_contr_m12w, merger_contr_Romeo, merger_contr_Juliet, merger_contr_Thelma, merger_contr_Louise, merger_contr_Romulus, merger_contr_Remus])
#
# Fraction formed in-situ (taken from 'insitu_and_mergers' table on computer; calculated from 'summary_data_stuff.py')
# Including (15 kpc) in-situ from only host, and in-situ from any halo within distance
#insitu_host_tot = np.array([0.00292397660818713, 0, 0.11003861003861, 0.0663449939686369, 0.00203562340966921, 0.00350877192982456, 0.115920763022744, 0.0435663627152989, 0, 0.177099236641221, 0, 0.0273775216138329])
insitu_tot = np.array([0.06158357771261, 0.214168039538715, 0.159309021113244, 0.240583232077764, 0.205193482688391, 0.126094570928196, 0.186627479794269, 0.154239019407559, 0.0987755102040816, 0.306578947368421, 0.0489690721649485, 0.103174603174603])
#
# Merger snapshots
merger_snap = np.array([470, 231, 104, 169, 208, 263,  71,  86, 246, 158, 285,  98]) # Read in from the data above
merger_lb = np.array([ 2.83453534,  8.98918149, 11.9080362 , 10.49698508,  9.56220147, 8.17275611, 12.52035399, 12.24925537,  8.60880408, 10.7522393 , 7.60205264, 12.02390745])
merger_reds = np.array([0.2386588 , 1.3420074 , 3.44444442, 2.0439024 , 1.56097555, 1.0930233 , 4.77981663, 4.08064508, 1.21830988, 2.2164948 , 0.95031059, 3.63768125])
#
# Formation times (from my paper)
form_time = np.array([10.98, 10.26, 12.00, 11.67, 10.26, 11.31, 12.84, 12.67, 9.88, 12.56, 12.67, 12.06])
#
# t50 and t90 (from Pratik)
t50 = np.array([6.47, 4.75, 5.55, 5.55, 5.07, 4.38, 8.04, 6.58, 4.61, 7.61, 6.62, 7.38])
t90 = np.array([1.25, 0.96, 1.17, 1.43, 1.47, 0.90, 2.11, 1.55, 1.06, 1.50, 1.36, 1.87])
#
# Gas properties
gases_host_merger = np.array([gases_m12b['gas.mass.host.merger'], gases_m12c['gas.mass.host.merger'], gases_m12f['gas.mass.host.merger'], gases_m12i['gas.mass.host.merger'], gases_m12m['gas.mass.host.merger'], gases_m12w['gas.mass.host.merger'], gases_Romeo['gas.mass.host.merger'], gases_Juliet['gas.mass.host.merger'], gases_Thelma['gas.mass.host.merger'], gases_Louise['gas.mass.host.merger'], gases_Romulus['gas.mass.host.merger'], gases_Remus['gas.mass.host.merger']])
gases_host_300 = np.array([gases_m12b['gas.mass.host.300'], gases_m12c['gas.mass.host.300'], gases_m12f['gas.mass.host.300'], gases_m12i['gas.mass.host.300'], gases_m12m['gas.mass.host.300'], gases_m12w['gas.mass.host.300'], gases_Romeo['gas.mass.host.300'], gases_Juliet['gas.mass.host.300'], gases_Thelma['gas.mass.host.300'], gases_Louise['gas.mass.host.300'], gases_Romulus['gas.mass.host.300'], gases_Remus['gas.mass.host.300']])
gases_total_300 = np.array([gases_m12b['gass.mass.sum.300'], gases_m12c['gass.mass.sum.300'], gases_m12f['gass.mass.sum.300'], gases_m12i['gass.mass.sum.300'], gases_m12m['gass.mass.sum.300'], gases_m12w['gass.mass.sum.300'], gases_Romeo['gass.mass.sum.300'], gases_Juliet['gass.mass.sum.300'], gases_Thelma['gass.mass.sum.300'], gases_Louise['gass.mass.sum.300'], gases_Romulus['gass.mass.sum.300'], gases_Remus['gass.mass.sum.300']])
gases_ratio_2r90 = np.array([gases_m12b['gas.mass.ratio.2R90'], gases_m12c['gas.mass.ratio.2R90'], gases_m12f['gas.mass.ratio.2R90'], gases_m12i['gas.mass.ratio.2R90'], gases_m12m['gas.mass.ratio.2R90'], gases_m12w['gas.mass.ratio.2R90'], gases_Romeo['gas.mass.ratio.2R90'], gases_Juliet['gas.mass.ratio.2R90'], gases_Thelma['gas.mass.ratio.2R90'], gases_Louise['gas.mass.ratio.2R90'], gases_Romulus['gas.mass.ratio.2R90'], gases_Remus['gas.mass.ratio.2R90']])
gases_ratio_cold = np.array([gases_m12b['gas.mass.ratio.cold'], gases_m12c['gas.mass.ratio.cold'], gases_m12f['gas.mass.ratio.cold'], gases_m12i['gas.mass.ratio.cold'], gases_m12m['gas.mass.ratio.cold'], gases_m12w['gas.mass.ratio.cold'], gases_Romeo['gas.mass.ratio.cold'], gases_Juliet['gas.mass.ratio.cold'], gases_Thelma['gas.mass.ratio.cold'], gases_Louise['gas.mass.ratio.cold'], gases_Romulus['gas.mass.ratio.cold'], gases_Remus['gas.mass.ratio.cold']])
gases_ratio_600 = np.array([gases_m12b['gas.mass.ratio.600'], gases_m12c['gas.mass.ratio.600'], gases_m12f['gas.mass.ratio.600'], gases_m12i['gas.mass.ratio.600'], gases_m12m['gas.mass.ratio.600'], gases_m12w['gas.mass.ratio.600'], gases_Romeo['gas.mass.ratio.600'], gases_Juliet['gas.mass.ratio.600'], gases_Thelma['gas.mass.ratio.600'], gases_Louise['gas.mass.ratio.600'], gases_Romulus['gas.mass.ratio.600'], gases_Remus['gas.mass.ratio.600']])
gases_halo_cold = np.array([gases_m12b['gas.mass.halo.cold'], gases_m12c['gas.mass.halo.cold'], gases_m12f['gas.mass.halo.cold'], gases_m12i['gas.mass.halo.cold'], gases_m12m['gas.mass.halo.cold'], gases_m12w['gas.mass.halo.cold'], gases_Romeo['gas.mass.halo.cold'], gases_Juliet['gas.mass.halo.cold'], gases_Thelma['gas.mass.halo.cold'], gases_Louise['gas.mass.halo.cold'], gases_Romulus['gas.mass.halo.cold'], gases_Remus['gas.mass.halo.cold']])
gases_host_cold = np.array([gases_m12b['gas.mass.host.cold'], gases_m12c['gas.mass.host.cold'], gases_m12f['gas.mass.host.cold'], gases_m12i['gas.mass.host.cold'], gases_m12m['gas.mass.host.cold'], gases_m12w['gas.mass.host.cold'], gases_Romeo['gas.mass.host.cold'], gases_Juliet['gas.mass.host.cold'], gases_Thelma['gas.mass.host.cold'], gases_Louise['gas.mass.host.cold'], gases_Romulus['gas.mass.host.cold'], gases_Remus['gas.mass.host.cold']])
#
# Gas fractions
gas_frac = merger_mass_gas_tot/(merger_mass_star_tot + merger_mass_gas_tot)
gas_frac_cold = gases_halo_cold/(merger_mass_star_tot + gases_halo_cold)
#
lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
# Get the merger times in terms of lookback time
merger_tlb_m12b = snaps['time'][600] - snaps['time'][halo_ses_pro_m12b['unique.snap'][np.where(halo_ses_pro_m12b['num.contr'] == np.flip(np.sort(halo_ses_pro_m12b['num.contr']))[0])[0][0]]]
merger_tlb_m12c = snaps['time'][600] - snaps['time'][halo_ses_pro_m12c['unique.snap'][np.where(halo_ses_pro_m12c['num.contr'] == np.flip(np.sort(halo_ses_pro_m12c['num.contr']))[0])[0][0]]]
merger_tlb_m12f = snaps['time'][600] - snaps['time'][halo_ses_pro_m12f['unique.snap'][np.where(halo_ses_pro_m12f['num.contr'] == np.flip(np.sort(halo_ses_pro_m12f['num.contr']))[0])[0][0]]]
merger_tlb_m12i = snaps['time'][600] - snaps['time'][halo_ses_pro_m12i['unique.snap'][np.where(halo_ses_pro_m12i['num.contr'] == np.flip(np.sort(halo_ses_pro_m12i['num.contr']))[0])[0][0]]]
merger_tlb_m12m = snaps['time'][600] - snaps['time'][halo_ses_pro_m12m['unique.snap'][np.where(halo_ses_pro_m12m['num.contr'] == np.flip(np.sort(halo_ses_pro_m12m['num.contr']))[0])[0][0]]]
merger_tlb_m12w = snaps['time'][600] - snaps['time'][halo_ses_pro_m12w['unique.snap'][np.where(halo_ses_pro_m12w['num.contr'] == np.flip(np.sort(halo_ses_pro_m12w['num.contr']))[0])[0][0]]]
merger_tlb_Romeo = snaps['time'][600] - snaps['time'][halo_ses_pro_Romeo['unique.snap'][np.where(halo_ses_pro_Romeo['num.contr'] == np.flip(np.sort(halo_ses_pro_Romeo['num.contr']))[0])[0][0]]]
merger_tlb_Juliet = snaps['time'][600] - snaps['time'][halo_ses_pro_Juliet['unique.snap'][np.where(halo_ses_pro_Juliet['num.contr'] == np.flip(np.sort(halo_ses_pro_Juliet['num.contr']))[0])[0][0]]]
merger_tlb_Thelma = snaps['time'][600] - snaps['time'][halo_ses_pro_Thelma['unique.snap'][np.where(halo_ses_pro_Thelma['num.contr'] == np.flip(np.sort(halo_ses_pro_Thelma['num.contr']))[0])[0][0]]]
merger_tlb_Louise = snaps['time'][600] - snaps['time'][halo_ses_pro_Louise['unique.snap'][np.where(halo_ses_pro_Louise['num.contr'] == np.flip(np.sort(halo_ses_pro_Louise['num.contr']))[0])[0][0]]]
merger_tlb_Romulus = snaps['time'][600] - snaps['time'][halo_ses_pro_Romulus['unique.snap'][np.where(halo_ses_pro_Romulus['num.contr'] == np.flip(np.sort(halo_ses_pro_Romulus['num.contr']))[0])[0][0]]]
merger_tlb_Remus = snaps['time'][600] - snaps['time'][halo_ses_pro_Remus['unique.snap'][np.where(halo_ses_pro_Remus['num.contr'] == np.flip(np.sort(halo_ses_pro_Remus['num.contr']))[0])[0][0]]]

# Mask out lookback times before the merger
mask_tlb_m12b = merger_m12b['tlb.pro'] > merger_tlb_m12b
mask_tlb_m12c = merger_m12c['tlb.pro'] > merger_tlb_m12c
mask_tlb_m12f = merger_m12f['tlb.pro'] > merger_tlb_m12f
mask_tlb_m12i = merger_m12i['tlb.pro'] > merger_tlb_m12i
mask_tlb_m12m = merger_m12m['tlb.pro'] > merger_tlb_m12m
mask_tlb_m12w = merger_m12w['tlb.pro'] > merger_tlb_m12w
mask_tlb_Romeo = merger_Romeo['tlb.pro'] > merger_tlb_Romeo
mask_tlb_Juliet = merger_Juliet['tlb.pro'] > merger_tlb_Juliet
mask_tlb_Thelma = merger_Thelma['tlb.pro'] > merger_tlb_Thelma
mask_tlb_Louise = merger_Louise['tlb.pro'] > merger_tlb_Louise
mask_tlb_Romulus = merger_Romulus['tlb.pro'] > merger_tlb_Romulus
mask_tlb_Remus = merger_Remus['tlb.pro'] > merger_tlb_Remus
#
# Get the times that are within 500 Myr before the merger
mask_m12b = (merger_m12b['tlb.pro'][mask_tlb_m12b] >= merger_m12b['tlb.pro'][mask_tlb_m12b][-1]) & (merger_m12b['tlb.pro'][mask_tlb_m12b] < (merger_m12b['tlb.pro'][mask_tlb_m12b][-1] + 0.5))
mask_m12c = (merger_m12c['tlb.pro'][mask_tlb_m12c] >= merger_m12c['tlb.pro'][mask_tlb_m12c][-1]) & (merger_m12c['tlb.pro'][mask_tlb_m12c] < (merger_m12c['tlb.pro'][mask_tlb_m12c][-1] + 0.5))
mask_m12f = (merger_m12f['tlb.pro'][mask_tlb_m12f] >= merger_m12f['tlb.pro'][mask_tlb_m12f][-1]) & (merger_m12f['tlb.pro'][mask_tlb_m12f] < (merger_m12f['tlb.pro'][mask_tlb_m12f][-1] + 0.5))
mask_m12i = (merger_m12i['tlb.pro'][mask_tlb_m12i] >= merger_m12i['tlb.pro'][mask_tlb_m12i][-1]) & (merger_m12i['tlb.pro'][mask_tlb_m12i] < (merger_m12i['tlb.pro'][mask_tlb_m12i][-1] + 0.5))
mask_m12m = (merger_m12m['tlb.pro'][mask_tlb_m12m] >= merger_m12m['tlb.pro'][mask_tlb_m12m][-1]) & (merger_m12m['tlb.pro'][mask_tlb_m12m] < (merger_m12m['tlb.pro'][mask_tlb_m12m][-1] + 0.5))
mask_m12w = (merger_m12w['tlb.pro'][mask_tlb_m12w] >= merger_m12w['tlb.pro'][mask_tlb_m12w][-1]) & (merger_m12w['tlb.pro'][mask_tlb_m12w] < (merger_m12w['tlb.pro'][mask_tlb_m12w][-1] + 0.5))
mask_Romeo = (merger_Romeo['tlb.pro'][mask_tlb_Romeo] >= merger_Romeo['tlb.pro'][mask_tlb_Romeo][-1]) & (merger_Romeo['tlb.pro'][mask_tlb_Romeo] < (merger_Romeo['tlb.pro'][mask_tlb_Romeo][-1] + 0.5))
mask_Juliet = (merger_Juliet['tlb.pro'][mask_tlb_Juliet] >= merger_Juliet['tlb.pro'][mask_tlb_Juliet][-1]) & (merger_Juliet['tlb.pro'][mask_tlb_Juliet] < (merger_Juliet['tlb.pro'][mask_tlb_Juliet][-1] + 0.5))
mask_Thelma = (merger_Thelma['tlb.pro'][mask_tlb_Thelma] >= merger_Thelma['tlb.pro'][mask_tlb_Thelma][-1]) & (merger_Thelma['tlb.pro'][mask_tlb_Thelma] < (merger_Thelma['tlb.pro'][mask_tlb_Thelma][-1] + 0.5))
mask_Louise = (merger_Louise['tlb.pro'][mask_tlb_Louise] >= merger_Louise['tlb.pro'][mask_tlb_Louise][-1]) & (merger_Louise['tlb.pro'][mask_tlb_Louise] < (merger_Louise['tlb.pro'][mask_tlb_Louise][-1] + 0.5))
mask_Romulus = (merger_Romulus['tlb.pro'][mask_tlb_Romulus] >= merger_Romulus['tlb.pro'][mask_tlb_Romulus][-1]) & (merger_Romulus['tlb.pro'][mask_tlb_Romulus] < (merger_Romulus['tlb.pro'][mask_tlb_Romulus][-1] + 0.5))
mask_Remus = (merger_Remus['tlb.pro'][mask_tlb_Remus] >= merger_Remus['tlb.pro'][mask_tlb_Remus][-1]) & (merger_Remus['tlb.pro'][mask_tlb_Remus] < (merger_Remus['tlb.pro'][mask_tlb_Remus][-1] + 0.5))
#
# Calculate the median offset angle during these times
angle_m12b = np.median(merger_m12b['theta.pro'][mask_tlb_m12b][mask_m12b])
angle_m12c = np.median(merger_m12c['theta.pro'][mask_tlb_m12c][mask_m12c])
angle_m12f = np.median(merger_m12f['theta.pro'][mask_tlb_m12f][mask_m12f])
angle_m12i = np.median(merger_m12i['theta.pro'][mask_tlb_m12i][mask_m12i])
angle_m12m = np.median(merger_m12m['theta.pro'][mask_tlb_m12m][mask_m12m])
angle_m12w = np.median(merger_m12w['theta.pro'][mask_tlb_m12w][mask_m12w])
angle_Romeo = np.median(merger_Romeo['theta.pro'][mask_tlb_Romeo][mask_Romeo])
angle_Juliet = np.median(merger_Juliet['theta.pro'][mask_tlb_Juliet][mask_Juliet])
angle_Thelma = np.median(merger_Thelma['theta.pro'][mask_tlb_Thelma][mask_Thelma])
angle_Louise = np.median(merger_Louise['theta.pro'][mask_tlb_Louise][mask_Louise])
angle_Romulus = np.median(merger_Romulus['theta.pro'][mask_tlb_Romulus][mask_Romulus])
angle_Remus = np.median(merger_Remus['theta.pro'][mask_tlb_Remus][mask_Remus])
# Median offset angles
angles = np.array([angle_m12b, angle_m12c, angle_m12f, angle_m12i, angle_m12m, angle_m12w, angle_Romeo, angle_Juliet, angle_Thelma, angle_Louise, angle_Romulus, angle_Remus])


# Asymmetry vs in-situ fractions and contributing fractions
# Set up the figure and plot the data
f, (ax, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw={'width_ratios': [5, 1]}, figsize=(10,8))
plt.tight_layout(False)
plt.subplots_adjust(hspace=0.1, bottom=0.15, left=0.15)
plt.gca().xaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
plt.gca().yaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
#
ax.scatter(asym_tot[0], insitu_tot[0], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150, label='$\\rm f_{in-situ}$')
ax.scatter(asym_tot[1], insitu_tot[1], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[2], insitu_tot[2], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[3], insitu_tot[3], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[5], insitu_tot[5], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[6], insitu_tot[6], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[7], insitu_tot[7], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[8], insitu_tot[8], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[9], insitu_tot[9], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[10], insitu_tot[10], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[11], insitu_tot[11], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
#
ax.scatter(asym_tot[0], merger_contr_tot[0], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150, label='$\\rm f_{merger,1}$')
ax.scatter(asym_tot[1], merger_contr_tot[1], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[2], merger_contr_tot[2], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[3], merger_contr_tot[3], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[5], merger_contr_tot[5], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[6], merger_contr_tot[6], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[7], merger_contr_tot[7], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[8], merger_contr_tot[8], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[9], merger_contr_tot[9], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[10], merger_contr_tot[10], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax.scatter(asym_tot[11], merger_contr_tot[11], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
#
ax.scatter(asym_tot[0], insitu_tot[0]+merger_contr_tot[0], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150, label='$\\rm f_{merger,1}+f_{in-situ}$')
ax.scatter(asym_tot[1], insitu_tot[1]+merger_contr_tot[1], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[2], insitu_tot[2]+merger_contr_tot[2], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[3], insitu_tot[3]+merger_contr_tot[3], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[5], insitu_tot[5]+merger_contr_tot[5], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[6], insitu_tot[6]+merger_contr_tot[6], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[7], insitu_tot[7]+merger_contr_tot[7], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[8], insitu_tot[8]+merger_contr_tot[8], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[9], insitu_tot[9]+merger_contr_tot[9], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[10], insitu_tot[10]+merger_contr_tot[10], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
ax.scatter(asym_tot[11], insitu_tot[11]+merger_contr_tot[11], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
#
ax2.scatter(asym_tot[4], insitu_tot[4], c='None', edgecolors=colors[3], alpha = 0.8, marker='s', s=150)
ax2.scatter(asym_tot[4], merger_contr_tot[4], c='None', edgecolors=colors[4], alpha = 0.8, marker='^', s=150)
ax2.scatter(asym_tot[4], insitu_tot[4]+merger_contr_tot[4], c=colors[0], edgecolors=colors[0], alpha = 0.8, marker='s', s=150)
#
# Get rid of the subplot walls that we don't want there, as well as their ticks
ax.spines['right'].set_visible(False)
ax2.spines['left'].set_visible(False)
#
ax.tick_params(which='both', right=False, labelsize=24)
ax2.tick_params(which='both', left=False, labelsize=24)
#
# Set the limits and the tick values
ax.set_xlim(0.9, 3.1)
ax2.set_xlim(8.8, 9.2)
#
ax.set_xticks(np.arange(0.8, 3.1, 0.1), minor=True)
ax2.set_xticks(np.arange(8.8, 9.2, 0.1), minor=True)
ax2.set_xticks([9])
#
# Plot the legend
handles, labels = ax.get_legend_handles_labels()
f.legend(handles, labels, ncol=1, prop={'size': 18}, bbox_to_anchor=(0.7, 0.975))
#
# Draw the slashes in the broken axis
d = 1.5
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=14, linestyle="none", color='k', mec='k', mew=2.5, clip_on=False)
ax.plot([1, 1], [0, 1], transform=ax.transAxes, **kwargs) # first bracket is x points, second is y points
ax2.plot([0, 0], [0, 1], transform=ax2.transAxes, **kwargs) # first bracket is x points, second is y points
#
# Add the axis labels and save the figure
f.text(0.6, 0.03, 'M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', ha='center', fontsize=32)
f.text(0.04, 0.57, 'fraction of prograde stars', va='center', rotation='vertical', fontsize=32)
#
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/asymmetry_vs_insitu_and_contr_pro.pdf', bbox_inches='tight', pad_inches=0.3)
plt.close()


#############


# merger lookback time  vs asymmetry
f, (ax, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [1, 5]}, figsize=(10,8))
plt.tight_layout(False)
plt.subplots_adjust(hspace=0.1, bottom=0.15, left=0.15)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
#
ax2.scatter(merger_lb[0], asym_tot[0], c=colors[0], alpha = 0.8, marker='o', s=200, label='m12b')
ax2.scatter(merger_lb[1], asym_tot[1], c=colors[1], alpha = 0.8, marker='o', s=200, label='m12c')
ax2.scatter(merger_lb[2], asym_tot[2], c=colors[6], alpha = 0.8, marker='o', s=200, label='m12f')
ax2.scatter(merger_lb[3], asym_tot[3], c=colors[3], alpha = 0.8, marker='o', s=200, label='m12i')
ax2.scatter(merger_lb[5], asym_tot[5], c=colors[5], alpha = 0.8, marker='o', s=200, label='m12w')
ax2.scatter(merger_lb[6], asym_tot[6], c=colors[0], alpha = 0.8, marker='s', s=200, label='Romeo')
ax2.scatter(merger_lb[7], asym_tot[7], c=colors[1], alpha = 0.8, marker='s', s=200, label='Juliet')
ax2.scatter(merger_lb[8], asym_tot[8], c=colors[6], alpha = 0.8, marker='s', s=200, label='Thelma')
ax2.scatter(merger_lb[9], asym_tot[9], c=colors[3], alpha = 0.8, marker='s', s=200, label='Louise')
ax2.scatter(merger_lb[10], asym_tot[10], c=colors[4], alpha = 0.8, marker='s', s=200, label='Romulus')
ax2.scatter(merger_lb[11], asym_tot[11], c=colors[5], alpha = 0.8, marker='s', s=200, label='Remus')
#
ax.scatter(merger_lb[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=200, label='m12m')
#
ax2.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
#
ax2.tick_params(which='both', top=False, labelsize=24)
ax.tick_params(which='both', bottom=False, labelsize=24)
#
# Set the limits and the tick values
ax2.set_ylim(0.9, 3.1)
ax.set_ylim(8.8, 9.2)
ax2.set_xlim(13,2)
#
ax2.set_yticks(np.arange(0.8, 3.1, 0.1), minor=True)
ax.set_yticks(np.arange(8.8, 9.2, 0.1), minor=True)
ax.set_yticks([9])
#
# Draw the slashes in the broken axis
d = 1.5
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=14, linestyle="none", color='k', mec='k', mew=2.5, clip_on=False)
ax.plot([0, 1], [0, 0], transform=ax.transAxes, **kwargs) # first bracket is x points, second is y points
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs) # first bracket is x points, second is y points
#
# Add the correlation stuff
f.text(0.2, 0.9, 'r${\\rm _s} = -0.25$', fontsize=18)
f.text(0.2, 0.86, 'p${\\rm _s}$-value$ = 0.43$', fontsize=18)
#
# Add the axis labels and save the figure
f.text(0.035, 0.57, 'M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', rotation='vertical', va='center', fontsize=32)
f.text(0.55, 0.035, 't$_{\\rm lb, merger}$ [Gyr]', ha='center', fontsize=32)
#
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_lb_vs_asymmetry.pdf', bbox_inches='tight', pad_inches=0.3)
plt.close()


#############


# peak stellar merger mass vs asymmetry
f, (ax, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [1, 5]}, figsize=(10,8))
plt.tight_layout(False)
plt.subplots_adjust(hspace=0.1, bottom=0.15, left=0.15)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
#
ax2.scatter(merger_mass_tot[0], asym_tot[0], c=colors[0], alpha = 0.8, marker='o', s=200, label='m12b')
ax2.scatter(merger_mass_tot[1], asym_tot[1], c=colors[1], alpha = 0.8, marker='o', s=200, label='m12c')
ax2.scatter(merger_mass_tot[2], asym_tot[2], c=colors[6], alpha = 0.8, marker='o', s=200, label='m12f')
ax2.scatter(merger_mass_tot[3], asym_tot[3], c=colors[3], alpha = 0.8, marker='o', s=200, label='m12i')
ax2.scatter(merger_mass_tot[5], asym_tot[5], c=colors[5], alpha = 0.8, marker='o', s=200, label='m12w')
ax2.scatter(merger_mass_tot[6], asym_tot[6], c=colors[0], alpha = 0.8, marker='s', s=200, label='Romeo')
ax2.scatter(merger_mass_tot[7], asym_tot[7], c=colors[1], alpha = 0.8, marker='s', s=200, label='Juliet')
ax2.scatter(merger_mass_tot[8], asym_tot[8], c=colors[6], alpha = 0.8, marker='s', s=200, label='Thelma')
ax2.scatter(merger_mass_tot[9], asym_tot[9], c=colors[3], alpha = 0.8, marker='s', s=200, label='Louise')
ax2.scatter(merger_mass_tot[10], asym_tot[10], c=colors[4], alpha = 0.8, marker='s', s=200, label='Romulus')
ax2.scatter(merger_mass_tot[11], asym_tot[11], c=colors[5], alpha = 0.8, marker='s', s=200, label='Remus')
#
ax.scatter(merger_mass_tot[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=200, label='m12m')
#
ax2.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
#
ax2.tick_params(which='both', top=False, labelsize=24)
ax.tick_params(which='both', bottom=False, labelsize=24)
#
# Set the limits and the tick values
ax2.set_ylim(0.9, 3.1)
ax.set_ylim(8.8, 9.2)
ax2.set_xlim(9e7,1e10)
ax2.set_xscale('log')
#
ax2.set_yticks(np.arange(0.8, 3.1, 0.1), minor=True)
ax.set_yticks(np.arange(8.8, 9.2, 0.1), minor=True)
ax.set_yticks([9])
#
# Draw the slashes in the broken axis
d = 1.5
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=14, linestyle="none", color='k', mec='k', mew=2.5, clip_on=False)
ax.plot([0, 1], [0, 0], transform=ax.transAxes, **kwargs) # first bracket is x points, second is y points
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs) # first bracket is x points, second is y points
#
# Add the correlation stuff
f.text(0.2, 0.9, 'r${\\rm _s} = 0.36$', fontsize=18)
f.text(0.2, 0.86, 'p${\\rm _s}$-value$ = 0.26$', fontsize=18)
#
# Add the axis labels and save the figure
f.text(0.035, 0.57, 'M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', rotation='vertical', va='center', fontsize=32)
f.text(0.55, 0.035, '$\\rm M_{star, merger}$ [$\\rm M_{\odot}$]', ha='center', fontsize=32)
#
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/merger_mass_vs_asymmetry.pdf', bbox_inches='tight', pad_inches=0.3)
plt.close()


#############


# Gas fraction
f, (ax, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [1, 5]}, figsize=(10,8))
plt.tight_layout(False)
plt.subplots_adjust(hspace=0.1, bottom=0.15, left=0.15)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
#
ax2.scatter(gas_frac[0], asym_tot[0], c=colors[0], alpha = 0.8, marker='o', s=200, label='m12b')
ax2.scatter(gas_frac[1], asym_tot[1], c=colors[1], alpha = 0.8, marker='o', s=200, label='m12c')
ax2.scatter(gas_frac[2], asym_tot[2], c=colors[6], alpha = 0.8, marker='o', s=200, label='m12f')
ax2.scatter(gas_frac[3], asym_tot[3], c=colors[3], alpha = 0.8, marker='o', s=200, label='m12i')
ax2.scatter(gas_frac[5], asym_tot[5], c=colors[5], alpha = 0.8, marker='o', s=200, label='m12w')
ax2.scatter(gas_frac[6], asym_tot[6], c=colors[0], alpha = 0.8, marker='s', s=200, label='Romeo')
ax2.scatter(gas_frac[7], asym_tot[7], c=colors[1], alpha = 0.8, marker='s', s=200, label='Juliet')
ax2.scatter(gas_frac[8], asym_tot[8], c=colors[6], alpha = 0.8, marker='s', s=200, label='Thelma')
ax2.scatter(gas_frac[9], asym_tot[9], c=colors[3], alpha = 0.8, marker='s', s=200, label='Louise')
ax2.scatter(gas_frac[10], asym_tot[10], c=colors[4], alpha = 0.8, marker='s', s=200, label='Romulus')
ax2.scatter(gas_frac[11], asym_tot[11], c=colors[5], alpha = 0.8, marker='s', s=200, label='Remus')
#
ax.scatter(gas_frac[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=200, label='m12m')
#
ax2.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
#
ax2.tick_params(which='both', top=False, labelsize=24)
ax.tick_params(which='both', bottom=False, labelsize=24)
#
# Set the limits and the tick values
ax2.set_ylim(0.9, 3.1)
ax.set_ylim(8.8, 9.2)
#
ax2.set_yticks(np.arange(0.8, 3.1, 0.1), minor=True)
ax.set_yticks(np.arange(8.8, 9.2, 0.1), minor=True)
ax.set_yticks([9])
#
# Draw the slashes in the broken axis
d = 1.5
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=14, linestyle="none", color='k', mec='k', mew=2.5, clip_on=False)
ax.plot([0, 1], [0, 0], transform=ax.transAxes, **kwargs) # first bracket is x points, second is y points
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs) # first bracket is x points, second is y points
#
# Add the correlation stuff
f.text(0.2, 0.9, 'r${\\rm _s} = -0.28$', fontsize=18)
f.text(0.2, 0.86, 'p${\\rm _s}$-value$ = 0.38$', fontsize=18)
#
# Add the axis labels and save the figure
f.text(0.035, 0.57, 'M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', rotation='vertical', va='center', fontsize=32)
f.text(0.55, 0.035, '$\\rm f_{gas}$', ha='center', fontsize=32)
#
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/gas_fraction_vs_asymmetry.pdf', bbox_inches='tight', pad_inches=0.3)
plt.close()


###########


# Offset angle versus asymmetry
f, (ax, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [1, 5]}, figsize=(10,8))
plt.tight_layout(False)
plt.subplots_adjust(hspace=0.1, bottom=0.15, left=0.15)
ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.1f}'))
#
ax2.scatter(angles[0], asym_tot[0], c=colors[0], alpha = 0.8, marker='o', s=200, label='m12b')
ax2.scatter(angles[1], asym_tot[1], c=colors[1], alpha = 0.8, marker='o', s=200, label='m12c')
ax2.scatter(angles[2], asym_tot[2], c=colors[6], alpha = 0.8, marker='o', s=200, label='m12f')
ax2.scatter(angles[3], asym_tot[3], c=colors[3], alpha = 0.8, marker='o', s=200, label='m12i')
ax2.scatter(angles[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=200, label='m12m')
ax2.scatter(angles[5], asym_tot[5], c=colors[5], alpha = 0.8, marker='o', s=200, label='m12w')
ax2.scatter(angles[6], asym_tot[6], c=colors[0], alpha = 0.8, marker='s', s=200, label='Romeo')
ax2.scatter(angles[7], asym_tot[7], c=colors[1], alpha = 0.8, marker='s', s=200, label='Juliet')
ax2.scatter(angles[8], asym_tot[8], c=colors[6], alpha = 0.8, marker='s', s=200, label='Thelma')
ax2.scatter(angles[9], asym_tot[9], c=colors[3], alpha = 0.8, marker='s', s=200, label='Louise')
ax2.scatter(angles[10], asym_tot[10], c=colors[4], alpha = 0.8, marker='s', s=200, label='Romulus')
ax2.scatter(angles[11], asym_tot[11], c=colors[5], alpha = 0.8, marker='s', s=200, label='Remus')
#
ax.scatter(angles[4], asym_tot[4], c=colors[4], alpha = 0.8, marker='o', s=200, label='m12m')
#
ax2.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
#
ax2.tick_params(which='both', top=False, labelsize=24)
ax.tick_params(which='both', bottom=False, labelsize=24)
#
# Set the limits and the tick values
ax2.set_ylim(0.9, 3.1)
ax.set_ylim(8.8, 9.2)
#
ax2.set_yticks(np.arange(0.8, 3.1, 0.1), minor=True)
ax.set_yticks(np.arange(8.8, 9.2, 0.1), minor=True)
ax.set_yticks([9.0])
#
# Draw the slashes in the broken axis
d = 1.5
kwargs = dict(marker=[(-1, -d), (1, d)], markersize=14, linestyle="none", color='k', mec='k', mew=2.5, clip_on=False)
ax.plot([0, 1], [0, 0], transform=ax.transAxes, **kwargs) # first bracket is x points, second is y points
ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs) # first bracket is x points, second is y points
#
# Add the legend
handles, labels = ax2.get_legend_handles_labels()
f.legend(handles, labels, ncol=2, prop={'size': 18}, bbox_to_anchor=(0.92, 0.975))
#
# Add the correlation stuff
f.text(0.2, 0.9, 'r${\\rm _s} = -0.01$', fontsize=18)
f.text(0.2, 0.86, 'p${\\rm _s}$-value$ = 0.97$', fontsize=18)
#
# Add the axis labels and save the figure
f.text(0.035, 0.57, 'M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', rotation='vertical', va='center', fontsize=32)
f.text(0.55, 0.035, '$\\rm \\theta_{\\rm offset, median}$', ha='center', fontsize=32)
#
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/offset_angle_vs_asymmetry.pdf', bbox_inches='tight', pad_inches=0.3)
plt.close()
