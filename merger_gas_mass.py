#!/usr/bin/python3

"""
 ===================
 = Merger gas mass =
 ===================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Find the mass of gas within R_90 of the halo that deposits the most prograde
       metal-poor stars.

       Manually saving the data in an array in "asymmetry_vs_prop_plot.py"

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt


### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']

if num_gal == 1:
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    #
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    #
    # Find the halo ID & snapshot of the most major prograde merger
    halo_snap_1 = halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    halo_id_1 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    halo_mass_1 = halo_ses_pro_1['star.mass.peak'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    #
    # Find the snapshots that are 100, 200, and 300 Myr before the merger
    snap_100_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.1)][-1]
    snap_200_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.2)][-1]
    snap_300_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.3)][-1]
    #
    time_merger_1 = snaps['time'][halo_snap_1]
    time_100_1 = snaps['time'][snap_100_1]
    time_200_1 = snaps['time'][snap_200_1]
    time_300_1 = snaps['time'][snap_300_1]
    #
    # Find the halo indices 100, 200, and 300 Myr before the merger
    halo_100_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_100_1]
    halo_200_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_200_1]
    halo_300_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_300_1]
    #
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part_100 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_100_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    part_200 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_200_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    part_300 = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_300_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_100_1 = (np.linalg.norm(part_100['gas']['position'] - halt['position'][halo_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][halo_100_1])
    mask_gas_200_1 = (np.linalg.norm(part_200['gas']['position'] - halt['position'][halo_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][halo_200_1])
    mask_gas_300_1 = (np.linalg.norm(part_300['gas']['position'] - halt['position'][halo_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][halo_300_1])
    # Stars
    mask_star_100_1 = (np.linalg.norm(part_100['star']['position'] - halt['position'][halo_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][halo_100_1])
    mask_star_200_1 = (np.linalg.norm(part_200['star']['position'] - halt['position'][halo_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][halo_200_1])
    mask_star_300_1 = (np.linalg.norm(part_300['star']['position'] - halt['position'][halo_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][halo_300_1])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 100 Myr
    v_mask_gas_100_1 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('star.vel.std.50', halo_100_1))
    v_mask_gas_100_2 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_1))
    v_mask_gas_100_3 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_1))
    # 200 Myr
    v_mask_gas_200_1 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('star.vel.std.50', halo_200_1))
    v_mask_gas_200_2 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_1))
    v_mask_gas_200_3 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_1))
    # 300 Myr
    v_mask_gas_300_1 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('star.vel.std.50', halo_300_1))
    v_mask_gas_300_2 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_1))
    v_mask_gas_300_3 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_1))
    # star
    # 100 Myr
    v_mask_star_100_1 = (np.linalg.norm(part_100['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('star.vel.std.50', halo_100_1))
    v_mask_star_100_2 = (np.linalg.norm(part_100['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_1))
    v_mask_star_100_3 = (np.linalg.norm(part_100['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_1))
    # 200 Myr
    v_mask_star_200_1 = (np.linalg.norm(part_200['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('star.vel.std.50', halo_200_1))
    v_mask_star_200_2 = (np.linalg.norm(part_200['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_1))
    v_mask_star_200_3 = (np.linalg.norm(part_200['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_1))
    # 300 Myr
    v_mask_star_300_1 = (np.linalg.norm(part_300['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('star.vel.std.50', halo_300_1))
    v_mask_star_300_2 = (np.linalg.norm(part_300['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_1))
    v_mask_star_300_3 = (np.linalg.norm(part_300['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_1))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    d_1 = dict()
    # Gas
    d_1['100.Myr.gas.mass'] = np.array([np.sum(part_100['gas']['mass'][mask_gas_100_1][v_mask_gas_100_1]), np.sum(part_100['gas']['mass'][mask_gas_100_1][v_mask_gas_100_2]), np.sum(part_100['gas']['mass'][mask_gas_100_1][v_mask_gas_100_3])])
    d_1['200.Myr.gas.mass'] = np.array([np.sum(part_200['gas']['mass'][mask_gas_200_1][v_mask_gas_200_1]), np.sum(part_200['gas']['mass'][mask_gas_200_1][v_mask_gas_200_2]), np.sum(part_200['gas']['mass'][mask_gas_200_1][v_mask_gas_200_3])])
    d_1['300.Myr.gas.mass'] = np.array([np.sum(part_300['gas']['mass'][mask_gas_300_1][v_mask_gas_300_1]), np.sum(part_300['gas']['mass'][mask_gas_300_1][v_mask_gas_300_2]), np.sum(part_300['gas']['mass'][mask_gas_300_1][v_mask_gas_300_3])])
    # Star
    d_1['100.Myr.star.mass'] = np.array([np.sum(part_100['star']['mass'][mask_star_100_1][v_mask_star_100_1]), np.sum(part_100['star']['mass'][mask_star_100_1][v_mask_star_100_2]), np.sum(part_100['star']['mass'][mask_star_100_1][v_mask_star_100_3])])
    d_1['200.Myr.star.mass'] = np.array([np.sum(part_200['star']['mass'][mask_star_200_1][v_mask_star_200_1]), np.sum(part_200['star']['mass'][mask_star_200_1][v_mask_star_200_2]), np.sum(part_200['star']['mass'][mask_star_200_1][v_mask_star_200_3])])
    d_1['300.Myr.star.mass'] = np.array([np.sum(part_300['star']['mass'][mask_star_300_1][v_mask_star_300_1]), np.sum(part_300['star']['mass'][mask_star_300_1][v_mask_star_300_2]), np.sum(part_300['star']['mass'][mask_star_300_1][v_mask_star_300_3])])
    #
    d_1['halo.dist'] = np.array([halt.prop('host.distance.total', halo_100_1), halt.prop('host.distance.total', halo_200_1), halt.prop('host.distance.total', halo_300_1)])
    #
    # Find the host index at these snapshots
    host_100_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_100_1]
    host_200_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_200_1]
    host_300_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_300_1]
    #
    # Select gas particles within the main host
    mask_gas_host_100_1 = (np.linalg.norm(part_100['gas']['position'] - halt['position'][host_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][host_100_1])
    mask_gas_host_200_1 = (np.linalg.norm(part_200['gas']['position'] - halt['position'][host_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][host_200_1])
    mask_gas_host_300_1 = (np.linalg.norm(part_300['gas']['position'] - halt['position'][host_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][host_300_1])
    #
    # Create mask for velocities
    # 100 Myr
    v_mask_gas_host_100_1 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < halt.prop('star.vel.std.50', host_100_1))
    v_mask_gas_host_100_2 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < 2*halt.prop('star.vel.std.50', host_100_1))
    v_mask_gas_host_100_3 = (np.linalg.norm(part_100['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < 3*halt.prop('star.vel.std.50', host_100_1))
    # 200 Myr
    v_mask_gas_host_200_1 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < halt.prop('star.vel.std.50', host_200_1))
    v_mask_gas_host_200_2 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < 2*halt.prop('star.vel.std.50', host_200_1))
    v_mask_gas_host_200_3 = (np.linalg.norm(part_200['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < 3*halt.prop('star.vel.std.50', host_200_1))
    # 300 Myr
    v_mask_gas_host_300_1 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < halt.prop('star.vel.std.50', host_300_1))
    v_mask_gas_host_300_2 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < 2*halt.prop('star.vel.std.50', host_300_1))
    v_mask_gas_host_300_3 = (np.linalg.norm(part_300['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < 3*halt.prop('star.vel.std.50', host_300_1))
    #
    # Save to the dictionary
    d_1['100.Myr.gas.mass.host'] = np.array([np.sum(part_100['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_1]), np.sum(part_100['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_2]), np.sum(part_100['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_3])])
    d_1['200.Myr.gas.mass.host'] = np.array([np.sum(part_200['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_1]), np.sum(part_200['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_2]), np.sum(part_200['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_3])])
    d_1['300.Myr.gas.mass.host'] = np.array([np.sum(part_300['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_1]), np.sum(part_300['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_2]), np.sum(part_300['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_3])])
    #
    d_1['snap.merger'] = halo_snap_1
    d_1['snap.100'] = snap_100_1
    d_1['snap.200'] = snap_200_1
    d_1['snap.300'] = snap_300_1
    #
    # Save the dictionary to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_gas_star_merger_mass', dict_or_array_to_write=d_1, verbose=True)
    #
    # Create a plot showing the gas masses at the different times for the different velocity selections
    times_1 = np.array([time_100_1, time_200_1, time_300_1])
    #
    fig = plt.figure(figsize=(10, 8))
    #
    plt.vlines(x=time_merger_1, ymin=0, ymax=1e12, color='k', linestyle=':')
    #
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7, label='1 $\\sigma$')
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7, label='2 $\\sigma$')
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7, label='3 $\\sigma$')
    #
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(time_merger_1, halo_mass_1, c='k', marker='*', s=95, alpha=0.7)
    plt.scatter(time_100_1, halt['star.mass'][halo_100_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_200_1, halt['star.mass'][halo_200_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_300_1, halt['star.mass'][halo_300_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    #
    # Label stuff
    plt.xlabel('time [Gyr]', fontsize=20)
    plt.ylabel('Mass within R$_{\\rm 90}$', fontsize=20)
    plt.title(gal1+' using star.vel.std.50', fontsize=24)
    plt.legend(prop={'size': 18}, loc='best')
    plt.tick_params(axis='x', which='major', labelsize=20)
    plt.tick_params(axis='y', which='major', labelsize=20)
    plt.xlim(time_merger_1+0.1, times_1[2]-0.1)
    plt.ylim(1e8, 1e10)
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal1+'_merger_gas_star_masses.pdf')
    plt.close()


if num_gal == 2:
    # Read in the halo tree
    halt = halo.io.IO.read_tree(simulation_directory=simulation_dir, species='star', assign_species_pointers=False, host_number=num_gal)
    #
    # Get the time dictionary
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    #
    #### Galaxy 1
    # Read in the metal-poor data
    data_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    #
    # Find the halo ID & snapshot of the most major prograde merger
    halo_snap_1 = halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    halo_id_1 = halo_ses_pro_1['unique.halo'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    halo_mass_1 = halo_ses_pro_1['star.mass.peak'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]
    #
    # Find the snapshots that are 100, 200, and 300 Myr before the merger
    snap_100_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.1)][-1]
    snap_200_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.2)][-1]
    snap_300_1 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_1]-0.3)][-1]
    #
    time_merger_1 = snaps['time'][halo_snap_1]
    time_100_1 = snaps['time'][snap_100_1]
    time_200_1 = snaps['time'][snap_200_1]
    time_300_1 = snaps['time'][snap_300_1]
    #
    # Find the halo indices 100, 200, and 300 Myr before the merger
    halo_100_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_100_1]
    halo_200_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_200_1]
    halo_300_1 = halt.prop('progenitor.main.indices', halo_id_1)[halo_snap_1 - snap_300_1]
    #
    ### 100 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_100_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_100_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][halo_100_1])
    # Stars
    mask_star_100_1 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][halo_100_1])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 100 Myr
    v_mask_gas_100_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('star.vel.std.50', halo_100_1))
    v_mask_gas_100_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_1))
    v_mask_gas_100_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_1))
    # star
    # 100 Myr
    v_mask_star_100_1_1 = (np.linalg.norm(part['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < halt.prop('star.vel.std.50', halo_100_1))
    v_mask_star_100_2_1 = (np.linalg.norm(part['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_1))
    v_mask_star_100_3_1 = (np.linalg.norm(part['star']['velocity'][mask_star_100_1] - halt.prop('velocity', halo_100_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_1))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    d_1 = dict()
    d_1['halo.dist'] = np.array([halt.prop('host.distance.total', halo_100_1), halt.prop('host.distance.total', halo_200_1), halt.prop('host.distance.total', halo_300_1)])
    # Gas
    d_1['100.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_100_1][v_mask_gas_100_1_1]), np.sum(part['gas']['mass'][mask_gas_100_1][v_mask_gas_100_2_1]), np.sum(part['gas']['mass'][mask_gas_100_1][v_mask_gas_100_3_1])])
    # Star
    d_1['100.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_100_1][v_mask_star_100_1_1]), np.sum(part['star']['mass'][mask_star_100_1][v_mask_star_100_2_1]), np.sum(part['star']['mass'][mask_star_100_1][v_mask_star_100_3_1])])
    #
    # Do the same for the host
    host_100_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_100_1]
    mask_gas_host_100_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_100_1], axis=1)*snaps['scalefactor'][snap_100_1] < halt['star.radius.90'][host_100_1])
    v_mask_gas_host_100_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < halt.prop('star.vel.std.50', host_100_1))
    v_mask_gas_host_100_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < 2*halt.prop('star.vel.std.50', host_100_1))
    v_mask_gas_host_100_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_1] - halt.prop('velocity', host_100_1), axis=1) < 3*halt.prop('star.vel.std.50', host_100_1))
    d_1['100.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_1_1]), np.sum(part['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_2_1]), np.sum(part['gas']['mass'][mask_gas_host_100_1][v_mask_gas_host_100_3_1])])
    #
    ### 200 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_200_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_200_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][halo_200_1])
    # Stars
    mask_star_200_1 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][halo_200_1])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 200 Myr
    v_mask_gas_200_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('star.vel.std.50', halo_200_1))
    v_mask_gas_200_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_1))
    v_mask_gas_200_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_1))
    # star
    # 200 Myr
    v_mask_star_200_1_1 = (np.linalg.norm(part['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < halt.prop('star.vel.std.50', halo_200_1))
    v_mask_star_200_2_1 = (np.linalg.norm(part['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_1))
    v_mask_star_200_3_1 = (np.linalg.norm(part['star']['velocity'][mask_star_200_1] - halt.prop('velocity', halo_200_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_1))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    # Gas
    d_1['200.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_200_1][v_mask_gas_200_1_1]), np.sum(part['gas']['mass'][mask_gas_200_1][v_mask_gas_200_2_1]), np.sum(part['gas']['mass'][mask_gas_200_1][v_mask_gas_200_3_1])])
    # Star
    d_1['200.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_200_1][v_mask_star_200_1_1]), np.sum(part['star']['mass'][mask_star_200_1][v_mask_star_200_2_1]), np.sum(part['star']['mass'][mask_star_200_1][v_mask_star_200_3_1])])
    #
    # Do the same for the host
    host_200_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_200_1]
    mask_gas_host_200_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_200_1], axis=1)*snaps['scalefactor'][snap_200_1] < halt['star.radius.90'][host_200_1])
    v_mask_gas_host_200_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < halt.prop('star.vel.std.50', host_200_1))
    v_mask_gas_host_200_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < 2*halt.prop('star.vel.std.50', host_200_1))
    v_mask_gas_host_200_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_1] - halt.prop('velocity', host_200_1), axis=1) < 3*halt.prop('star.vel.std.50', host_200_1))
    d_1['200.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_1_1]), np.sum(part['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_2_1]), np.sum(part['gas']['mass'][mask_gas_host_200_1][v_mask_gas_host_200_3_1])])
    #
    ### 300 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_300_1, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_300_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][halo_300_1])
    # Stars
    mask_star_300_1 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][halo_300_1])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 300 Myr
    v_mask_gas_300_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('star.vel.std.50', halo_300_1))
    v_mask_gas_300_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_1))
    v_mask_gas_300_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_1))
    # star
    # 300 Myr
    v_mask_star_300_1_1 = (np.linalg.norm(part['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < halt.prop('star.vel.std.50', halo_300_1))
    v_mask_star_300_2_1 = (np.linalg.norm(part['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_1))
    v_mask_star_300_3_1 = (np.linalg.norm(part['star']['velocity'][mask_star_300_1] - halt.prop('velocity', halo_300_1), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_1))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    # Gas
    d_1['300.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_300_1][v_mask_gas_300_1_1]), np.sum(part['gas']['mass'][mask_gas_300_1][v_mask_gas_300_2_1]), np.sum(part['gas']['mass'][mask_gas_300_1][v_mask_gas_300_3_1])])
    # Star
    d_1['300.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_300_1][v_mask_star_300_1_1]), np.sum(part['star']['mass'][mask_star_300_1][v_mask_star_300_2_1]), np.sum(part['star']['mass'][mask_star_300_1][v_mask_star_300_3_1])])
    #
    # Do the same for the host
    host_300_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_300_1]
    mask_gas_host_300_1 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_300_1], axis=1)*snaps['scalefactor'][snap_300_1] < halt['star.radius.90'][host_300_1])
    v_mask_gas_host_300_1_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < halt.prop('star.vel.std.50', host_300_1))
    v_mask_gas_host_300_2_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < 2*halt.prop('star.vel.std.50', host_300_1))
    v_mask_gas_host_300_3_1 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_1] - halt.prop('velocity', host_300_1), axis=1) < 3*halt.prop('star.vel.std.50', host_300_1))
    d_1['300.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_1_1]), np.sum(part['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_2_1]), np.sum(part['gas']['mass'][mask_gas_host_300_1][v_mask_gas_host_300_3_1])])
    #
    d_1['snap.merger'] = halo_snap_1
    d_1['snap.100'] = snap_100_1
    d_1['snap.200'] = snap_200_1
    d_1['snap.300'] = snap_300_1
    #
    # Save data to a dictionary
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_gas_star_merger_mass', dict_or_array_to_write=d_1, verbose=True)
    #
    #
    #### Galaxy 2
    # Read in the metal-poor data
    data_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    halo_ses_pro_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_prograde_data')
    #
    # Find the halo ID & snapshot of the most major prograde merger
    halo_snap_2 = halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    halo_id_2 = halo_ses_pro_2['unique.halo'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    halo_mass_2 = halo_ses_pro_2['star.mass.peak'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]
    #
    # Find the snapshots that are 100, 200, and 300 Myr before the merger
    snap_100_2 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_2]-0.1)][-1]
    snap_200_2 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_2]-0.2)][-1]
    snap_300_2 = snaps['index'][snaps['time'] < (snaps['time'][halo_snap_2]-0.3)][-1]
    #
    time_merger_2 = snaps['time'][halo_snap_2]
    time_100_2 = snaps['time'][snap_100_2]
    time_200_2 = snaps['time'][snap_200_2]
    time_300_2 = snaps['time'][snap_300_2]
    #
    # Find the halo indices 100, 200, and 300 Myr before the merger
    halo_100_2 = halt.prop('progenitor.main.indices', halo_id_2)[halo_snap_2 - snap_100_2]
    halo_200_2 = halt.prop('progenitor.main.indices', halo_id_2)[halo_snap_2 - snap_200_2]
    halo_300_2 = halt.prop('progenitor.main.indices', halo_id_2)[halo_snap_2 - snap_300_2]
    #
    ### 100 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_100_2, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_100_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_100_2], axis=1)*snaps['scalefactor'][snap_100_2] < halt['star.radius.90'][halo_100_2])
    # Stars
    mask_star_100_2 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_100_2], axis=1)*snaps['scalefactor'][snap_100_2] < halt['star.radius.90'][halo_100_2])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 100 Myr
    v_mask_gas_100_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_2] - halt.prop('velocity', halo_100_2), axis=1) < halt.prop('star.vel.std.50', halo_100_2))
    v_mask_gas_100_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_2] - halt.prop('velocity', halo_100_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_2))
    v_mask_gas_100_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_100_2] - halt.prop('velocity', halo_100_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_2))
    # star
    # 100 Myr
    v_mask_star_100_1_2 = (np.linalg.norm(part['star']['velocity'][mask_star_100_2] - halt.prop('velocity', halo_100_2), axis=1) < halt.prop('star.vel.std.50', halo_100_2))
    v_mask_star_100_2_2 = (np.linalg.norm(part['star']['velocity'][mask_star_100_2] - halt.prop('velocity', halo_100_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_100_2))
    v_mask_star_100_3_2 = (np.linalg.norm(part['star']['velocity'][mask_star_100_2] - halt.prop('velocity', halo_100_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_100_2))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    d_2 = dict()
    d_2['halo.dist'] = np.array([halt.prop('host2.distance.total', halo_100_2), halt.prop('host2.distance.total', halo_200_2), halt.prop('host2.distance.total', halo_300_2)])
    # Gas
    d_2['100.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_100_2][v_mask_gas_100_1_2]), np.sum(part['gas']['mass'][mask_gas_100_2][v_mask_gas_100_2_2]), np.sum(part['gas']['mass'][mask_gas_100_2][v_mask_gas_100_3_2])])
    # Star
    d_2['100.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_100_2][v_mask_star_100_1_2]), np.sum(part['star']['mass'][mask_star_100_2][v_mask_star_100_2_2]), np.sum(part['star']['mass'][mask_star_100_2][v_mask_star_100_3_2])])
    #
    # Do the same for the host
    host_100_2 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_100_2]
    mask_gas_host_100_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_100_2], axis=1)*snaps['scalefactor'][snap_100_2] < halt['star.radius.90'][host_100_2])
    v_mask_gas_host_100_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_2] - halt.prop('velocity', host_100_2), axis=1) < halt.prop('star.vel.std.50', host_100_2))
    v_mask_gas_host_100_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_2] - halt.prop('velocity', host_100_2), axis=1) < 2*halt.prop('star.vel.std.50', host_100_2))
    v_mask_gas_host_100_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_100_2] - halt.prop('velocity', host_100_2), axis=1) < 3*halt.prop('star.vel.std.50', host_100_2))
    d_2['100.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_100_2][v_mask_gas_host_100_1_2]), np.sum(part['gas']['mass'][mask_gas_host_100_2][v_mask_gas_host_100_2_2]), np.sum(part['gas']['mass'][mask_gas_host_100_2][v_mask_gas_host_100_3_2])])
    #
    ### 200 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_200_2, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_200_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_200_2], axis=1)*snaps['scalefactor'][snap_200_2] < halt['star.radius.90'][halo_200_2])
    # Stars
    mask_star_200_2 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_200_2], axis=1)*snaps['scalefactor'][snap_200_2] < halt['star.radius.90'][halo_200_2])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 200 Myr
    v_mask_gas_200_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_2] - halt.prop('velocity', halo_200_2), axis=1) < halt.prop('star.vel.std.50', halo_200_2))
    v_mask_gas_200_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_2] - halt.prop('velocity', halo_200_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_2))
    v_mask_gas_200_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_200_2] - halt.prop('velocity', halo_200_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_2))
    # star
    # 200 Myr
    v_mask_star_200_1_2 = (np.linalg.norm(part['star']['velocity'][mask_star_200_2] - halt.prop('velocity', halo_200_2), axis=1) < halt.prop('star.vel.std.50', halo_200_2))
    v_mask_star_200_2_2 = (np.linalg.norm(part['star']['velocity'][mask_star_200_2] - halt.prop('velocity', halo_200_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_200_2))
    v_mask_star_200_3_2 = (np.linalg.norm(part['star']['velocity'][mask_star_200_2] - halt.prop('velocity', halo_200_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_200_2))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    # Gas
    d_2['200.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_200_2][v_mask_gas_200_1_2]), np.sum(part['gas']['mass'][mask_gas_200_2][v_mask_gas_200_2_2]), np.sum(part['gas']['mass'][mask_gas_200_2][v_mask_gas_200_3_2])])
    # Star
    d_2['200.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_200_2][v_mask_star_200_1_2]), np.sum(part['star']['mass'][mask_star_200_2][v_mask_star_200_2_2]), np.sum(part['star']['mass'][mask_star_200_2][v_mask_star_200_3_2])])
    #
    # Do the same for the host
    host_200_2 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_200_2]
    mask_gas_host_200_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_200_2], axis=1)*snaps['scalefactor'][snap_200_2] < halt['star.radius.90'][host_200_2])
    v_mask_gas_host_200_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_2] - halt.prop('velocity', host_200_2), axis=1) < halt.prop('star.vel.std.50', host_200_2))
    v_mask_gas_host_200_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_2] - halt.prop('velocity', host_200_2), axis=1) < 2*halt.prop('star.vel.std.50', host_200_2))
    v_mask_gas_host_200_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_200_2] - halt.prop('velocity', host_200_2), axis=1) < 3*halt.prop('star.vel.std.50', host_200_2))
    d_2['200.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_200_2][v_mask_gas_host_200_1_2]), np.sum(part['gas']['mass'][mask_gas_host_200_2][v_mask_gas_host_200_2_2]), np.sum(part['gas']['mass'][mask_gas_host_200_2][v_mask_gas_host_200_3_2])])
    #
    ### 300 Myr
    # Read in the particle data 100, 200, and 300 Myr before the merger
    part = gizmo.io.Read.read_snapshots(['gas','star'], 'snapshot', snap_300_2, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
    #
    # Create a mask for gas particles that are within R90 of the halo
    # Have to multiply the positions by the scalefactor because positions are co-moving and radii are physical distances
    # Gas
    mask_gas_300_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][halo_300_2], axis=1)*snaps['scalefactor'][snap_300_2] < halt['star.radius.90'][halo_300_2])
    # Stars
    mask_star_300_2 = (np.linalg.norm(part['star']['position'] - halt['position'][halo_300_2], axis=1)*snaps['scalefactor'][snap_300_2] < halt['star.radius.90'][halo_300_2])
    #
    # Calculate the total velocities of the gas particles and create masks for particles based on various dispersions
    # Gas
    # 300 Myr
    v_mask_gas_300_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_2] - halt.prop('velocity', halo_300_2), axis=1) < halt.prop('star.vel.std.50', halo_300_2))
    v_mask_gas_300_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_2] - halt.prop('velocity', halo_300_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_2))
    v_mask_gas_300_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_300_2] - halt.prop('velocity', halo_300_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_2))
    # star
    # 300 Myr
    v_mask_star_300_1_2 = (np.linalg.norm(part['star']['velocity'][mask_star_300_2] - halt.prop('velocity', halo_300_2), axis=1) < halt.prop('star.vel.std.50', halo_300_2))
    v_mask_star_300_2_2 = (np.linalg.norm(part['star']['velocity'][mask_star_300_2] - halt.prop('velocity', halo_300_2), axis=1) < 2*halt.prop('star.vel.std.50', halo_300_2))
    v_mask_star_300_3_2 = (np.linalg.norm(part['star']['velocity'][mask_star_300_2] - halt.prop('velocity', halo_300_2), axis=1) < 3*halt.prop('star.vel.std.50', halo_300_2))
    #
    # Add up all of the gas that is within the halo, and within the velocity criteria. Save to dictionary.
    # Gas
    d_2['300.Myr.gas.mass'] = np.array([np.sum(part['gas']['mass'][mask_gas_300_2][v_mask_gas_300_1_2]), np.sum(part['gas']['mass'][mask_gas_300_2][v_mask_gas_300_2_2]), np.sum(part['gas']['mass'][mask_gas_300_2][v_mask_gas_300_3_2])])
    # Star
    d_2['300.Myr.star.mass'] = np.array([np.sum(part['star']['mass'][mask_star_300_2][v_mask_star_300_1_2]), np.sum(part['star']['mass'][mask_star_300_2][v_mask_star_300_2_2]), np.sum(part['star']['mass'][mask_star_300_2][v_mask_star_300_3_2])])
    #
    # Do the same for the host
    host_300_2 = halt.prop('progenitor.main.indices', halt['host.index'][0])[600-snap_300_2]
    mask_gas_host_300_2 = (np.linalg.norm(part['gas']['position'] - halt['position'][host_300_2], axis=1)*snaps['scalefactor'][snap_300_2] < halt['star.radius.90'][host_300_2])
    v_mask_gas_host_300_1_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_2] - halt.prop('velocity', host_300_2), axis=1) < halt.prop('star.vel.std.50', host_300_2))
    v_mask_gas_host_300_2_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_2] - halt.prop('velocity', host_300_2), axis=1) < 2*halt.prop('star.vel.std.50', host_300_2))
    v_mask_gas_host_300_3_2 = (np.linalg.norm(part['gas']['velocity'][mask_gas_host_300_2] - halt.prop('velocity', host_300_2), axis=1) < 3*halt.prop('star.vel.std.50', host_300_2))
    d_2['300.Myr.gas.mass.host'] = np.array([np.sum(part['gas']['mass'][mask_gas_host_300_2][v_mask_gas_host_300_1_2]), np.sum(part['gas']['mass'][mask_gas_host_300_2][v_mask_gas_host_300_2_2]), np.sum(part['gas']['mass'][mask_gas_host_300_2][v_mask_gas_host_300_3_2])])
    #
    d_2['snap.merger'] = halo_snap_2
    d_2['snap.100'] = snap_100_2
    d_2['snap.200'] = snap_200_2
    d_2['snap.300'] = snap_300_2
    #
    # Save the dictionary to a file
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_gas_star_merger_mass', dict_or_array_to_write=d_2, verbose=True)
    ############################
    ## Plot the data
    # Create a plot showing the gas masses at the different times for the different velocity selections
    times_1 = np.array([time_100_1, time_200_1, time_300_1])
    times_2 = np.array([time_100_2, time_200_2, time_300_2])
    #
    # Galaxy 1
    fig = plt.figure(figsize=(10, 8))
    #
    plt.vlines(x=time_merger_1, ymin=0, ymax=1e12, color='k', linestyle=':')
    #
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7, label='1 $\\sigma$')
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7, label='2 $\\sigma$')
    plt.scatter(times_1[0], d_1['100.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7, label='3 $\\sigma$')
    #
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[0], d_1['100.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[1], d_1['200.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_1[2], d_1['300.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(time_merger_1, halo_mass_1, c='k', marker='*', s=95, alpha=0.7)
    plt.scatter(time_100_1, halt['star.mass'][halo_100_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_200_1, halt['star.mass'][halo_200_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_300_1, halt['star.mass'][halo_300_1], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    #
    # Label stuff
    plt.xlabel('time [Gyr]', fontsize=20)
    plt.ylabel('Mass within R$_{\\rm 90}$', fontsize=20)
    plt.title(gal1+' using star.vel.std.50', fontsize=24)
    plt.legend(prop={'size': 18}, loc='best')
    plt.tick_params(axis='x', which='major', labelsize=20)
    plt.tick_params(axis='y', which='major', labelsize=20)
    plt.xlim(time_merger_1+0.1, times_1[2]-0.1)
    plt.ylim(1e8, 1e11)
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal1+'_merger_gas_star_masses.pdf')
    plt.close()
    #
    # Galaxy 2
    fig = plt.figure(figsize=(10, 8))
    #
    plt.vlines(x=time_merger_2, ymin=0, ymax=1e12, color='k', linestyle=':')
    #
    plt.scatter(times_2[0], d_2['100.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7, label='1 $\\sigma$')
    plt.scatter(times_2[0], d_2['100.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7, label='2 $\\sigma$')
    plt.scatter(times_2[0], d_2['100.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7, label='3 $\\sigma$')
    #
    plt.scatter(times_2[1], d_2['200.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_2[1], d_2['200.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_2[1], d_2['200.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_2[2], d_2['300.Myr.gas.mass'][0], c=colors[0], marker='o', s=85, alpha=0.7)
    plt.scatter(times_2[2], d_2['300.Myr.gas.mass'][1], c=colors[1], marker='o', s=85, alpha=0.7)
    plt.scatter(times_2[2], d_2['300.Myr.gas.mass'][2], c=colors[3], marker='o', s=85, alpha=0.7)
    #
    plt.scatter(times_2[0], d_2['100.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[0], d_2['100.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[0], d_2['100.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_2[1], d_2['200.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[1], d_2['200.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[1], d_2['200.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(times_2[2], d_2['300.Myr.star.mass'][0], c=colors[0], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[2], d_2['300.Myr.star.mass'][1], c=colors[1], marker='*', s=95, alpha=0.7)
    plt.scatter(times_2[2], d_2['300.Myr.star.mass'][2], c=colors[3], marker='*', s=95, alpha=0.7)
    #
    plt.scatter(time_merger_2, halo_mass_2, c='k', marker='*', s=95, alpha=0.7)
    plt.scatter(time_100_2, halt['star.mass'][halo_100_2], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_200_2, halt['star.mass'][halo_200_2], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    plt.scatter(time_300_2, halt['star.mass'][halo_300_2], c='k', marker='*', s=95, alpha=0.7, facecolors='none')
    #
    # Label stuff
    plt.xlabel('time [Gyr]', fontsize=20)
    plt.ylabel('Mass within R$_{\\rm 90}$', fontsize=20)
    plt.title(gal2+' using star.vel.std.50', fontsize=24)
    plt.legend(prop={'size': 18}, loc='best')
    plt.tick_params(axis='x', which='major', labelsize=20)
    plt.tick_params(axis='y', which='major', labelsize=20)
    plt.xlim(time_merger_2+0.1, times_2[2]-0.1)
    plt.ylim(1e8, 1e10)
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal2+'_merger_gas_star_masses.pdf')
    plt.close()
