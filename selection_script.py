#!/usr/bin/python3

"""
 ========================
 = Metal-poor Star Data =
 ========================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2019

 GOAL: Select stars that are in the spatial range:
            - R = [4, 12] kpc
            - |Z| < 3 kpc
       Select stars with iron abundance of:
            - [Fe/H] < -2.5

       Save the following properties
            - Indices at z = 0
            - Masses
            - 3D positions
            - 3D velocities
            - Radial distance from the host
            - [Fe/H] abundances
            - Ages
            - formation distance
            - rotational velocity
"""

## Import all of the tools for analysis
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
print('Read in the tools')

### Read in the halo information
gal1 = 'Romulus'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir = '/home1/05400/ibsantis/scripts'
print('Set paths')

# Read in the star data at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, simulation_directory=simulation_dir, assign_hosts_rotation=True, assign_formation_coordinates=True)
print('Stars at z = 0 read in')

#### Analysis
## GALAXY 1
if num_gal == 1:
    inds = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds)
    metal_inds = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5], inds)
    print('Indices found!')

    # Create arrays of desired properties
    masses = part['star']['mass'][metal_inds]
    pos_3d = part['star'].prop('star.host1.distance.principal', metal_inds) # X, Y, Z Positions
    vel_3d = part['star'].prop('star.host1.velocity.principal', metal_inds) # X, Y, Z Velocities
    pos_3d_cyl = part['star'].prop('star.host1.distance.principal.cylindrical', metal_inds) # R, Phi, Z Positions
    vel_3d_cyl = part['star'].prop('star.host1.velocity.principal.cylindrical', metal_inds) # R, Phi, Z Velocities
    pos_1d = part['star'].prop('star.host1.distance.total', metal_inds)
    fe_abund = part['star'].prop('metallicity.iron', metal_inds)
    ages = part['star'].prop('age', metal_inds)
    formation = part['star'].prop('form.host1.distance.total', metal_inds)
    #
    # Find the rotational velocity
    vrot = np.median(part['star'].prop('star.host1.velocity.principal.cylindrical', inds)[:,1])
    print('Data grouped together')

    # Put the data into a dictionary
    data1 = dict()
    data1['indices'] = metal_inds
    data1['masses'] = masses
    data1['ages'] = ages
    data1['fe.abund'] = fe_abund
    data1['pos.3d.car'] = pos_3d
    data1['vel.3d.car'] = vel_3d
    data1['pos.3d.cyl'] = pos_3d_cyl
    data1['vel.3d.cyl'] = vel_3d_cyl
    data1['pos.1d'] = pos_1d
    data1['form.dist'] = formation
    data1['vrot.z0'] = vrot

    # Save this data to a file so that I can use it for analysis and plotting
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data', dict_or_array_to_write=data1, verbose=True)
    print('Galaxy 1 data saved!')


if num_gal == 2:
    inds = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,0], [4,12])
    inds = ut.array.get_indices(part['star'].prop('star.host1.distance.principal.cylindrical')[:,2], [-3,3], inds)
    metal_inds = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5], inds)
    print('Indices found!')

    # Create arrays of desired properties
    masses = part['star']['mass'][metal_inds]
    pos_3d = part['star'].prop('star.host1.distance.principal', metal_inds) # X, Y, Z Positions
    vel_3d = part['star'].prop('star.host1.velocity.principal', metal_inds) # X, Y, Z Velocities
    pos_3d_cyl = part['star'].prop('star.host1.distance.principal.cylindrical', metal_inds) # R, Phi, Z Positions
    vel_3d_cyl = part['star'].prop('star.host1.velocity.principal.cylindrical', metal_inds) # R, Phi, Z Velocities
    pos_1d = part['star'].prop('star.host1.distance.total', metal_inds)
    fe_abund = part['star'].prop('metallicity.iron', metal_inds)
    ages = part['star'].prop('age', metal_inds)
    formation = part['star'].prop('form.host1.distance.total', metal_inds)
    #
    # Find the rotational velocity
    vrot = np.median(part['star'].prop('star.host1.velocity.principal.cylindrical', inds)[:,1])
    print('Data grouped together')

    # Put the data into a dictionary
    data1 = dict()
    data1['indices'] = metal_inds
    data1['masses'] = masses
    data1['ages'] = ages
    data1['fe.abund'] = fe_abund
    data1['pos.3d.car'] = pos_3d
    data1['vel.3d.car'] = vel_3d
    data1['pos.3d.cyl'] = pos_3d_cyl
    data1['vel.3d.cyl'] = vel_3d_cyl
    data1['pos.1d'] = pos_1d
    data1['form.dist'] = formation
    data1['vrot.z0'] = vrot

    # Save this data to a file so that I can use it for analysis and plotting
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data', dict_or_array_to_write=data1, verbose=True)
    print('Galaxy 1 data saved!')

    ## GALAXY 2
    inds2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,0], [4,12])
    inds2 = ut.array.get_indices(part['star'].prop('star.host2.distance.principal.cylindrical')[:,2], [-3,3], inds2)
    metal_inds2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [-np.inf, -2.5], inds2)
    print('Indices found!')

    # Create arrays of desired properties
    masses2 = part['star']['mass'][metal_inds2]
    pos_3d2 = part['star'].prop('star.host2.distance.principal', metal_inds2) # X, Y, Z Positions
    vel_3d2 = part['star'].prop('star.host2.velocity.principal', metal_inds2) # X, Y, Z Velocities
    pos_3d_cyl2 = part['star'].prop('star.host2.distance.principal.cylindrical', metal_inds2) # R, Phi, Z Positions
    vel_3d_cyl2 = part['star'].prop('star.host2.velocity.principal.cylindrical', metal_inds2) # R, Phi, Z Velocities
    pos_1d2 = part['star'].prop('star.host2.distance.total', metal_inds2)
    fe_abund2 = part['star'].prop('metallicity.iron', metal_inds2)
    ages2 = part['star'].prop('age', metal_inds2)
    formation2 = part['star'].prop('form.host2.distance.total', metal_inds2)
    #
    # Find the rotational velocity
    vrot2 = np.median(part['star'].prop('star.host2.velocity.principal.cylindrical', inds2)[:,1])
    print('Data grouped together')

    # Put the data into a dictionary
    data2 = dict()
    data2['indices'] = metal_inds2
    data2['masses'] = masses2
    data2['ages'] = ages2
    data2['fe.abund'] = fe_abund2
    data2['pos.3d.car'] = pos_3d2
    data2['vel.3d.car'] = vel_3d2
    data2['pos.3d.cyl'] = pos_3d_cyl2
    data2['vel.3d.cyl'] = vel_3d_cyl2
    data2['pos.1d'] = pos_1d2
    data2['form.dist'] = formation2
    data2['vrot.z0'] = vrot2

    # Save this data to a file so that I can use it for analysis and plotting
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data', dict_or_array_to_write=data2, verbose=True)
    print('Galaxy 2 data saved!')

print('All done :)')
