#!/usr/bin/python3

"""
 ====================
 = Age vs Asymmetry =
 ====================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot Stellar Age vs asymmetry for all hosts

    - This is done with no selection on [Fe/H]

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
#
loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    plt.rcParams["font.family"] = "serif"
print('Set paths')

## Read in the data
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_age')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_age')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_age')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_age')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_age')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_age')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_age')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_age')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_age')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_age')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_age')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_age')
#
asym_m12b_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_age_kareem')
asym_m12c_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_age_kareem')
asym_m12f_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_age_kareem')
asym_m12i_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_age_kareem')
asym_m12m_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_age_kareem')
asym_m12w_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_age_kareem')
asym_Romeo_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_age_kareem')
asym_Juliet_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_age_kareem')
asym_Thelma_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_age_kareem')
asym_Louise_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_age_kareem')
asym_Romulus_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_age_kareem')
asym_Remus_kareem = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_age_kareem')
#
asym_m12b_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_age_no_fe')
asym_m12c_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_age_no_fe')
asym_m12f_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_age_no_fe')
asym_m12i_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_age_no_fe')
asym_m12m_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_age_no_fe')
asym_m12w_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_age_no_fe')
asym_Romeo_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_age_no_fe')
asym_Juliet_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_age_no_fe')
asym_Thelma_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_age_no_fe')
asym_Louise_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_age_no_fe')
asym_Romulus_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_age_no_fe')
asym_Remus_no_fe = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_age_no_fe')
#
asym_m12b_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_age_no_fe_25')
asym_m12c_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_age_no_fe_25')
asym_m12f_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_age_no_fe_25')
asym_m12i_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_age_no_fe_25')
asym_m12m_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_age_no_fe_25')
asym_m12w_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_age_no_fe_25')
asym_Romeo_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_age_no_fe_25')
asym_Juliet_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_age_no_fe_25')
asym_Thelma_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_age_no_fe_25')
asym_Louise_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_age_no_fe_25')
asym_Romulus_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_age_no_fe_25')
asym_Remus_no_fe_25 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_age_no_fe_25')
#
asym_m12b_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_age_no_fe_333')
asym_m12c_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_age_no_fe_333')
asym_m12f_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_age_no_fe_333')
asym_m12i_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_age_no_fe_333')
asym_m12m_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_age_no_fe_333')
asym_m12w_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_age_no_fe_333')
asym_Romeo_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_age_no_fe_333')
asym_Juliet_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_age_no_fe_333')
asym_Thelma_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_age_no_fe_333')
asym_Louise_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_age_no_fe_333')
asym_Romulus_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_age_no_fe_333')
asym_Remus_no_fe_333 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_age_no_fe_333')
#

# Differential plot
# Stack the data
asym_all = np.vstack((asym_m12b['asymmetry.age'],asym_m12c['asymmetry.age'],asym_m12f['asymmetry.age'],asym_m12i['asymmetry.age'],asym_m12m['asymmetry.age'],asym_m12w['asymmetry.age'],asym_Romeo['asymmetry.age'],asym_Juliet['asymmetry.age'],asym_Thelma['asymmetry.age'],asym_Louise['asymmetry.age'],asym_Romulus['asymmetry.age'],asym_Remus['asymmetry.age']))
asym_med = np.nanmedian(asym_all, axis=0)
ages = np.array([5., 5.5, 6., 6.5, 7., 7.5, 8., 8.5, 9., 9.5, 10., 10.5, 11., 11.5, 12., 12.5, 13., 13.5])
ages = ages+0.25
#
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(ages, asym_m12b['asymmetry.age'], color=colors[0], label='m12b', alpha=0.5)
ax.plot(ages, asym_m12c['asymmetry.age'], color=colors[1], label='m12c', alpha=0.5)
ax.plot(ages, asym_m12f['asymmetry.age'], color=colors[6], label='m12f', alpha=0.5)
ax.plot(ages, asym_m12i['asymmetry.age'], color=colors[3], label='m12i', alpha=0.5)
ax.plot(ages, asym_m12m['asymmetry.age'], color=colors[4], label='m12m', alpha=0.5)
ax.plot(ages, asym_m12w['asymmetry.age'], color=colors[5], label='m12w', alpha=0.5)
ax.plot(ages, asym_Romeo['asymmetry.age'], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Juliet['asymmetry.age'], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Thelma['asymmetry.age'], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Louise['asymmetry.age'], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Romulus['asymmetry.age'], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Remus['asymmetry.age'], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(ages, asym_med, color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(ages, asym_med, linestyle='', label=' ')
ax.plot(ages, asym_med, linestyle='', label=' ')
ax.plot(ages, asym_med, linestyle='', label=' ')
ax.plot(ages, asym_med, linestyle='', label=' ')
ax.plot(ages, asym_med, linestyle='', label=' ')
#
plt.xticks(np.arange(9, 14.1, 1.))
#plt.yticks(np.arange(1, 11, 9))
plt.xlim(9.5, 14)
#plt.ylim(ymin=1, ymax=30)
plt.title('[Fe/H] < -2.5', fontsize=32)
plt.yscale('log')
plt.xlabel('Age [Gyr]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/age_vs_asymmetry_all.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_kareem = np.vstack((asym_m12b_kareem['asymmetry.age'],asym_m12c_kareem['asymmetry.age'],asym_m12f_kareem['asymmetry.age'],asym_m12i_kareem['asymmetry.age'],asym_m12m_kareem['asymmetry.age'],asym_m12w_kareem['asymmetry.age'],asym_Romeo_kareem['asymmetry.age'],asym_Juliet_kareem['asymmetry.age'],asym_Thelma_kareem['asymmetry.age'],asym_Louise_kareem['asymmetry.age'],asym_Romulus_kareem['asymmetry.age'],asym_Remus_kareem['asymmetry.age']))
asym_med_kareem = np.nanmedian(asym_all_kareem, axis=0)
ages = np.array([5., 5.5, 6., 6.5, 7., 7.5, 8., 8.5, 9., 9.5, 10., 10.5, 11., 11.5, 12., 12.5, 13., 13.5])
ages = ages+0.25
#
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(ages, asym_m12b_kareem['asymmetry.age'], color=colors[0], label='m12b', alpha=0.5)
ax.plot(ages, asym_m12c_kareem['asymmetry.age'], color=colors[1], label='m12c', alpha=0.5)
ax.plot(ages, asym_m12f_kareem['asymmetry.age'], color=colors[6], label='m12f', alpha=0.5)
ax.plot(ages, asym_m12i_kareem['asymmetry.age'], color=colors[3], label='m12i', alpha=0.5)
ax.plot(ages, asym_m12m_kareem['asymmetry.age'], color=colors[4], label='m12m', alpha=0.5)
ax.plot(ages, asym_m12w_kareem['asymmetry.age'], color=colors[5], label='m12w', alpha=0.5)
ax.plot(ages, asym_Romeo_kareem['asymmetry.age'], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Juliet_kareem['asymmetry.age'], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Thelma_kareem['asymmetry.age'], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Louise_kareem['asymmetry.age'], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Romulus_kareem['asymmetry.age'], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Remus_kareem['asymmetry.age'], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(ages, asym_med_kareem, color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(ages, asym_med_kareem, linestyle='', label=' ')
ax.plot(ages, asym_med_kareem, linestyle='', label=' ')
ax.plot(ages, asym_med_kareem, linestyle='', label=' ')
ax.plot(ages, asym_med_kareem, linestyle='', label=' ')
ax.plot(ages, asym_med_kareem, linestyle='', label=' ')
#
plt.xticks(np.arange(9, 14.1, 1.))
#plt.yticks(np.arange(1, 11, 9))
plt.xlim(8.5, 14)
#plt.ylim(ymin=1, ymax=30)
plt.title('[Fe/H] < -2', fontsize=32)
plt.yscale('log')
plt.xlabel('Age [Gyr]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/age_vs_asymmetry_all_kareem.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_no_fe = np.vstack((asym_m12b_no_fe['asymmetry.age'],asym_m12c_no_fe['asymmetry.age'],asym_m12f_no_fe['asymmetry.age'],asym_m12i_no_fe['asymmetry.age'],asym_m12m_no_fe['asymmetry.age'],asym_m12w_no_fe['asymmetry.age'],asym_Romeo_no_fe['asymmetry.age'],asym_Juliet_no_fe['asymmetry.age'],asym_Thelma_no_fe['asymmetry.age'],asym_Louise_no_fe['asymmetry.age'],asym_Romulus_no_fe['asymmetry.age'],asym_Remus_no_fe['asymmetry.age']))
asym_med_no_fe = np.nanmedian(asym_all_no_fe, axis=0)
ages = np.array([5., 5.5, 6., 6.5, 7., 7.5, 8., 8.5, 9., 9.5, 10., 10.5, 11., 11.5, 12., 12.5, 13., 13.5])
ages = ages+0.25
#
plt.figure(3)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(ages, asym_m12b_no_fe['asymmetry.age'], color=colors[0], label='m12b', alpha=0.5)
ax.plot(ages, asym_m12c_no_fe['asymmetry.age'], color=colors[1], label='m12c', alpha=0.5)
ax.plot(ages, asym_m12f_no_fe['asymmetry.age'], color=colors[6], label='m12f', alpha=0.5)
ax.plot(ages, asym_m12i_no_fe['asymmetry.age'], color=colors[3], label='m12i', alpha=0.5)
ax.plot(ages, asym_m12m_no_fe['asymmetry.age'], color=colors[4], label='m12m', alpha=0.5)
ax.plot(ages, asym_m12w_no_fe['asymmetry.age'], color=colors[5], label='m12w', alpha=0.5)
ax.plot(ages, asym_Romeo_no_fe['asymmetry.age'], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Juliet_no_fe['asymmetry.age'], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Thelma_no_fe['asymmetry.age'], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Louise_no_fe['asymmetry.age'], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Romulus_no_fe['asymmetry.age'], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Remus_no_fe['asymmetry.age'], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(ages, asym_med_no_fe, color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(ages, asym_med_no_fe, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe, linestyle='', label=' ')
#
plt.xticks(np.arange(5, 14.1, 1.))
#plt.yticks(np.arange(1, 11, 9))
plt.xlim(10, 14)
plt.ylim(ymin=1, ymax=30)
plt.title('No metallicity cut', fontsize=32)
plt.yscale('log')
plt.xlabel('Age [Gyr]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/age_vs_asymmetry_all_no_fe.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_no_fe_25 = np.vstack((asym_m12b_no_fe_25['asymmetry.age'],asym_m12c_no_fe_25['asymmetry.age'],asym_m12f_no_fe_25['asymmetry.age'],asym_m12i_no_fe_25['asymmetry.age'],asym_m12m_no_fe_25['asymmetry.age'],asym_m12w_no_fe_25['asymmetry.age'],asym_Romeo_no_fe_25['asymmetry.age'],asym_Juliet_no_fe_25['asymmetry.age'],asym_Thelma_no_fe_25['asymmetry.age'],asym_Louise_no_fe_25['asymmetry.age'],asym_Romulus_no_fe_25['asymmetry.age'],asym_Remus_no_fe_25['asymmetry.age']))
asym_med_no_fe_25 = np.nanmedian(asym_all_no_fe_25, axis=0)
ages = np.arange(5, 13.8, 0.25)
ages = ages+0.125
#
plt.figure(4)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(ages, asym_m12b_no_fe_25['asymmetry.age'], color=colors[0], label='m12b', alpha=0.5)
ax.plot(ages, asym_m12c_no_fe_25['asymmetry.age'], color=colors[1], label='m12c', alpha=0.5)
ax.plot(ages, asym_m12f_no_fe_25['asymmetry.age'], color=colors[6], label='m12f', alpha=0.5)
ax.plot(ages, asym_m12i_no_fe_25['asymmetry.age'], color=colors[3], label='m12i', alpha=0.5)
ax.plot(ages, asym_m12m_no_fe_25['asymmetry.age'], color=colors[4], label='m12m', alpha=0.5)
ax.plot(ages, asym_m12w_no_fe_25['asymmetry.age'], color=colors[5], label='m12w', alpha=0.5)
ax.plot(ages, asym_Romeo_no_fe_25['asymmetry.age'], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Juliet_no_fe_25['asymmetry.age'], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Thelma_no_fe_25['asymmetry.age'], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Louise_no_fe_25['asymmetry.age'], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Romulus_no_fe_25['asymmetry.age'], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(ages, asym_Remus_no_fe_25['asymmetry.age'], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(ages, asym_med_no_fe_25, color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(ages, asym_med_no_fe_25, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe_25, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe_25, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe_25, linestyle='', label=' ')
ax.plot(ages, asym_med_no_fe_25, linestyle='', label=' ')
#
plt.xticks(np.arange(5, 14.1, 1.))
#plt.yticks(np.arange(1, 11, 9))
plt.xlim(10, 14)
plt.ylim(ymin=1, ymax=30)
plt.title('No metallicity cut, 0.25 Gyr spacing', fontsize=28)
plt.yscale('log')
plt.xlabel('Age [Gyr]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/age_vs_asymmetry_all_no_fe_25.pdf')
plt.close()


# Differential plot
# Stack the data
asym_all_no_fe_333 = np.vstack((asym_m12b_no_fe_333['asymmetry.age'],asym_m12c_no_fe_333['asymmetry.age'],asym_m12f_no_fe_333['asymmetry.age'],asym_m12i_no_fe_333['asymmetry.age'],asym_m12m_no_fe_333['asymmetry.age'],asym_m12w_no_fe_333['asymmetry.age'],asym_Romeo_no_fe_333['asymmetry.age'],asym_Juliet_no_fe_333['asymmetry.age'],asym_Thelma_no_fe_333['asymmetry.age'],asym_Louise_no_fe_333['asymmetry.age'],asym_Romulus_no_fe_333['asymmetry.age'],asym_Remus_no_fe_333['asymmetry.age']))
asym_med_no_fe_333 = np.nanmedian(asym_all_no_fe_333, axis=0)
ages = np.arange(5, 14.1, 0.333)
ages = ages+0.1665
#
plt.figure(5)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(ages[:-1], asym_m12b_no_fe_333['asymmetry.age'], color=colors[0], label='m12b', alpha=0.5)
ax.plot(ages[:-1], asym_m12c_no_fe_333['asymmetry.age'], color=colors[1], label='m12c', alpha=0.5)
ax.plot(ages[:-1], asym_m12f_no_fe_333['asymmetry.age'], color=colors[6], label='m12f', alpha=0.5)
ax.plot(ages[:-1], asym_m12i_no_fe_333['asymmetry.age'], color=colors[3], label='m12i', alpha=0.5)
ax.plot(ages[:-1], asym_m12m_no_fe_333['asymmetry.age'], color=colors[4], label='m12m', alpha=0.5)
ax.plot(ages[:-1], asym_m12w_no_fe_333['asymmetry.age'], color=colors[5], label='m12w', alpha=0.5)
ax.plot(ages[:-1], asym_Romeo_no_fe_333['asymmetry.age'], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(ages[:-1], asym_Juliet_no_fe_333['asymmetry.age'], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(ages[:-1], asym_Thelma_no_fe_333['asymmetry.age'], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(ages[:-1], asym_Louise_no_fe_333['asymmetry.age'], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(ages[:-1], asym_Romulus_no_fe_333['asymmetry.age'], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(ages[:-1], asym_Remus_no_fe_333['asymmetry.age'], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(ages[:-1], asym_med_no_fe_333, color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(ages[:-1], asym_med_no_fe_333, linestyle='', label=' ')
ax.plot(ages[:-1], asym_med_no_fe_333, linestyle='', label=' ')
ax.plot(ages[:-1], asym_med_no_fe_333, linestyle='', label=' ')
ax.plot(ages[:-1], asym_med_no_fe_333, linestyle='', label=' ')
ax.plot(ages[:-1], asym_med_no_fe_333, linestyle='', label=' ')
#
plt.xticks(np.arange(5, 14.1, 1.))
#plt.yticks(np.arange(1, 11, 9))
plt.xlim(10, 13.8)
plt.ylim(ymin=1, ymax=30)
#plt.title('No metallicity cut, 0.333 Gyr spacing', fontsize=28)
plt.yscale('log')
plt.xlabel('Stellar Age [Gyr]', fontsize=32)
plt.ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/age_vs_asymmetry_all_no_fe_333.pdf')
plt.close()
