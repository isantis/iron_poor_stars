#!/usr/bin/python3

"""
 ======================
 = H3 Asymmetry Ratio =
 ======================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2020

 GOAL: Compute the asymmetry ratio using the H3 selection window

       - [Fe/H] < -2
       - 1 < |Z| < 3
       - |b| > 30 degrees]
            - I accomplish this via selecting stars above and below
              two "lines" in R vs Z space

      Version 1: Prograde/Retrograde
                    d['asymmetry']
                    d['asymmetry.upper']
                    d['asymmetry.lower']

      Version 2: Prograde/Total
                    d['asymmetry']

"""

## Import all of the tools for analysis
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
gal1 = 'Romulus'
loc = 'stampede'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

if num_gal == 1:
    ### Read in the z = 0 data
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #
    # Read in the old data to use the rotational velocity
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    #
    # Select stars out of the disk
    inds_1 = ut.array.get_indices(np.abs(part['star'].prop('star.host1.distance.cylindrical.principal')[:,2]), [1,3])
    #
    # Select stars with |b| > 30 degrees
    mask_up_1 = (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] > ((-0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] + 4.62)) & (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] > ((0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] - 4.62))
    mask_dn_1 = (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] < ((-0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] + 4.62)) & (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] < ((0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] - 4.62))
    #
    inds_up_1 = inds_1[mask_up_1]
    inds_dn_1 = inds_1[mask_dn_1]
    inds_tot_1 = np.sort(np.concatenate((inds_up_1, inds_dn_1)))
    #
    # Loop over metallicity
    metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
    asymmetry_tot_1 = np.zeros(len(metallicities)-1)
    asymmetry_up_1 = np.zeros(len(metallicities)-1)
    asymmetry_dn_1 = np.zeros(len(metallicities)-1)
    #
    for i in range(0, len(metallicities)-1):
        # All indices
        metal_inds_tot_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_tot_1)
        if len(metal_inds_tot_1) != 0:
            if len(metal_inds_tot_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_tot_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_tot_1[i] = np.nan
        else:
            asymmetry_tot_1[i] = np.nan
        #
        """
        # Upper stars
        metal_inds_up_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_up_1)
        if len(metal_inds_up_1) != 0:
            if len(metal_inds_up_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_up_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_up_1[i] = np.nan
        else:
            asymmetry_up_1[i] = np.nan
        #
        # Lower stars
        metal_inds_dn_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_dn_1)
        if len(metal_inds_dn_1) != 0:
            if len(metal_inds_dn_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_dn_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_dn_1[i] = np.nan
        else:
            asymmetry_dn_1[i] = np.nan
    #
    """
    # Save the data to a file
    d_1 = dict()
    d_1['asymmetry'] = asymmetry_tot_1
    #d_1['asymmetry.upper'] = asymmetry_up_1
    #d_1['asymmetry.lower'] = asymmetry_dn_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_h3_v2', dict_or_array_to_write=d_1, verbose=True)

if num_gal == 2:
    ### Read in the z = 0 data
    snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)
    part = gizmo.io.Read.read_snapshots('star', 'snapshot', 600, simulation_directory=simulation_dir, assign_hosts_rotation=True)
    #
    # Read in the old data to use the rotational velocity
    data_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_data')
    data_2 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_data')
    #
    # GALAXY 1
    # Select stars out of the disk
    inds_1 = ut.array.get_indices(np.abs(part['star'].prop('star.host1.distance.cylindrical.principal')[:,2]), [1,3])
    #
    # Select stars with |b| > 30 degrees
    mask_up_1 = (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] > ((-0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] + 4.62)) & (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] > ((0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] - 4.62))
    mask_dn_1 = (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] < ((-0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] + 4.62)) & (part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,2] < ((0.58)*part['star'].prop('star.host1.distance.cylindrical.principal', inds_1)[:,0] - 4.62))
    #
    inds_up_1 = inds_1[mask_up_1]
    inds_dn_1 = inds_1[mask_dn_1]
    inds_tot_1 = np.sort(np.concatenate((inds_up_1, inds_dn_1)))
    #
    # Loop over metallicity
    metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
    asymmetry_tot_1 = np.zeros(len(metallicities)-1)
    asymmetry_up_1 = np.zeros(len(metallicities)-1)
    asymmetry_dn_1 = np.zeros(len(metallicities)-1)
    #
    for i in range(0, len(metallicities)-1):
        # All indices
        metal_inds_tot_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_tot_1)
        if len(metal_inds_tot_1) != 0:
            if len(metal_inds_tot_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_tot_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_tot_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_tot_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_tot_1[i] = np.nan
        else:
            asymmetry_tot_1[i] = np.nan
        #
        """
        # Upper stars
        metal_inds_up_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_up_1)
        if len(metal_inds_up_1) != 0:
            if len(metal_inds_up_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_up_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_up_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_up_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_up_1[i] = np.nan
        else:
            asymmetry_up_1[i] = np.nan
        #
        # Lower stars
        metal_inds_dn_1 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_dn_1)
        if len(metal_inds_dn_1) != 0:
            if len(metal_inds_dn_1) == 1:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[0][0]*part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[0][1] - part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[0][0]*part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[0][1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[:,0]*part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[:,1] - part['star'].prop('star.host1.velocity.principal',metal_inds_dn_1)[:,0]*part['star'].prop('star.host1.distance.principal',metal_inds_dn_1)[:,1])/(8*data_1['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_dn_1[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_dn_1[i] = np.nan
        else:
            asymmetry_dn_1[i] = np.nan
    #
    """
    # Save the data to a file
    d_1 = dict()
    d_1['asymmetry'] = asymmetry_tot_1
    #d_1['asymmetry.upper'] = asymmetry_up_1
    #d_1['asymmetry.lower'] = asymmetry_dn_1
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asym_h3_v2', dict_or_array_to_write=d_1, verbose=True)
    #
    # GALAXY 2
    # Select stars out of the disk
    inds_2 = ut.array.get_indices(np.abs(part['star'].prop('star.host2.distance.cylindrical.principal')[:,2]), [1,3])
    #
    # Select stars with |b| > 30 degrees
    mask_up_2 = (part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,2] > ((-0.58)*part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,0] + 4.62)) & (part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,2] > ((0.58)*part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,0] - 4.62))
    mask_dn_2 = (part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,2] < ((-0.58)*part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,0] + 4.62)) & (part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,2] < ((0.58)*part['star'].prop('star.host2.distance.cylindrical.principal', inds_2)[:,0] - 4.62))
    #
    inds_up_2 = inds_2[mask_up_2]
    inds_dn_2 = inds_2[mask_dn_2]
    inds_tot_2 = np.sort(np.concatenate((inds_up_2, inds_dn_2)))
    #
    # Loop over metallicity
    asymmetry_tot_2 = np.zeros(len(metallicities)-1)
    asymmetry_up_2 = np.zeros(len(metallicities)-1)
    asymmetry_dn_2 = np.zeros(len(metallicities)-1)
    #
    for i in range(0, len(metallicities)-1):
        # All indices
        metal_inds_tot_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_tot_2)
        if len(metal_inds_tot_2) != 0:
            if len(metal_inds_tot_2) == 1:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_tot_2)[0][0]*part['star'].prop('star.host2.velocity.principal',metal_inds_tot_2)[0][1] - part['star'].prop('star.host2.velocity.principal',metal_inds_tot_2)[0][0]*part['star'].prop('star.host2.distance.principal',metal_inds_tot_2)[0][1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_tot_2)[:,0]*part['star'].prop('star.host2.velocity.principal',metal_inds_tot_2)[:,1] - part['star'].prop('star.host2.velocity.principal',metal_inds_tot_2)[:,0]*part['star'].prop('star.host2.distance.principal',metal_inds_tot_2)[:,1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_tot_2[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_tot_2[i] = np.nan
        else:
            asymmetry_tot_2[i] = np.nan
        #
        """
        # Upper stars
        metal_inds_up_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_up_2)
        if len(metal_inds_up_2) != 0:
            if len(metal_inds_up_2) == 1:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_up_2)[0][0]*part['star'].prop('star.host2.velocity.principal',metal_inds_up_2)[0][1] - part['star'].prop('star.host2.velocity.principal',metal_inds_up_2)[0][0]*part['star'].prop('star.host2.distance.principal',metal_inds_up_2)[0][1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_up_2)[:,0]*part['star'].prop('star.host2.velocity.principal',metal_inds_up_2)[:,1] - part['star'].prop('star.host2.velocity.principal',metal_inds_up_2)[:,0]*part['star'].prop('star.host2.distance.principal',metal_inds_up_2)[:,1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_up_2[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_up_2[i] = np.nan
        else:
            asymmetry_up_2[i] = np.nan
        #
        # Lower stars
        metal_inds_dn_2 = ut.array.get_indices(part['star'].prop('metallicity.iron'), [metallicities[i], metallicities[i+1]], inds_dn_2)
        if len(metal_inds_dn_2) != 0:
            if len(metal_inds_dn_2) == 1:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_dn_2)[0][0]*part['star'].prop('star.host2.velocity.principal',metal_inds_dn_2)[0][1] - part['star'].prop('star.host2.velocity.principal',metal_inds_dn_2)[0][0]*part['star'].prop('star.host2.distance.principal',metal_inds_dn_2)[0][1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            else:
                jphi = (part['star'].prop('star.host2.distance.principal',metal_inds_dn_2)[:,0]*part['star'].prop('star.host2.velocity.principal',metal_inds_dn_2)[:,1] - part['star'].prop('star.host2.velocity.principal',metal_inds_dn_2)[:,0]*part['star'].prop('star.host2.distance.principal',metal_inds_dn_2)[:,1])/(8*data_2['vrot.z0'])
                pos_mask = (jphi > 0)
                neg_mask = (jphi < 0)
            if np.sum(neg_mask) != 0:
                asymmetry_dn_2[i] = np.sum(pos_mask)/np.sum(neg_mask)
            else:
                asymmetry_dn_2[i] = np.nan
        else:
            asymmetry_dn_2[i] = np.nan
    #
    """
    # Save the data to a file
    d_2 = dict()
    d_2['asymmetry'] = asymmetry_tot_2
    #d_2['asymmetry.upper'] = asymmetry_up_2
    #d_2['asymmetry.lower'] = asymmetry_dn_2
    #
    ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asym_h3_v2', dict_or_array_to_write=d_2, verbose=True)
