#!/usr/bin/python3

"""
 ===========================
 = Asymmetry + Merger plot =
 ===========================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Quarter, 2020

 GOAL: Plot the asymmetry evolution arrays and offset angle arrays for each host
"""

## Import all of the tools for analysis
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
gal1 = 'Romeo'
#selection = 'z0'
selection = 'all_fe_poor'
gal_color = colors[3]
loc = 'mac'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
    gal_color2 = colors[3]
    num_gal = 2
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    gal_color2 = colors[3]
    num_gal = 2
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
    gal_color2 = colors[3]
    num_gal = 2
else:
    galaxy = gal1
    resolution = '_res7100'
    num_gal = 1

if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
    simulation_dir = '/Users/isaiahsantistevan/simulation/galaxies/m12m_res7100'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
print('Set paths')

snaps = ut.simulation.read_snapshot_times(directory=simulation_dir)

if num_gal == 1:
    # Read in the data for all hosts
    asym_r412 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r412_vz_MOIz_'+selection)
    offset_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_primary_merger')
    halo_ses_pro = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    #
    # Get merger lookback times
    merger_1 = snaps['time'][600] - snaps['time'][halo_ses_pro['unique.snap'][np.where(halo_ses_pro['num.contr'] == np.flip(np.sort(halo_ses_pro['num.contr']))[0])[0][0]]]
    merger_2 = snaps['time'][600] - snaps['time'][halo_ses_pro['unique.snap'][np.where(halo_ses_pro['num.contr'] == np.flip(np.sort(halo_ses_pro['num.contr']))[1])[0][0]]]
    merger_3 = snaps['time'][600] - snaps['time'][halo_ses_pro['unique.snap'][np.where(halo_ses_pro['num.contr'] == np.flip(np.sort(halo_ses_pro['num.contr']))[2])[0][0]]]
    #
    # Plot all of the data on one figure
    lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
    #
    plt.rcParams["font.family"] = "serif"
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212, sharex=ax1)
    #
    ax1.plot(lookback[asym_r412['asymmetry.snapshots']][asym_r412['asymmetry'] != 1e6], asym_r412['asymmetry'][asym_r412['asymmetry'] != 1e6], color=gal_color, linestyle='-', label=gal1)
    ax1.arrow(x=merger_1, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=1.0)
    ax1.arrow(x=merger_2, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.5)
    ax1.arrow(x=merger_3, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.2)
    #ax1.arrow(x=merger_1, y=9.5, dx=0, dy=-2.25, width=0.1, head_width=0.3, head_length=0.75, length_includes_head=True, color='k', alpha=1.0)
    #ax1.arrow(x=merger_2, y=9.5, dx=0, dy=-2.25, width=0.1, head_width=0.3, head_length=0.75, length_includes_head=True, color='k', alpha=0.5)
    #ax1.arrow(x=merger_3, y=9.5, dx=0, dy=-2.25, width=0.1, head_width=0.3, head_length=0.75, length_includes_head=True, color='k', alpha=0.2)
    ax1.hlines(1, 0, 14, 'k', 'dotted')
    ax1.set_xlim(lookback[asym_r412['asymmetry.snapshots']][-1], lookback[asym_r412['asymmetry.snapshots']][0])
    ax1.set_ylim(ymin=0.5, ymax=4)
    ax1.set_ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=24)
    ax1.get_yaxis().set_label_coords(-0.08, 0.5)
    ax1.label_outer()
    ax1.text(3, 3.4, gal1, fontsize=22, bbox={'facecolor':'black', 'alpha': 0.3, 'pad': 6})
    ax1.text(3, 3.4, gal1, fontsize=22, bbox={'facecolor':'none', 'edgecolor':'black', 'alpha': 0.7, 'pad': 6})
    #ax1.text(7, 8, gal1, fontsize=22, bbox={'facecolor':'black', 'alpha': 0.3, 'pad': 6})
    #ax1.text(7, 8, gal1, fontsize=22, bbox={'facecolor':'none', 'edgecolor':'black', 'alpha': 0.7, 'pad': 6})
    ##ax1.legend(prop={'size': 18}, loc='upper center')
    #
    ax2.plot(lookback[offset_1['snap.pro']], offset_1['theta.pro'], color=gal_color)
    ax2.set_xlim(lookback[asym_r412['asymmetry.snapshots']][-1], lookback[asym_r412['asymmetry.snapshots']][0])
    ax2.set_ylim(0, 180)
    ax2.set_yticks(np.arange(0, 181, 30))
    ax2.set_ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    ax2.get_yaxis().set_label_coords(-0.08, 0.5)
    ax2.label_outer()
    #
    plt.xlabel('lookback time [Gyr]', fontsize=28)
    plt.tight_layout()
    #plt.suptitle(gal1, fontsize=28, y=0.915)
    plt.subplots_adjust(hspace=0.05)
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal1+'_asym_offset.pdf')
    plt.close()

if num_gal == 2:
    # Read in the data for all hosts
    asym_r412_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_asymmetry_r412_vz_MOIz_'+selection)
    offset_1 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_primary_merger')
    halo_ses_pro_1 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal1+'_sestito_prograde_data')
    #
    asym_r412_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_asymmetry_r412_vz_MOIz_'+selection)
    offset_2 = ut.io.file_hdf5(file_name_base=home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_primary_merger')
    halo_ses_pro_2 = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/'+gal2+'_sestito_prograde_data')
    #
    # Get merger lookback times
    merger_1_1 = snaps['time'][600] - snaps['time'][halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[0])[0][0]]]
    merger_2_1 = snaps['time'][600] - snaps['time'][halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[1])[0][0]]]
    merger_3_1 = snaps['time'][600] - snaps['time'][halo_ses_pro_1['unique.snap'][np.where(halo_ses_pro_1['num.contr'] == np.flip(np.sort(halo_ses_pro_1['num.contr']))[2])[0][0]]]
    #
    merger_1_2 = snaps['time'][600] - snaps['time'][halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[0])[0][0]]]
    merger_2_2 = snaps['time'][600] - snaps['time'][halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[1])[0][0]]]
    merger_3_2 = snaps['time'][600] - snaps['time'][halo_ses_pro_2['unique.snap'][np.where(halo_ses_pro_2['num.contr'] == np.flip(np.sort(halo_ses_pro_2['num.contr']))[2])[0][0]]]
    #
    # Plot all of the data on one figure
    lookback = np.around(snaps['time'][600] - snaps['time'],decimals=3)
    #
    plt.rcParams["font.family"] = "serif"
    plt.figure(1)
    plt.figure(figsize=(10, 8))
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212, sharex=ax1)
    #
    ax1.plot(lookback[asym_r412_1['asymmetry.snapshots']][asym_r412_1['asymmetry'] != 1e6], asym_r412_1['asymmetry'][asym_r412_1['asymmetry'] != 1e6], color=gal_color, linestyle='-', label=gal1)
    ax1.arrow(x=merger_1_1, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=1.0)
    ax1.arrow(x=merger_2_1, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.5)
    ax1.arrow(x=merger_3_1, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.2)
    ax1.hlines(1, 0, 14, 'k', 'dotted')
    ax1.set_xlim(lookback[asym_r412_1['asymmetry.snapshots']][-1], lookback[asym_r412_1['asymmetry.snapshots']][0])
    ax1.set_ylim(ymin=0.5, ymax=4)
    ax1.set_ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=24)
    ax1.get_yaxis().set_label_coords(-0.08, 0.5)
    ax1.label_outer()
    ax1.text(3, 3.4, gal1, fontsize=22, bbox={'facecolor':'black', 'alpha': 0.3, 'pad': 6})
    ax1.text(3, 3.4, gal1, fontsize=22, bbox={'facecolor':'none', 'edgecolor':'black', 'alpha': 0.7, 'pad': 6})
    #ax1.legend(prop={'size': 18}, loc='best')
    #
    ax2.plot(lookback[offset_1['snap.pro']], offset_1['theta.pro'], color=gal_color)
    ax2.set_xlim(lookback[asym_r412_1['asymmetry.snapshots']][-1], lookback[asym_r412_1['asymmetry.snapshots']][0])
    ax2.set_ylim(0, 180)
    ax2.set_yticks(np.arange(0, 181, 30))
    ax2.set_ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    ax2.get_yaxis().set_label_coords(-0.08, 0.5)
    ax2.label_outer()
    #
    plt.xlabel('lookback time [Gyr]', fontsize=28)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.05)
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal1+'_asym_offset.pdf')
    plt.close()
    #
    #
    #
    plt.rcParams["font.family"] = "serif"
    plt.figure(2)
    plt.figure(figsize=(10, 8))
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212, sharex=ax1)
    #
    ax1.plot(lookback[asym_r412_2['asymmetry.snapshots']][asym_r412_2['asymmetry'] != 1e6], asym_r412_2['asymmetry'][asym_r412_2['asymmetry'] != 1e6], color=gal_color2, linestyle='-', label=gal2)
    ax1.arrow(x=merger_1_2, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=1.0)
    ax1.arrow(x=merger_2_2, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.5)
    ax1.arrow(x=merger_3_2, y=4, dx=0, dy=-0.85, width=0.1, head_width=0.3, head_length=0.25, length_includes_head=True, color='k', alpha=0.2)
    ax1.hlines(1, 0, 14, 'k', 'dotted')
    ax1.set_xlim(lookback[asym_r412_2['asymmetry.snapshots']][-1], lookback[asym_r412_2['asymmetry.snapshots']][0])
    ax1.set_ylim(ymin=0.5, ymax=4)
    ax1.set_ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=24)
    ax1.get_yaxis().set_label_coords(-0.08, 0.5)
    ax1.label_outer()
    ax1.legend(prop={'size': 18}, loc='best')
    #
    ax2.plot(lookback[offset_2['snap.pro']], offset_2['theta.pro'], color=gal_color2)
    ax2.set_xlim(lookback[asym_r412_2['asymmetry.snapshots']][-1], lookback[asym_r412_2['asymmetry.snapshots']][0])
    ax2.set_ylim(0, 180)
    ax2.set_yticks(np.arange(0, 181, 30))
    ax2.set_ylabel('$\\theta_{\\rm offset}$ [degree]', fontsize=24)
    ax2.get_yaxis().set_label_coords(-0.08, 0.5)
    ax2.label_outer()
    #
    plt.xlabel('lookback time [Gyr]', fontsize=28)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.05)
    plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/'+gal2+'_asym_offset.pdf')
    plt.close()
