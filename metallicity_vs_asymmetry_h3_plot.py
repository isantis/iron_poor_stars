#!/usr/bin/python3

"""
 =================================
 = Metallicity vs Asymmetry (H3) =
 =================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2020

 GOAL: Plot [Fe/H] (x-axis) vs asymmetry (y-axis) for all hosts

"""

# Load in the tools
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

### Set path and initial parameters
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']
# color 0 m12b, 1 m12c, 6 m12f, 3 m12i, 4 m12m, 5 m12w
#       0 Romeo, 1 Juliet, 6 Thelma, 3 Louise, 4 Romulus, 5 Remus
#
loc = 'mac'
if loc == 'mac':
    home_dir = '/Users/isaiahsantistevan/simulation'
else:
    home_dir = '/home1/05400/ibsantis/scripts'
    plt.rcParams["font.family"] = "serif"
print('Set paths')

## Read in the data
# Binned
asym_m12b = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12b_asym_h3')
asym_m12c = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12c_asym_h3')
asym_m12f = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12f_asym_h3')
asym_m12i = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12i_asym_h3')
asym_m12m = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12m_asym_h3')
asym_m12w = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/m12w_asym_h3')
asym_Romeo = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romeo_asym_h3')
asym_Juliet = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Juliet_asym_h3')
asym_Thelma = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Thelma_asym_h3')
asym_Louise = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Louise_asym_h3')
asym_Romulus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Romulus_asym_h3')
asym_Remus = ut.io.file_hdf5(home_dir+'/iron_poor_data/hdf5_files/Remus_asym_h3')

# Calculate the median asymmetry for each metallicity
# Differential plot (ALL DATA)
# Stack the data
asym_all = np.vstack((asym_m12b['asymmetry'],asym_m12c['asymmetry'],asym_m12f['asymmetry'],asym_m12i['asymmetry'],asym_m12m['asymmetry'],asym_m12w['asymmetry'],asym_Romeo['asymmetry'],asym_Juliet['asymmetry'],asym_Thelma['asymmetry'],asym_Louise['asymmetry'],asym_Romulus['asymmetry'],asym_Remus['asymmetry']))
asym_med_all = np.nanmedian(asym_all, axis=0)
#
asym_up = np.vstack((asym_m12b['asymmetry.upper'],asym_m12c['asymmetry.upper'],asym_m12f['asymmetry.upper'],asym_m12i['asymmetry.upper'],asym_m12m['asymmetry.upper'],asym_m12w['asymmetry.upper'],asym_Romeo['asymmetry.upper'],asym_Juliet['asymmetry.upper'],asym_Thelma['asymmetry.upper'],asym_Louise['asymmetry.upper'],asym_Romulus['asymmetry.upper'],asym_Remus['asymmetry.upper']))
asym_med_up = np.nanmedian(asym_up, axis=0)
#
asym_dn = np.vstack((asym_m12b['asymmetry.lower'],asym_m12c['asymmetry.lower'],asym_m12f['asymmetry.lower'],asym_m12i['asymmetry.lower'],asym_m12m['asymmetry.lower'],asym_m12w['asymmetry.lower'],asym_Romeo['asymmetry.lower'],asym_Juliet['asymmetry.lower'],asym_Thelma['asymmetry.lower'],asym_Louise['asymmetry.lower'],asym_Romulus['asymmetry.lower'],asym_Remus['asymmetry.lower']))
asym_med_dn = np.nanmedian(asym_dn, axis=0)
#
metallicities = np.array([-4.0, -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0])
metallicities = metallicities+0.25

"""
Function used to calculate the upper and lower errors:

def errors(df_low, df_up, f):
    g = f/(1 - f)
    #
    f_low = f - df_low
    g_low = f_low/(1 - f_low)
    #
    dg_low = g - g_low
    #
    f_up = f + df_up
    g_up = f_up/(1 - f_up)
    #
    dg_up = g_up - g
    #
    return (dg_low, dg_up)

    Where
        df_low and df_up are given from the H3 survey
        f is the prograde/total fraction
        g is prograde/retrograde fraction
        dg_low and dg_up are new errors
"""

#
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(metallicities[:8], asym_m12b['asymmetry'][:8], color=colors[0], label='m12b', alpha=0.5)
ax.plot(metallicities[:8], asym_m12c['asymmetry'][:8], color=colors[1], label='m12c', alpha=0.5)
ax.plot(metallicities[:8], asym_m12f['asymmetry'][:8], color=colors[6], label='m12f', alpha=0.5)
ax.plot(metallicities[:8], asym_m12i['asymmetry'][:8], color=colors[3], label='m12i', alpha=0.5)
ax.plot(metallicities[:8], asym_m12m['asymmetry'][:8], color=colors[4], label='m12m', alpha=0.5)
ax.plot(metallicities[:8], asym_m12w['asymmetry'][:8], color=colors[5], label='m12w', alpha=0.5)
ax.plot(metallicities[:8], asym_Romeo['asymmetry'][:8], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Juliet['asymmetry'][:8], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Thelma['asymmetry'][:8], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Louise['asymmetry'][:8], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Romulus['asymmetry'][:8], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Remus['asymmetry'][:8], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(metallicities[:8], asym_med_all[:8], color='k', label='median', alpha=0.8, linewidth=7)
#ax.plot(metallicities[:8], asym_med_up[:8], color='k', linestyle='-.', label='median (upper)', alpha=0.5, linewidth=7)
#ax.plot(metallicities[:8], asym_med_dn[:8], color='k', linestyle=':', label='median (lower)', alpha=0.5, linewidth=7)
plt.errorbar(-3.25, 5, yerr=np.array([[3.0337944821661287],[17.643204077131124]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-3.25, 5, s=300, marker='x', c='k', label='MW (C20)')
#
plt.errorbar(-2.75, 3.81, yerr=np.array([[0.9060969380566535],[1.2700766855041454]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-2.75, 3.81, s=300, marker='x', c='k')
#
plt.errorbar(-2.25, 2.38, yerr=np.array([[0.2441672925943843],[0.2763911991369863]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-2.25, 2.38, s=300, marker='x', c='k')
#
plt.errorbar(-1.75, 2.25, yerr=np.array([[0.1594699952187617],[0.17336757575872985]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-1.75, 2.25, s=300, marker='x', c='k')
#
plt.errorbar(-1.375, 2.07, yerr=np.array([[0.1473078541575863],[0.16006681211690177]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-1.375, 2.07, s=300, marker='x', c='k')
#
plt.errorbar(-1.125, 2.25, yerr=np.array([[0.13617327358045284],[0.1462371859872995]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-1.125, 2.25, s=300, marker='x', c='k')
#
plt.errorbar(-0.875, 5.49, yerr=np.array([[0.31953776338214457],[0.3439306034531695]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-0.875, 5.49, s=300, marker='x', c='k')
#
plt.errorbar(-0.625, 17.67, yerr=np.array([[0.8901747107577371],[0.9595635085693317]]), color='k', lw=5, capsize=8, alpha=0.60)
plt.scatter(-0.625, 17.67, s=300, marker='x', c='k')
#
ax.plot(metallicities[:8], asym_med_all[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_all[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_all[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_all[:8], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('$M_{\\rm star, pro}$/$M_{\\rm star, ret}$', fontsize=32)
#
handles,labels = ax.get_legend_handles_labels()
handles = [handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9], handles[10], handles[11], handles[12], handles[17], handles[13], handles[14], handles[15], handles[16]]
labels = [labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9], labels[10], labels[11], labels[12], labels[17], labels[13], labels[14], labels[15], labels[16]]
ax.legend(handles, labels, ncol=3, prop={'size': 17})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_h3_all.pdf')
plt.close()

# Differential plot (UPPER DATA)
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(metallicities[:8], asym_m12b['asymmetry.upper'][:8], color=colors[0], label='m12b', alpha=0.5)
ax.plot(metallicities[:8], asym_m12c['asymmetry.upper'][:8], color=colors[1], label='m12c', alpha=0.5)
ax.plot(metallicities[:8], asym_m12f['asymmetry.upper'][:8], color=colors[6], label='m12f', alpha=0.5)
ax.plot(metallicities[:8], asym_m12i['asymmetry.upper'][:8], color=colors[3], label='m12i', alpha=0.5)
ax.plot(metallicities[:8], asym_m12m['asymmetry.upper'][:8], color=colors[4], label='m12m', alpha=0.5)
ax.plot(metallicities[:8], asym_m12w['asymmetry.upper'][:8], color=colors[5], label='m12w', alpha=0.5)
ax.plot(metallicities[:8], asym_Romeo['asymmetry.upper'][:8], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Juliet['asymmetry.upper'][:8], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Thelma['asymmetry.upper'][:8], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Louise['asymmetry.upper'][:8], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Romulus['asymmetry.upper'][:8], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Remus['asymmetry.upper'][:8], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(metallicities[:8], asym_med_up[:8], color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(metallicities[:8], asym_med_up[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_up[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_up[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_up[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_up[:8], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
ax.legend(ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_h3_up.pdf')
plt.close()


# Differential plot (LOWER DATA)
plt.figure(3)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#
ax.plot(metallicities[:8], asym_m12b['asymmetry.lower'][:8], color=colors[0], label='m12b', alpha=0.5)
ax.plot(metallicities[:8], asym_m12c['asymmetry.lower'][:8], color=colors[1], label='m12c', alpha=0.5)
ax.plot(metallicities[:8], asym_m12f['asymmetry.lower'][:8], color=colors[6], label='m12f', alpha=0.5)
ax.plot(metallicities[:8], asym_m12i['asymmetry.lower'][:8], color=colors[3], label='m12i', alpha=0.5)
ax.plot(metallicities[:8], asym_m12m['asymmetry.lower'][:8], color=colors[4], label='m12m', alpha=0.5)
ax.plot(metallicities[:8], asym_m12w['asymmetry.lower'][:8], color=colors[5], label='m12w', alpha=0.5)
ax.plot(metallicities[:8], asym_Romeo['asymmetry.lower'][:8], color=colors[0], label='Romeo', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Juliet['asymmetry.lower'][:8], color=colors[1], label='Juliet', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Thelma['asymmetry.lower'][:8], color=colors[6], label='Thelma', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Louise['asymmetry.lower'][:8], color=colors[3], label='Louise', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Romulus['asymmetry.lower'][:8], color=colors[4], label='Romulus', alpha=0.5, linestyle=':')
ax.plot(metallicities[:8], asym_Remus['asymmetry.lower'][:8], color=colors[5], label='Remus', alpha=0.5, linestyle=':')
#
ax.plot(metallicities[:8], asym_med_dn[:8], color='k', label='median', alpha=0.8, linewidth=7)
ax.plot(metallicities[:8], asym_med_dn[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_dn[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_dn[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_dn[:8], linestyle='', label=' ')
ax.plot(metallicities[:8], asym_med_dn[:8], linestyle='', label=' ')
#
plt.xticks(np.arange(-3.5, 2.1, 0.5))
plt.yticks(np.arange(1, 11, 9))
plt.xlim(-3.8, -0.4)
plt.ylim(ymin=1, ymax=30)
plt.yscale('log')
plt.xlabel('[Fe/H]', fontsize=32)
plt.ylabel('M$_{\\rm star, pro}$/M$_{\\rm star, ret}$', fontsize=32)
#
ax.legend(ncol=3, prop={'size': 18})
plt.tick_params(axis='both', which='major', labelsize=24)
#
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir+'/iron_poor_data/hdf5_plots/metallicity_vs_asymmetry_h3_dn.pdf')
plt.close()
